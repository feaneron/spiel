/*
 * test-slide-features-descriptor.c
 *
 * Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "spiel-slide-feature-private.h"
#include "spiel-slide-feature-descriptor.h"

/******************************************************************************/

static void
slide_feature_descriptor_media (void)
{
  g_autoptr(SpielSlideFeatureDescriptor) descriptor = NULL;
  g_autoptr(SpielSlideFeature) media = NULL;
  g_autoptr(GError) error = NULL;

  media = spiel_slide_feature_new_media ();

  descriptor = spiel_slide_feature_descriptor_from_string ("media", &error);
  g_assert_no_error (error);
  g_assert_nonnull (descriptor);
  g_assert_true (spiel_slide_feature_descriptor_match (descriptor, media));

  g_clear_object (&descriptor);

  descriptor = spiel_slide_feature_descriptor_from_string ("media()", &error);
  g_assert_error (error,
                  SPIEL_SLIDE_FEATURE_DESCRIPTOR_ERROR,
                  SPIEL_SLIDE_FEATURE_DESCRIPTOR_ERROR_INVALID_SYNTAX);
  g_assert_null (descriptor);
  g_clear_error (&error);
}

/******************************************************************************/

static void
slide_feature_descriptor_note (void)
{
  g_autoptr(SpielSlideFeatureDescriptor) descriptor = NULL;
  g_autoptr(SpielSlideFeature) note = NULL;
  g_autoptr(GError) error = NULL;

  note = spiel_slide_feature_new_note (g_strdup ("Note"));

  descriptor = spiel_slide_feature_descriptor_from_string ("note", &error);
  g_assert_no_error (error);
  g_assert_nonnull (descriptor);
  g_assert_true (spiel_slide_feature_descriptor_match (descriptor, note));

  g_clear_object (&descriptor);

  descriptor = spiel_slide_feature_descriptor_from_string ("note()", &error);
  g_assert_error (error,
                  SPIEL_SLIDE_FEATURE_DESCRIPTOR_ERROR,
                  SPIEL_SLIDE_FEATURE_DESCRIPTOR_ERROR_INVALID_SYNTAX);
  g_assert_null (descriptor);
  g_clear_error (&error);
}

/******************************************************************************/

static void
slide_feature_descriptor_title (void)
{
  g_autoptr(SpielSlideFeatureDescriptor) descriptor = NULL;
  g_autoptr(SpielSlideFeature) title_1 = NULL;
  g_autoptr(SpielSlideFeature) title_2 = NULL;
  g_autoptr(SpielSlideFeature) title_3 = NULL;
  g_autoptr(GError) error = NULL;

  title_1 = spiel_slide_feature_new_title (g_strdup ("Title 1"), 1);
  title_2 = spiel_slide_feature_new_title (g_strdup ("Title 2"), 2);
  title_3 = spiel_slide_feature_new_title (g_strdup ("Title 3"), 3);

  descriptor = spiel_slide_feature_descriptor_from_string ("title(1)", &error);
  g_assert_no_error (error);
  g_assert_nonnull (descriptor);
  g_assert_true (spiel_slide_feature_descriptor_match (descriptor, title_1));
  g_assert_false (spiel_slide_feature_descriptor_match (descriptor, title_2));
  g_assert_false (spiel_slide_feature_descriptor_match (descriptor, title_3));

  g_clear_object (&descriptor);

  descriptor = spiel_slide_feature_descriptor_from_string ("title(> 1)", &error);
  g_assert_no_error (error);
  g_assert_nonnull (descriptor);
  g_assert_false (spiel_slide_feature_descriptor_match (descriptor, title_1));
  g_assert_true (spiel_slide_feature_descriptor_match (descriptor, title_2));
  g_assert_true (spiel_slide_feature_descriptor_match (descriptor, title_3));

  g_clear_object (&descriptor);

  descriptor = spiel_slide_feature_descriptor_from_string ("title(< 2)", &error);
  g_assert_no_error (error);
  g_assert_nonnull (descriptor);
  g_assert_true (spiel_slide_feature_descriptor_match (descriptor, title_1));
  g_assert_false (spiel_slide_feature_descriptor_match (descriptor, title_2));
  g_assert_false (spiel_slide_feature_descriptor_match (descriptor, title_3));

  g_clear_object (&descriptor);

  descriptor = spiel_slide_feature_descriptor_from_string ("title(>= 2)", &error);
  g_assert_no_error (error);
  g_assert_nonnull (descriptor);
  g_assert_false (spiel_slide_feature_descriptor_match (descriptor, title_1));
  g_assert_true (spiel_slide_feature_descriptor_match (descriptor, title_2));
  g_assert_true (spiel_slide_feature_descriptor_match (descriptor, title_3));

  g_clear_object (&descriptor);

  descriptor = spiel_slide_feature_descriptor_from_string ("title(<= 2)", &error);
  g_assert_no_error (error);
  g_assert_nonnull (descriptor);
  g_assert_true (spiel_slide_feature_descriptor_match (descriptor, title_1));
  g_assert_true (spiel_slide_feature_descriptor_match (descriptor, title_2));
  g_assert_false (spiel_slide_feature_descriptor_match (descriptor, title_3));

  g_clear_object (&descriptor);

  descriptor = spiel_slide_feature_descriptor_from_string ("title(!= 2)", &error);
  g_assert_no_error (error);
  g_assert_nonnull (descriptor);
  g_assert_true (spiel_slide_feature_descriptor_match (descriptor, title_1));
  g_assert_false (spiel_slide_feature_descriptor_match (descriptor, title_2));
  g_assert_true (spiel_slide_feature_descriptor_match (descriptor, title_3));

  g_clear_object (&descriptor);

  descriptor = spiel_slide_feature_descriptor_from_string ("title(>=)", &error);
  g_assert_error (error,
                  SPIEL_SLIDE_FEATURE_DESCRIPTOR_ERROR,
                  SPIEL_SLIDE_FEATURE_DESCRIPTOR_ERROR_INVALID_SYNTAX);
  g_assert_null (descriptor);
  g_clear_error (&error);

  descriptor = spiel_slide_feature_descriptor_from_string ("title(-5)", &error);
  g_assert_error (error,
                  SPIEL_SLIDE_FEATURE_DESCRIPTOR_ERROR,
                  SPIEL_SLIDE_FEATURE_DESCRIPTOR_ERROR_INVALID_SYNTAX);
  g_assert_null (descriptor);
  g_clear_error (&error);

  descriptor = spiel_slide_feature_descriptor_from_string ("title(99999999999999999999999999)", &error);
  g_assert_error (error,
                  SPIEL_SLIDE_FEATURE_DESCRIPTOR_ERROR,
                  SPIEL_SLIDE_FEATURE_DESCRIPTOR_ERROR_INVALID_SYNTAX);
  g_assert_null (descriptor);
  g_clear_error (&error);
}

/******************************************************************************/

static void
slide_feature_descriptor_equal (void)
{
  g_autoptr(SpielSlideFeatureDescriptor) descriptor_a = NULL;
  g_autoptr(SpielSlideFeatureDescriptor) descriptor_b = NULL;
  g_autoptr(GError) error = NULL;

  descriptor_a = spiel_slide_feature_descriptor_from_string ("note", &error);
  g_assert_no_error (error);
  g_assert_nonnull (descriptor_a);
  descriptor_b = spiel_slide_feature_descriptor_from_string ("note", &error);
  g_assert_no_error (error);
  g_assert_nonnull (descriptor_b);
  g_assert_true (spiel_slide_feature_descriptor_equal (descriptor_a, descriptor_b));
  g_clear_object (&descriptor_a);
  g_clear_object (&descriptor_b);

  descriptor_a = spiel_slide_feature_descriptor_from_string ("title(>1)", &error);
  g_assert_no_error (error);
  g_assert_nonnull (descriptor_a);
  descriptor_b = spiel_slide_feature_descriptor_from_string ("title(>   1   )", &error);
  g_assert_no_error (error);
  g_assert_nonnull (descriptor_b);
  g_assert_true (spiel_slide_feature_descriptor_equal (descriptor_a, descriptor_b));
  g_clear_object (&descriptor_a);
  g_clear_object (&descriptor_b);

  descriptor_a = spiel_slide_feature_descriptor_from_string ("title(1)", &error);
  g_assert_no_error (error);
  g_assert_nonnull (descriptor_a);
  descriptor_b = spiel_slide_feature_descriptor_from_string ("note", &error);
  g_assert_no_error (error);
  g_assert_nonnull (descriptor_b);
  g_assert_false (spiel_slide_feature_descriptor_equal (descriptor_a, descriptor_b));
  g_clear_object (&descriptor_a);
  g_clear_object (&descriptor_b);

  descriptor_a = spiel_slide_feature_descriptor_from_string ("title(1)", &error);
  g_assert_no_error (error);
  g_assert_nonnull (descriptor_a);
  descriptor_b = spiel_slide_feature_descriptor_from_string ("title(<3)", &error);
  g_assert_no_error (error);
  g_assert_nonnull (descriptor_b);
  g_assert_false (spiel_slide_feature_descriptor_equal (descriptor_a, descriptor_b));
  g_clear_object (&descriptor_a);
  g_clear_object (&descriptor_b);
}

/******************************************************************************/

int
main (int   argc,
      char *argv[])
{
  g_test_init (&argc, &argv, G_TEST_OPTION_ISOLATE_DIRS, NULL);

  g_test_add_func ("/slide/feature-descriptor/media", slide_feature_descriptor_media);
  g_test_add_func ("/slide/feature-descriptor/note", slide_feature_descriptor_note);
  g_test_add_func ("/slide/feature-descriptor/title", slide_feature_descriptor_title);

  g_test_add_func ("/slide/feature-descriptor/equal", slide_feature_descriptor_equal);

  return g_test_run ();
}
