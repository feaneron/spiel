/*
 * test-project.c
 *
 * Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "spiel-project-private.h"

typedef struct {
  GMainLoop *mainloop;
  gpointer data;
  GError *error;
} AsyncData;

/******************************************************************************/

static void
project_created_cb (GObject      *source,
                    GAsyncResult *result,
                    gpointer      user_data)
{
  AsyncData *data = user_data;

  data->data = spiel_project_create_finish (result, &data->error);
  g_main_loop_quit (data->mainloop);
}

static void
project_create (void)
{
  g_autoptr (GMainLoop) mainloop = NULL;
  AsyncData data;

  mainloop = g_main_loop_new (NULL, FALSE);

  data = (AsyncData) {
    .mainloop = mainloop,
    .data = NULL,
    .error = NULL,
  };

  spiel_project_create (NULL, project_created_cb, &data);

  g_main_loop_run (mainloop);

  g_assert_no_error (data.error);
  g_assert_nonnull (data.data);
  g_assert_true (SPIEL_IS_PROJECT (data.data));

  g_assert_finalize_object (G_OBJECT (data.data));
}

/******************************************************************************/

int
main (int   argc,
      char *argv[])
{
  g_test_init (&argc, &argv, G_TEST_OPTION_ISOLATE_DIRS, NULL);

  g_test_add_func ("/project/create", project_create);

  return g_test_run ();
}
