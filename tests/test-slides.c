/*
 * test-slides.c
 *
 * Copyright 2024 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "spiel-slide.h"
#include "spiel-slide-feature-descriptor.h"

static size_t
count_features (const SpielSlideFeature **features)
{
  size_t counter = 0;
  while (features && features[counter])
    counter++;
  return counter;
}

/******************************************************************************/

static void
slide_media (void)
{
  struct {
    const char *markdown;
    size_t n_features;
    const char * const * features;
  } test_cases[] = {
    {
      .markdown = "![**a** *b* _c_](file.png)",
      .n_features = 1,
      .features = (const char * const []) {
        "media",
        NULL,
      },
    },
  };

  for (size_t i = 0; i < G_N_ELEMENTS (test_cases); i++)
    {
      const SpielSlideFeature **features;
      g_autoptr (SpielSlide) slide = NULL;

      slide = spiel_slide_new (test_cases[i].markdown);
      g_assert_true (SPIEL_IS_SLIDE (slide));

      features = spiel_slide_get_features (slide);
      g_assert_cmpuint (count_features (features), ==, test_cases[i].n_features);

      for (size_t j = 0; j < test_cases[i].n_features; j++)
        {
          g_autoptr (SpielSlideFeatureDescriptor) descriptor = NULL;
          g_autoptr (GError) error = NULL;

          descriptor = spiel_slide_feature_descriptor_from_string (test_cases[i].features[j], &error);
          g_assert_no_error (error);
          g_assert_true (spiel_slide_feature_descriptor_match (descriptor, features[j]));
        }
    }
}

/******************************************************************************/

static void
slide_title (void)
{
  struct {
    const char *markdown;
    size_t n_features;
    const char * const * features;
  } test_cases[] = {
    {
      .markdown = "# Title 1",
      .n_features = 1,
      .features = (const char * const []) {
        "title(1)",
        NULL,
      },
    },
    {
      .markdown = "## Title 2",
      .n_features = 1,
      .features = (const char * const []) {
        "title(2)",
        NULL,
      },
    },
  };

  for (size_t i = 0; i < G_N_ELEMENTS (test_cases); i++)
    {
      const SpielSlideFeature **features;
      g_autoptr (SpielSlide) slide = NULL;

      slide = spiel_slide_new (test_cases[i].markdown);
      g_assert_true (SPIEL_IS_SLIDE (slide));

      features = spiel_slide_get_features (slide);
      g_assert_cmpuint (count_features (features), ==, test_cases[i].n_features);

      for (size_t j = 0; j < test_cases[i].n_features; j++)
        {
          g_autoptr (SpielSlideFeatureDescriptor) descriptor = NULL;
          g_autoptr (GError) error = NULL;

          descriptor = spiel_slide_feature_descriptor_from_string (test_cases[i].features[j], &error);
          g_assert_no_error (error);
          g_assert_true (spiel_slide_feature_descriptor_match (descriptor, features[j]));
        }
    }
}

/******************************************************************************/

int
main (int   argc,
      char *argv[])
{
  g_test_init (&argc, &argv, G_TEST_OPTION_ISOLATE_DIRS, NULL);

  g_test_add_func ("/slide/media", slide_media);
  g_test_add_func ("/slide/title", slide_title);

  return g_test_run ();
}
