# Spiel

Create pretty talks.

![Spiel Screenshot](https://gitlab.gnome.org/feaneron/spiel/raw/main/data/opening-slide.png)

## Installing

The latest version from the main branch is available from the build artifacts.

- [Download Spiel Development Version](https://gitlab.gnome.org/feaneron/spiel/-/jobs/2442019/artifacts/file/com.feaneron.Spiel.flatpak)

This version will not get any automated updates after installation.

## Building

### GNOME Builder

GNOME Builder is the environment used for developing this application. It can use Flatpak manifests to create a consistent building and running environment cross-distro. Thus, it is highly
recommended you use it.

1. Download [GNOME Builder](https://flathub.org/apps/details/org.gnome.Builder).
2. In Builder, click the "Clone Repository" button at the bottom, using `git@gitlab.gnome.org/feaneron/spiel.git`
or `https://gitlab.gnome.org/feaneron/spiel.git` as the URL.
3. Click the build button at the top once the project is loaded.


## Installation

Depending on how you want it installed instructions can differ. If you
used GNOME Builder to build it, clicking the bar at the top window will 
open a submenu with "Export Bundle". This will create a Flatpak bundle, 
which can be installed on any system that supports Flatpak.

**In order for the Spiel flatpak to be able to read the directory images are
installed in, you must install a bundle.** Once you have a bundle installed,
development builds will work properly.

## Code Of Conduct

Spiel operates under the GNOME Code Of Conduct. See the full text of the Code
of Conduct [here](CODE_OF_CONDUCT.md).
