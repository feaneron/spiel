/*
 * spiel-preferences-window.c
 *
 * Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "spiel-preferences-window.h"

#include "spiel-application.h"
#include "spiel-source-view.h"

#include <gtksourceview/gtksource.h>

#define DEFAULT_TEXT \
"# Markdown\n" \
" 1. Numbered Lists\n" \
" * Unnumbered and [Links](https://gnome.org)\n" \
" * `Preformatted Text`\n" \
" * _Emphasis_ or *Emphasis* **Combined**\n" \
"> Block quotes too!"

struct _SpielPreferencesWindow
{
  AdwPreferencesDialog parent_instance;

  GtkFlowBox *scheme_group;
  GtkSourceView *source_view;

  GSettings *settings;
};

G_DEFINE_FINAL_TYPE (SpielPreferencesWindow, spiel_preferences_window, ADW_TYPE_PREFERENCES_DIALOG)


/*
 * Auxiliary functions
 */

static void
update_style_schemes (SpielPreferencesWindow *self)
{
  g_autofree char *current_scheme = NULL;
  GtkSourceStyleSchemeManager *scheme_manager;
  AdwStyleManager *style_manager;
  const char * const *scheme_ids;
  GtkFlowBoxChild *child;

  current_scheme = g_settings_get_string (self->settings, "style-scheme");

  style_manager = adw_style_manager_get_default ();

  scheme_manager = gtk_source_style_scheme_manager_get_default ();
  scheme_ids = gtk_source_style_scheme_manager_get_scheme_ids (scheme_manager);

  while ((child = gtk_flow_box_get_child_at_index (self->scheme_group, 0)) != NULL)
    gtk_flow_box_remove (self->scheme_group, GTK_WIDGET (child));

  for (size_t i = 0; scheme_ids && scheme_ids[i] != NULL; i++)
    {
      GtkSourceStyleScheme *scheme;
      GtkWidget *preview;
      gboolean scheme_is_dark;
      gboolean is_active;

      scheme = gtk_source_style_scheme_manager_get_scheme (scheme_manager, scheme_ids[i]);

      if (!scheme)
        continue;

      scheme_is_dark = g_strcmp0 (gtk_source_style_scheme_get_metadata (scheme, "variant"), "dark") == 0;
      if (scheme_is_dark != adw_style_manager_get_dark (style_manager))
        continue;

      if (adw_style_manager_get_dark (style_manager))
        {
          const char *light_variant = gtk_source_style_scheme_get_metadata (scheme, "light-variant");
          is_active = g_strcmp0 (light_variant, current_scheme) == 0;
        }
      else
        {
          is_active = g_strcmp0 (scheme_ids[i], current_scheme) == 0;
        }

      preview = gtk_source_style_scheme_preview_new (scheme);
      gtk_source_style_scheme_preview_set_selected (GTK_SOURCE_STYLE_SCHEME_PREVIEW (preview),
                                                    is_active);
      gtk_flow_box_append (self->scheme_group, preview);
    }
}


/*
 * Callbacks
 */

static void
on_style_scheme_activated_cb (GtkFlowBox             *flowbox,
                              GtkFlowBoxChild        *child,
                              SpielPreferencesWindow *self)
{
  GtkSourceStyleScheme *scheme;
  GtkWidget *preview;
  const char *scheme_id;

  preview = gtk_flow_box_child_get_child (child);
  g_assert (GTK_SOURCE_IS_STYLE_SCHEME_PREVIEW (preview));

  scheme = gtk_source_style_scheme_preview_get_scheme (GTK_SOURCE_STYLE_SCHEME_PREVIEW (preview));

  if (g_strcmp0 (gtk_source_style_scheme_get_metadata (scheme, "variant"), "dark") == 0)
    scheme_id = gtk_source_style_scheme_get_metadata (scheme, "light-variant");
  else
    scheme_id = gtk_source_style_scheme_get_id (scheme);

  g_assert (scheme_id != NULL);

  g_settings_set_string (self->settings, "style-scheme", scheme_id);

  for (int i = 0;
       (child = gtk_flow_box_get_child_at_index (self->scheme_group, i)) != NULL;
       i++)
    {
      GtkSourceStyleSchemePreview *child_preview;
      GtkSourceStyleScheme *child_scheme;

      child_preview = GTK_SOURCE_STYLE_SCHEME_PREVIEW (gtk_flow_box_child_get_child (child));
      child_scheme = gtk_source_style_scheme_preview_get_scheme (child_preview);

      gtk_source_style_scheme_preview_set_selected (child_preview, scheme == child_scheme);
    }
}


/*
 * GObject overrides
 */

static void
spiel_preferences_window_finalize (GObject *object)
{
  SpielPreferencesWindow *self = (SpielPreferencesWindow *)object;

  g_clear_object (&self->settings);

  G_OBJECT_CLASS (spiel_preferences_window_parent_class)->finalize (object);
}

static void
spiel_preferences_window_class_init (SpielPreferencesWindowClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  g_type_ensure (SPIEL_TYPE_SOURCE_VIEW);

  object_class->finalize = spiel_preferences_window_finalize;

  gtk_widget_class_set_template_from_resource (widget_class, "/com/feaneron/Spiel/spiel-preferences-window.ui");

  gtk_widget_class_bind_template_child (widget_class, SpielPreferencesWindow, scheme_group);
  gtk_widget_class_bind_template_child (widget_class, SpielPreferencesWindow, source_view);

  gtk_widget_class_bind_template_callback (widget_class, on_style_scheme_activated_cb);
}

static void
spiel_preferences_window_init (SpielPreferencesWindow *self)
{
  SpielApplication *application;
  GtkTextBuffer *buffer;

  application = SPIEL_APPLICATION (g_application_get_default ());
  self->settings = spiel_application_get_settings (application);

	gtk_widget_init_template (GTK_WIDGET (self));

  buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (self->source_view));
  gtk_text_buffer_set_text (buffer, DEFAULT_TEXT, -1);

  update_style_schemes (self);
}

AdwDialog *
spiel_preferences_window_new (void)
{
  return g_object_new (SPIEL_TYPE_PREFERENCES_WINDOW, NULL);
}
