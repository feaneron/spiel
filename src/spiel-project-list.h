/*
 * spiel-project-list.h
 *
 * Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gio/gio.h>

#include "spiel-types.h"

G_BEGIN_DECLS

#define SPIEL_TYPE_PROJECT_LIST (spiel_project_list_get_type())
G_DECLARE_FINAL_TYPE (SpielProjectList, spiel_project_list, SPIEL, PROJECT_LIST, GObject)

SpielProjectList * spiel_project_list_new (void);

const GError * spiel_project_list_get_error (SpielProjectList *self);
gboolean spiel_project_list_is_loading (SpielProjectList *self);

SpielProject * spiel_project_list_get_project_by_id (SpielProjectList *self,
                                                     const char       *project_id);

void spiel_project_list_create_project (SpielProjectList    *self,
                                        GCancellable        *cancellable,
                                        GAsyncReadyCallback  callback,
                                        gpointer             user_data);

SpielProject * spiel_project_list_create_project_finish (SpielProjectList  *self,
                                                         GAsyncResult      *result,
                                                         GError           **error);

void spiel_project_list_delete_projects (SpielProjectList    *self,
                                         GList               *projects,
                                         GCancellable        *cancellable,
                                         GAsyncReadyCallback  callback,
                                         gpointer             user_data);

gboolean spiel_project_list_delete_projects_finish (SpielProjectList  *self,
                                                    GAsyncResult      *result,
                                                    GError           **error);

G_END_DECLS
