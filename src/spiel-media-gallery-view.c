/*
 * spiel-media-gallery-view.c
 *
 * Copyright 2024 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "spiel-media-gallery-view.h"

#include "spiel-media-gallery.h"

struct _SpielMediaGalleryView
{
  AdwBin parent_instance;

  SpielMediaGallery *media_gallery;
};

G_DEFINE_FINAL_TYPE (SpielMediaGalleryView, spiel_media_gallery_view, ADW_TYPE_BIN)

enum
{
  PROP_0,
  PROP_MEDIA_GALLERY,
  N_PROPS,
};

static GParamSpec *properties [N_PROPS];

static void
spiel_media_gallery_view_finalize (GObject *object)
{
  SpielMediaGalleryView *self = (SpielMediaGalleryView *)object;

  g_clear_object (&self->media_gallery);

  G_OBJECT_CLASS (spiel_media_gallery_view_parent_class)->finalize (object);
}

static void
spiel_media_gallery_view_constructed (GObject *object)
{
  SpielMediaGalleryView *self = (SpielMediaGalleryView *)object;

  G_OBJECT_CLASS (spiel_media_gallery_view_parent_class)->constructed (object);
}

static void
spiel_media_gallery_view_get_property (GObject    *object,
                                       guint       prop_id,
                                       GValue     *value,
                                       GParamSpec *pspec)
{
  SpielMediaGalleryView *self = SPIEL_MEDIA_GALLERY_VIEW (object);

  switch (prop_id)
    {
    case PROP_MEDIA_GALLERY:
      g_value_set_object (value, self->media_gallery);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
spiel_media_gallery_view_set_property (GObject      *object,
                                       guint         prop_id,
                                       const GValue *value,
                                       GParamSpec   *pspec)
{
  SpielMediaGalleryView *self = SPIEL_MEDIA_GALLERY_VIEW (object);

  switch (prop_id)
    {
    case PROP_MEDIA_GALLERY:
      g_assert (self->media_gallery == NULL);
      self->media_gallery = g_value_dup_object (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
spiel_media_gallery_view_class_init (SpielMediaGalleryViewClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->finalize = spiel_media_gallery_view_finalize;
  object_class->constructed = spiel_media_gallery_view_constructed;
  object_class->get_property = spiel_media_gallery_view_get_property;
  object_class->set_property = spiel_media_gallery_view_set_property;

  properties[PROP_MEDIA_GALLERY] = g_param_spec_object ("media-gallery", NULL, NULL,
                                                        SPIEL_TYPE_MEDIA_GALLERY,
                                                        G_PARAM_READWRITE |
                                                        G_PARAM_CONSTRUCT_ONLY |
                                                        G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);

  gtk_widget_class_set_template_from_resource (widget_class, "/com/feaneron/Spiel/spiel-media-gallery-view.ui");
}

static void
spiel_media_gallery_view_init (SpielMediaGalleryView *self)
{
	gtk_widget_init_template (GTK_WIDGET (self));
}

GtkWidget *
spiel_media_gallery_view_new (SpielMediaGallery *media_gallery)
{
  return g_object_new (SPIEL_TYPE_MEDIA_GALLERY_VIEW,
                       "media-gallery", media_gallery,
                       NULL);
}
