/*
 * spiel-media-gallery.c
 *
 * Copyright 2024 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "spiel-media-gallery.h"

#include <gio/gio.h>

struct _SpielMediaGallery
{
  GObject parent_instance;

  GListStore *file_infos;
};

static void g_list_model_interface_init (GListModelInterface *iface);

G_DEFINE_FINAL_TYPE_WITH_CODE (SpielMediaGallery, spiel_media_gallery, G_TYPE_OBJECT,
                               G_IMPLEMENT_INTERFACE (G_TYPE_LIST_MODEL, g_list_model_interface_init))


/*
 * Callbacks
 */

static void
on_items_changed_cb (GListModel        *model,
                     unsigned int       position,
                     unsigned int       removed,
                     unsigned int       added,
                     SpielMediaGallery *self)
{
  g_list_model_items_changed (G_LIST_MODEL (self), position, removed, added);
}


/*
 * GListModel interface
 */

static gpointer
spiel_project_list_get_item (GListModel   *model,
                             unsigned int  i)
{
  SpielMediaGallery *self = (SpielMediaGallery *)model;

  g_assert (SPIEL_IS_MEDIA_GALLERY (model));

  return g_list_model_get_item (G_LIST_MODEL (self->file_infos), i);
}

static GType
spiel_project_list_get_item_type (GListModel *model)
{
  return G_TYPE_FILE_INFO;
}

static unsigned int
spiel_project_list_get_n_items (GListModel *model)
{
  SpielMediaGallery *self = (SpielMediaGallery *)model;

  g_assert (SPIEL_IS_MEDIA_GALLERY (model));

  return g_list_model_get_n_items (G_LIST_MODEL (self->file_infos));
}


static void
g_list_model_interface_init (GListModelInterface *iface)
{
  iface->get_n_items = spiel_project_list_get_n_items;
  iface->get_item_type = spiel_project_list_get_item_type;
  iface->get_item = spiel_project_list_get_item;
}


/*
 * GObject overrides
 */

static void
spiel_media_gallery_finalize (GObject *object)
{
  SpielMediaGallery *self = (SpielMediaGallery *)object;

  g_clear_object (&self->file_infos);

  G_OBJECT_CLASS (spiel_media_gallery_parent_class)->finalize (object);
}

static void
spiel_media_gallery_class_init (SpielMediaGalleryClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = spiel_media_gallery_finalize;
}

static void
spiel_media_gallery_init (SpielMediaGallery *self)
{
  self->file_infos = g_list_store_new (G_TYPE_FILE_INFO);
  g_signal_connect (self->file_infos, "items-changed", G_CALLBACK (on_items_changed_cb), self);
}

SpielMediaGallery *
spiel_media_gallery_new (void)
{
  return g_object_new (SPIEL_TYPE_MEDIA_GALLERY, NULL);
}
