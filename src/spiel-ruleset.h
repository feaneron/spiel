/*
 * spiel-ruleset.h
 *
 * Copyright 2024 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>

#include "spiel-types.h"

G_BEGIN_DECLS

typedef enum
{
  SPIEL_RULESET_ERROR_NO_RULES,
} SpielRulesetError;

#define SPIEL_RULESET_ERROR (spiel_ruleset_error_quark ())

GQuark spiel_ruleset_error_quark (void);


#define SPIEL_TYPE_RULESET (spiel_ruleset_get_type())
G_DECLARE_FINAL_TYPE (SpielRuleset, spiel_ruleset, SPIEL, RULESET, GObject)

SpielRuleset *spiel_ruleset_new (const char * const  *required_features,
                                 const char * const  *optional_features,
                                 const char * const  *denied_features,
                                 GError             **error);

const char * const * spiel_ruleset_get_required_features (SpielRuleset *self);
const char * const * spiel_ruleset_get_optional_features (SpielRuleset *self);
const char * const * spiel_ruleset_get_denied_features (SpielRuleset *self);

G_END_DECLS
