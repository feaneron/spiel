/*
 * spiel-slide-rendition.c
 *
 * Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "spiel-slide-rendition.h"

#include "spiel-application.h"
#include "spiel-enum-types.h"
#include "spiel-slide.h"
#include "spiel-slide-layout-registry-private.h"
#include "spiel-slide-layout.h"

/* TODO: configurable sizes */
#define TARGET_WIDTH  1920.0
#define TARGET_HEIGHT 1080.0

struct _SpielSlideRendition
{
  GtkWidget parent_instance;

  SpielSlideRenditionType rendition_type;

  SpielSlide *slide;
  SpielSlideLayout *layout;

  gulong slide_changed_id;
};

G_DEFINE_FINAL_TYPE (SpielSlideRendition, spiel_slide_rendition, GTK_TYPE_WIDGET)

enum {
  PROP_0,
  PROP_RENDITION_TYPE,
  PROP_SLIDE,
  N_PROPS,
};

static GParamSpec *properties[N_PROPS] = { NULL, };


/*
 * Auxiliary functions
 */

static inline void
clear_layout (SpielSlideRendition *self)
{
  if (self->layout)
    {
      gtk_widget_unparent (GTK_WIDGET (self->layout));
      self->layout = NULL;
    }
}

static const char *
css_class_from_rendition_type (SpielSlideRenditionType rendition_type)
{
  switch (rendition_type)
    {
    case SPIEL_SLIDE_RENDITION_THUMBNAIL:
      return "thumbnail";

    case SPIEL_SLIDE_RENDITION_PRESENTATION:
      return "presentation";

    default:
      g_assert_not_reached ();
    }
}

static void
recreate_layout (SpielSlideRendition *self)
{
  if (self->slide)
    {
      SpielSlideLayoutRegistry *registry;
      const SpielSlideFeature **features;
      GType layout_type;

      registry = spiel_application_get_layout_registry (SPIEL_APPLICATION_DEFAULT);
      features = spiel_slide_get_features (self->slide);
      layout_type = spiel_slide_layout_registry_match (registry, features);

      if (!self->layout || G_OBJECT_TYPE (self->layout) != layout_type)
        {
          clear_layout (self);

          if (layout_type != G_TYPE_NONE)
            {
              self->layout = g_object_new (layout_type, NULL);
              gtk_widget_set_parent (GTK_WIDGET (self->layout), GTK_WIDGET (self));
            }
        }

      if (self->layout)
        spiel_slide_layout_apply (self->layout, features);
    }
  else
    {
      clear_layout (self);
    }
}


/*
 * Callbacks
 */

static void
on_slide_changed_cb (SpielSlide          *slide,
                     GParamSpec          *pspec,
                     SpielSlideRendition *self)
{
  recreate_layout (self);
}


/*
 * GtkWidget overrides
 */

static GtkSizeRequestMode
spiel_slide_rendition_get_request_mode (GtkWidget *widget)
{
  return GTK_SIZE_REQUEST_HEIGHT_FOR_WIDTH;
}

static void
spiel_slide_rendition_measure (GtkWidget      *widget,
                               GtkOrientation  orientation,
                               int             for_size,
                               int            *minimum,
                               int            *natural,
                               int            *minimum_baseline,
                               int            *natural_baseline)
{
  double ratio = TARGET_WIDTH / TARGET_HEIGHT;

  switch (orientation)
    {
    case GTK_ORIENTATION_HORIZONTAL:
      if (minimum)
        *minimum = 1;
      if (natural)
        *natural = TARGET_WIDTH;
      break;

    case GTK_ORIENTATION_VERTICAL:
      if (minimum)
        *minimum = MAX (ceil (for_size / ratio), 1);
      if (natural)
        *natural = MAX (ceil (for_size / ratio), 1);
      break;

    default:
      g_assert_not_reached ();
    }
}

static void
spiel_slide_rendition_size_allocate (GtkWidget *widget,
                                     int        width,
                                     int        height,
                                     int        baseline)
{
  SpielSlideRendition *self = (SpielSlideRendition *) widget;

  g_assert (SPIEL_IS_SLIDE_RENDITION (self));

  if (!self->layout)
    return;

  switch (self->rendition_type)
    {
    case SPIEL_SLIDE_RENDITION_THUMBNAIL:
      {
        graphene_point_t offset;
        GskTransform *transform;
        double scale_x;
        double scale_y;
        double scale;

        scale_x = width / TARGET_WIDTH;
        scale_y = height / TARGET_HEIGHT;
        scale = MIN (scale_x, scale_y);

        graphene_point_init (&offset,
                             (width - TARGET_WIDTH * scale) / 2.0,
                             (height - TARGET_HEIGHT * scale) / 2.0);

        transform = gsk_transform_translate (NULL, &offset);
        transform = gsk_transform_scale (transform, scale, scale);

        gtk_widget_allocate (GTK_WIDGET (self->layout), TARGET_WIDTH, TARGET_HEIGHT, baseline, transform);
      }
      break;

    case SPIEL_SLIDE_RENDITION_PRESENTATION:
      gtk_widget_allocate (GTK_WIDGET (self->layout), width, height, baseline, NULL);
      break;

    default:
      g_assert_not_reached ();
    }
}


/*
 * GObject overrides
 */

static void
spiel_slide_rendition_dispose (GObject *object)
{
  SpielSlideRendition *self = (SpielSlideRendition *)object;

  clear_layout (self);

  g_clear_signal_handler (&self->slide_changed_id, self->slide);
  g_clear_object (&self->slide);

  G_OBJECT_CLASS (spiel_slide_rendition_parent_class)->dispose (object);
}

static void
spiel_slide_rendition_get_property (GObject    *object,
                                    guint       prop_id,
                                    GValue     *value,
                                    GParamSpec *pspec)
{
  SpielSlideRendition *self = SPIEL_SLIDE_RENDITION (object);

  switch (prop_id)
    {
    case PROP_RENDITION_TYPE:
      g_value_set_enum (value, self->rendition_type);
      break;

    case PROP_SLIDE:
      g_value_set_object (value, self->slide);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
spiel_slide_rendition_set_property (GObject      *object,
                                    guint         prop_id,
                                    const GValue *value,
                                    GParamSpec   *pspec)
{
  SpielSlideRendition *self = SPIEL_SLIDE_RENDITION (object);

  switch (prop_id)
    {
    case PROP_RENDITION_TYPE:
      spiel_slide_rendition_set_rendition_type (self, g_value_get_enum (value));
      break;

    case PROP_SLIDE:
      spiel_slide_rendition_set_slide (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
spiel_slide_rendition_class_init (SpielSlideRenditionClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = spiel_slide_rendition_dispose;
  object_class->get_property = spiel_slide_rendition_get_property;
  object_class->set_property = spiel_slide_rendition_set_property;

  widget_class->get_request_mode = spiel_slide_rendition_get_request_mode;
  widget_class->measure = spiel_slide_rendition_measure;
  widget_class->size_allocate = spiel_slide_rendition_size_allocate;

  properties[PROP_RENDITION_TYPE] =
    g_param_spec_enum ("rendition-type", "", "",
                       SPIEL_TYPE_SLIDE_RENDITION_TYPE,
                       SPIEL_SLIDE_RENDITION_THUMBNAIL,
                       G_PARAM_READWRITE |
                       G_PARAM_EXPLICIT_NOTIFY |
                       G_PARAM_STATIC_STRINGS);

  properties[PROP_SLIDE] =
    g_param_spec_object ("slide", "", "",
                         SPIEL_TYPE_SLIDE,
                         G_PARAM_READWRITE |
                         G_PARAM_EXPLICIT_NOTIFY |
                         G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);

  gtk_widget_class_set_css_name (widget_class, "rendition");
}

static void
spiel_slide_rendition_init (SpielSlideRendition *self)
{
  self->rendition_type = SPIEL_SLIDE_RENDITION_THUMBNAIL;
  gtk_widget_add_css_class (GTK_WIDGET (self), css_class_from_rendition_type (self->rendition_type));
}

SpielSlideRenditionType
spiel_slide_rendition_get_rendition_type (SpielSlideRendition *self)
{
  g_return_val_if_fail (SPIEL_IS_SLIDE_RENDITION (self), 0);

  return self->rendition_type;
}

void
spiel_slide_rendition_set_rendition_type (SpielSlideRendition     *self,
                                          SpielSlideRenditionType  rendition_type)
{
  g_return_if_fail (SPIEL_IS_SLIDE_RENDITION (self));

  if (self->rendition_type == rendition_type)
    return;

  gtk_widget_remove_css_class (GTK_WIDGET (self), css_class_from_rendition_type (self->rendition_type));
  gtk_widget_add_css_class (GTK_WIDGET (self), css_class_from_rendition_type (rendition_type));

  self->rendition_type = rendition_type;
  gtk_widget_queue_resize (GTK_WIDGET (self));

  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_RENDITION_TYPE]);
}

SpielSlide *
spiel_slide_rendition_get_slide (SpielSlideRendition *self)
{
  g_return_val_if_fail (SPIEL_IS_SLIDE_RENDITION (self), NULL);

  return self->slide;
}

void
spiel_slide_rendition_set_slide (SpielSlideRendition *self,
                                 SpielSlide          *slide)
{
  g_return_if_fail (SPIEL_IS_SLIDE_RENDITION (self));

  if (self->slide != slide)
    {
      if (self->slide)
        g_clear_signal_handler (&self->slide_changed_id, self->slide);

      g_set_object (&self->slide, slide);

      if (self->slide)
        {
          self->slide_changed_id = g_signal_connect (self->slide,
                                                     "notify",
                                                     G_CALLBACK (on_slide_changed_cb),
                                                     self);
        }

      recreate_layout (self);

      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_SLIDE]);
    }
}
