/*
 * spiel-presenter-note-row.c
 *
 * Copyright 2024 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "spiel-presenter-note-row.h"

#include "spiel-slide-feature.h"

struct _SpielPresenterNoteRow
{
  AdwPreferencesRow parent_instance;

  SpielSlideFeature *note;
};

G_DEFINE_FINAL_TYPE (SpielPresenterNoteRow, spiel_presenter_note_row, ADW_TYPE_PREFERENCES_ROW)

enum {
  PROP_0,
  PROP_NOTE,
  N_PROPS,
};

static GParamSpec *properties [N_PROPS];


/*
 * GObject overrides
 */

static void
spiel_presenter_note_row_dispose (GObject *object)
{
  SpielPresenterNoteRow *self = (SpielPresenterNoteRow *)object;

  g_clear_pointer (&self->note, spiel_slide_feature_free);

  gtk_widget_dispose_template (GTK_WIDGET (self), SPIEL_TYPE_PRESENTER_NOTE_ROW);

  G_OBJECT_CLASS (spiel_presenter_note_row_parent_class)->dispose (object);
}

static void
spiel_presenter_note_row_constructed (GObject *object)
{
  SpielPresenterNoteRow *self = (SpielPresenterNoteRow *)object;

  G_OBJECT_CLASS (spiel_presenter_note_row_parent_class)->constructed (object);

  adw_preferences_row_set_title (ADW_PREFERENCES_ROW (self), spiel_slide_feature_get_note (self->note));
}

static void
spiel_presenter_note_row_get_property (GObject    *object,
                                       guint       prop_id,
                                       GValue     *value,
                                       GParamSpec *pspec)
{
  SpielPresenterNoteRow *self = SPIEL_PRESENTER_NOTE_ROW (object);

  switch (prop_id)
    {
    case PROP_NOTE:
      g_value_set_boxed (value, self->note);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
spiel_presenter_note_row_set_property (GObject      *object,
                                       guint         prop_id,
                                       const GValue *value,
                                       GParamSpec   *pspec)
{
  SpielPresenterNoteRow *self = SPIEL_PRESENTER_NOTE_ROW (object);

  switch (prop_id)
    {
    case PROP_NOTE:
      g_assert (self->note == NULL);
      self->note = g_value_dup_boxed (value);
      g_assert (self->note != NULL);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
spiel_presenter_note_row_class_init (SpielPresenterNoteRowClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = spiel_presenter_note_row_dispose;
  object_class->constructed = spiel_presenter_note_row_constructed;
  object_class->get_property = spiel_presenter_note_row_get_property;
  object_class->set_property = spiel_presenter_note_row_set_property;

  properties[PROP_NOTE] =
    g_param_spec_boxed ("note", NULL, NULL,
                        SPIEL_TYPE_SLIDE_FEATURE,
                        G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);

  gtk_widget_class_set_template_from_resource (widget_class, "/com/feaneron/Spiel/spiel-presenter-note-row.ui");
}

static void
spiel_presenter_note_row_init (SpielPresenterNoteRow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}

GtkWidget *
spiel_presenter_note_row_new (const SpielSlideFeature *note)
{
  g_assert (spiel_slide_feature_get_feature_type (note) == SPIEL_FEATURE_NOTE);

  return g_object_new (SPIEL_TYPE_PRESENTER_NOTE_ROW,
                       "note", note,
                       NULL);
}

