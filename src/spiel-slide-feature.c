/*
 * spiel-slide-feature.c
 *
 * Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "spiel-slide-feature-private.h"

G_DEFINE_BOXED_TYPE (SpielSlideFeature, spiel_slide_feature, spiel_slide_feature_copy, spiel_slide_feature_free)

struct _SpielSlideFeature
{
  SpielFeatureType feature;

  union {

    struct {
      char *text;
      uint32_t level;
    } title;

    struct {
      char *text;
    } note;

  } data;
};

/**
 * spiel_slide_feature_new:
 *
 * Creates a new #SpielSlideFeature.
 *
 * Returns: (transfer full): A newly created #SpielSlideFeature
 */

SpielSlideFeature *
spiel_slide_feature_new_title (char     *text,
                               uint32_t  level)
{
  SpielSlideFeature *self;

  self = g_slice_new0 (SpielSlideFeature);
  self->feature = SPIEL_FEATURE_TITLE;
  self->data.title.level = level;
  self->data.title.text = text;

  return self;
}

SpielSlideFeature *
spiel_slide_feature_new_media (void)
{
  SpielSlideFeature *self;

  self = g_slice_new0 (SpielSlideFeature);
  self->feature = SPIEL_FEATURE_MEDIA;

  return self;
}

SpielSlideFeature *
spiel_slide_feature_new_note (char *note)
{
  SpielSlideFeature *self;

  self = g_slice_new0 (SpielSlideFeature);
  self->feature = SPIEL_FEATURE_NOTE;
  self->data.note.text = note;

  return self;
}

SpielFeatureType
spiel_slide_feature_get_feature_type (const SpielSlideFeature *self)
{
  g_assert (self != NULL);

  return self->feature;
}

uint32_t
spiel_slide_feature_get_title_level (const SpielSlideFeature *self)
{
  g_assert (self != NULL);
  g_assert (self->feature == SPIEL_FEATURE_TITLE);

  return self->data.title.level;
}

const char *
spiel_slide_feature_get_title_text (const SpielSlideFeature *self)
{
  g_assert (self != NULL);
  g_assert (self->feature == SPIEL_FEATURE_TITLE);

  return self->data.title.text;
}

const char *
spiel_slide_feature_get_note (const SpielSlideFeature *self)
{
  g_assert (self != NULL);
  g_assert (self->feature == SPIEL_FEATURE_NOTE);

  return self->data.note.text;
}

unsigned int
spiel_slide_feature_hash_shallow (gconstpointer data)
{
  const SpielSlideFeature *self = (const SpielSlideFeature *)data;
  unsigned int hash = 0;

  hash |= self->feature;

  switch (self->feature)
    {
    case SPIEL_FEATURE_TITLE:
      hash |= (self->data.title.level << 16);
      break;

    case SPIEL_FEATURE_MEDIA:
    case SPIEL_FEATURE_NOTE:
      break;

    default:
      g_assert_not_reached ();
    }

  return hash;
}

unsigned int
spiel_slide_feature_hash_deep (gconstpointer data)
{
  const SpielSlideFeature *self = (const SpielSlideFeature *)data;
  unsigned int hash = 0;

  hash |= self->feature;

  switch (self->feature)
    {
    case SPIEL_FEATURE_TITLE:
      hash |= (self->data.title.level << 16);
      hash ^= g_str_hash (self->data.title.text);
      break;

    case SPIEL_FEATURE_MEDIA:
      break;

    case SPIEL_FEATURE_NOTE:
      hash ^= g_str_hash (self->data.note.text);
      break;

    default:
      g_assert_not_reached ();
    }

  return hash;
}

/**
 * spiel_slide_feature_equal_shallow:
 * @a: a #SpielSlideFeature
 * @b: a #SpielSlideFeature
 *
 * Compares @a and @b without comparing their contents.
 *
 * Returns: whether @a is equal to @b
 */
gboolean
spiel_slide_feature_equal_shallow (gconstpointer a,
                                   gconstpointer b)
{
  const SpielSlideFeature *feature_a = (const SpielSlideFeature *)a;
  const SpielSlideFeature *feature_b = (const SpielSlideFeature *)b;

  if (feature_a->feature != feature_b->feature)
    return FALSE;

  switch (feature_a->feature)
    {
    case SPIEL_FEATURE_TITLE:
      return feature_a->data.title.level == feature_b->data.title.level;

    case SPIEL_FEATURE_MEDIA:
    case SPIEL_FEATURE_NOTE:
      return TRUE;

    default:
      g_assert_not_reached ();
    }

  return TRUE;
}

/**
 * spiel_slide_feature_equal_deep:
 * @a: a #SpielSlideFeature
 * @b: a #SpielSlideFeature
 *
 * Compares @a and @b, including their contents.
 *
 * Returns: whether @a is equal to @b
 */
gboolean
spiel_slide_feature_equal_deep (gconstpointer a,
                                gconstpointer b)
{

  const SpielSlideFeature *feature_a = (const SpielSlideFeature *)a;
  const SpielSlideFeature *feature_b = (const SpielSlideFeature *)b;

  if (!spiel_slide_feature_equal_shallow (a, b))
    return FALSE;

  switch (feature_a->feature)
    {
    case SPIEL_FEATURE_TITLE:
      return g_strcmp0 (feature_a->data.title.text, feature_b->data.title.text) == 0;

    case SPIEL_FEATURE_MEDIA:
      return TRUE;

    case SPIEL_FEATURE_NOTE:
      return g_strcmp0 (feature_a->data.note.text, feature_b->data.note.text) == 0;

    default:
      g_assert_not_reached ();
    }

  return TRUE;
}

/**
 * spiel_slide_feature_copy:
 * @self: a #SpielSlideFeature
 *
 * Makes a deep copy of a #SpielSlideFeature.
 *
 * Returns: (transfer full): A newly created #SpielSlideFeature with the same
 *   contents as @self
 */
SpielSlideFeature *
spiel_slide_feature_copy (const SpielSlideFeature *self)
{
  g_autoptr (SpielSlideFeature) copy = NULL;

  g_return_val_if_fail (self, NULL);

  copy = g_memdup2 (self, sizeof (SpielSlideFeature));

  switch (self->feature)
    {
    case SPIEL_FEATURE_TITLE:
      copy->data.title.text = g_strdup (self->data.title.text);
      break;

    case SPIEL_FEATURE_MEDIA:
      break;

    case SPIEL_FEATURE_NOTE:
      copy->data.note.text = g_strdup (self->data.note.text);
      break;

    default:
      g_assert_not_reached ();
    }

  return g_steal_pointer (&copy);
}

/**
 * spiel_slide_feature_free:
 * @self: a #SpielSlideFeature
 *
 * Frees a #SpielSlideFeature allocated using spiel_slide_feature_new()
 * or spiel_slide_feature_copy().
 */
void
spiel_slide_feature_free (SpielSlideFeature *self)
{
  g_return_if_fail (self);

  switch (self->feature)
    {
    case SPIEL_FEATURE_TITLE:
      g_clear_pointer (&self->data.title.text, g_free);
      break;

    case SPIEL_FEATURE_MEDIA:
      break;

    case SPIEL_FEATURE_NOTE:
      g_clear_pointer (&self->data.note.text, g_free);
      break;

    default:
      g_assert_not_reached ();
    }

  g_slice_free (SpielSlideFeature, self);
}
