/*
 * spiel-slide-layout-factory.c
 *
 * Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "spiel-slide-layout-factory.h"

G_DEFINE_INTERFACE (SpielSlideLayoutFactory, spiel_slide_layout_factory, PEAS_TYPE_EXTENSION_BASE)

static void
spiel_slide_layout_factory_default_init (SpielSlideLayoutFactoryInterface *iface)
{
}

/**
 * spiel_slide_layout_factory_register_layouts:
 * @self: a #SpielSlideLayoutFactory
 */
void
spiel_slide_layout_factory_register_layouts (SpielSlideLayoutFactory  *self,
                                             SpielSlideLayoutRegistry *registry)
{
  g_return_if_fail (SPIEL_IS_SLIDE_LAYOUT_FACTORY (self));
  g_return_if_fail (SPIEL_SLIDE_LAYOUT_FACTORY_GET_IFACE (self)->register_layouts);

  SPIEL_SLIDE_LAYOUT_FACTORY_GET_IFACE (self)->register_layouts (self, registry);
}
