/*
 * spiel-recoloring.c
 *
 * Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "spiel-recoloring.h"

#include <math.h>

#define SHARED_CSS \
  "  --card-fg-color: var(--window-fg-color);\n" \
  "  --headerbar-border-color: var(--window-fg-color);\n" \
  "  --popover-fg-color: var(--window-fg-color);\n" \
  "  --dialog-fg-color: var(--window-fg-color);\n" \
  "  --dark-fill-bg-color: var(--headerbar-bg-color);\n" \
  "  --view-fg-color: var(--window-fg-color);\n"
#define LIGHT_CSS_SUFFIX \
  "  --card_bg_color: rgb(255, 255, 255 / 80%);\n"
#define DARK_CSS_SUFFIX \
  "  --card_bg_color: rgb(255, 255, 255 / 8%);\n"

enum {
  FOREGROUND,
  BACKGROUND,
};

static gboolean
get_color (GtkSourceStyleScheme *scheme,
           const char           *style_name,
           GdkRGBA              *color,
           int                   kind)
{
  GtkSourceStyle *style;
  g_autofree char *fg = NULL;
  g_autofree char *bg = NULL;
  gboolean fg_set = FALSE;
  gboolean bg_set = FALSE;

  g_assert (GTK_SOURCE_IS_STYLE_SCHEME (scheme));
  g_assert (style_name != NULL);

  if (!(style = gtk_source_style_scheme_get_style (scheme, style_name)))
    return FALSE;

  g_object_get (style,
                "foreground", &fg,
                "foreground-set", &fg_set,
                "background", &bg,
                "background-set", &bg_set,
                NULL);

  if (kind == FOREGROUND && fg && fg_set)
    gdk_rgba_parse (color, fg);
  else if (kind == BACKGROUND && bg && bg_set)
    gdk_rgba_parse (color, bg);
  else
    return FALSE;

  return color->alpha >= .1;
}

static inline gboolean
get_foreground (GtkSourceStyleScheme *scheme,
                const char           *style_name,
                GdkRGBA              *fg)
{
  return get_color (scheme, style_name, fg, FOREGROUND);
}

static inline gboolean
get_background (GtkSourceStyleScheme *scheme,
                const char           *style_name,
                GdkRGBA              *bg)
{
  return get_color (scheme, style_name, bg, BACKGROUND);
}

static gboolean
get_metadata_color (GtkSourceStyleScheme *scheme,
                    const char           *key,
                    GdkRGBA              *color)
{
  const char *str;

  if ((str = gtk_source_style_scheme_get_metadata (scheme, key)))
    return gdk_rgba_parse (color, str);

  return FALSE;
}

static void
set_css_color (GString       *str,
               const char    *name,
               const GdkRGBA *color)
{
  g_autofree char *color_str = NULL;
  GdkRGBA opaque;

  g_assert (str != NULL);
  g_assert (name != NULL);
  g_assert (color != NULL);

  opaque = *color;
  opaque.alpha = 1.0f;

  color_str = gdk_rgba_to_string (&opaque);
  g_string_append_printf (str, "  --%s: %s;\n", name, color_str);
}

static void
set_css_color_mixed (GString       *str,
                     const char    *name,
                     const GdkRGBA *a,
                     const GdkRGBA *b,
                     double         level)
{
  g_autofree char *a_str = NULL;
  g_autofree char *b_str = NULL;

  g_assert (str != NULL);
  g_assert (name != NULL);
  g_assert (a != NULL);
  g_assert (b != NULL);

  a_str = gdk_rgba_to_string (a);
  b_str = gdk_rgba_to_string (b);

  g_string_append_printf (str,
                          "  --%s: color-mix(in srgb, %s %d%%, %s);\n",
                          name,
                          a_str,
                          (int) CLAMP (100 - level * 100, 0, 100),
                          b_str);
}

#if 0
static inline void
premix_colors (GdkRGBA       *dest,
               const GdkRGBA *fg,
               const GdkRGBA *bg,
               gboolean       bg_set,
               double         alpha)
{
  g_assert (dest != NULL);
  g_assert (fg != NULL);
  g_assert (bg != NULL || bg_set == FALSE);
  g_assert (alpha >= 0.0 && alpha <= 1.0);

  if (bg_set)
    {
      dest->red = ((1 - alpha) * bg->red) + (alpha * fg->red);
      dest->green = ((1 - alpha) * bg->green) + (alpha * fg->green);
      dest->blue = ((1 - alpha) * bg->blue) + (alpha * fg->blue);
      dest->alpha = 1.0;
    }
  else
    {
      *dest = *fg;
      dest->alpha = alpha;
    }
}
#endif

static gboolean
source_style_scheme_is_dark (GtkSourceStyleScheme *scheme)
{
  const char *id = gtk_source_style_scheme_get_id (scheme);
  const char *variant = gtk_source_style_scheme_get_metadata (scheme, "variant");
  GdkRGBA text_bg;

  if (g_strcmp0 (variant, "light") == 0)
    return FALSE;
  else if (g_strcmp0 (variant, "dark") == 0)
    return TRUE;
  else if (strstr (id, "-dark") != NULL)
    return TRUE;

  if (get_background (scheme, "text", &text_bg))
    {
      /* http://alienryderflex.com/hsp.html */
      double r = text_bg.red * 255.0;
      double g = text_bg.green * 255.0;
      double b = text_bg.blue * 255.0;
      double hsp = sqrt (0.299 * (r * r) +
                         0.587 * (g * g) +
                         0.114 * (b * b));

      return hsp <= 127.5;
    }

  return FALSE;
}

char *
spiel_recoloring_generate_css (GtkSourceStyleScheme *style_scheme)
{
  static const GdkRGBA black = {0,0,0,1};
  static const GdkRGBA white = {1,1,1,1};
  const GdkRGBA *alt;
  GdkRGBA text_bg;
  GdkRGBA text_fg;
  GdkRGBA right_margin;
  const char *id;
  const char *name;
  GString *str;
  GdkRGBA color;
  gboolean is_dark;
  gboolean has_fg;
  gboolean has_bg;

  g_return_val_if_fail (GTK_SOURCE_IS_STYLE_SCHEME (style_scheme), NULL);

  /* Don't restyle Adwaita as we already have it */
  id = gtk_source_style_scheme_get_name (style_scheme);
  if (g_str_has_prefix (id, "Adwaita"))
    return NULL;

  name = gtk_source_style_scheme_get_name (style_scheme);
  is_dark = source_style_scheme_is_dark (style_scheme);
  alt = is_dark ? &white : &black;

  str = g_string_new (NULL);
  g_string_append_printf (str, "/* %s */\n", name);
  g_string_append (str, ":root { \n");
  g_string_append (str, SHARED_CSS);

  /* TODO: Improve error checking and fallbacks */

  has_bg = get_background (style_scheme, "text", &text_bg);
  has_fg = get_foreground (style_scheme, "text", &text_fg);
  get_background (style_scheme, "right-margin", &right_margin);
  right_margin.alpha = 1;

  if (get_metadata_color (style_scheme, "window_bg_color", &color))
    set_css_color (str, "window-bg-color", &color);
  else if (has_bg && has_fg && is_dark)
    set_css_color (str, "window-bg-color", &text_bg);
  else if (has_bg && has_fg)
    set_css_color_mixed (str, "window-bg-color", &text_bg, &text_fg, .03);
  else if (is_dark)
    set_css_color_mixed (str, "window-bg-color", &text_bg, &white, .025);
  else
    set_css_color_mixed (str, "window-bg-color", &text_bg, &white, .1);

  if (get_metadata_color (style_scheme, "window_fg_color", &color))
    set_css_color (str, "window-fg-color", &color);
  else if (has_bg && has_fg)
    set_css_color (str, "window-fg-color", &text_fg);
  else if (is_dark)
    set_css_color_mixed (str, "window-fg-color", &text_bg, alt, .05);
  else
    set_css_color_mixed (str, "window-fg-color", &text_bg, alt, .025);

  if (get_metadata_color (style_scheme, "headerbar_bg_color", &color))
    set_css_color (str, "headerbar-bg-color", &color);
  else if (is_dark)
    set_css_color_mixed (str, "headerbar-bg-color", &text_bg, &white, .07);
  else
    set_css_color_mixed (str, "headerbar-bg-color", &text_bg, &white, .25);

  if (get_metadata_color (style_scheme, "headerbar_fg_color", &color))
    set_css_color (str, "headerbar-fg-color", &color);
  else if (has_bg && has_fg)
    set_css_color (str, "headerbar-fg-color", &text_fg);
  else if (is_dark)
    set_css_color_mixed (str, "headerbar-fg-color", &text_bg, alt, .05);
  else
    set_css_color_mixed (str, "headerbar-fg-color", &text_bg, alt, .025);

  set_css_color_mixed (str, "popover-bg-color", &text_bg, &white, is_dark ? .07 : .25);

  if (is_dark)
    set_css_color_mixed (str, "dialog_bg_color", &text_bg, &white, .07);
  else
    set_css_color (str, "dialog-bg-color", &text_bg);

  if (is_dark)
    set_css_color_mixed (str, "view-bg-color", &text_bg, &black, .1);
  else
    set_css_color_mixed (str, "view-bg-color", &text_bg, &white, .3);

  set_css_color (str, "view-fg-color", &text_fg);

  if (is_dark)
    {
      set_css_color_mixed (str, "sidebar-bg-color", &text_bg, &black, .2);
      set_css_color_mixed (str, "secondary-sidebar-bg-color", &text_bg, &black, .2);
    }
  else
    {
      set_css_color_mixed (str, "sidebar-bg-color", &text_bg, &white, .2);
      set_css_color_mixed (str, "secondary-sidebar-bg-color", &text_bg, &white, .2);
    }

  set_css_color_mixed (str, "sidebar-backdrop-color", &text_bg, &black, is_dark ? .3 : .03);
  set_css_color_mixed (str, "secondary-sidebar-backdrop-color", &text_bg, &black, is_dark ? .3 : .03);
  set_css_color (str, "sidebar-fg-color", &text_fg);
  set_css_color (str, "secondary-sidebar-fg-color", &text_fg);

  if (get_metadata_color (style_scheme, "accent_bg_color", &color) ||
      get_background (style_scheme, "selection", &color))
    set_css_color (str, "accent-bg-color", &color);

  if (get_metadata_color (style_scheme, "accent_fg_color", &color) ||
      get_foreground (style_scheme, "selection", &color))
    set_css_color (str, "accent-fg-color", &color);

  if (get_metadata_color (style_scheme, "accent_color", &color))
    {
      set_css_color (str, "accent-color", &color);
    }
  else if (get_metadata_color (style_scheme, "accent_bg_color", &color) ||
           get_background (style_scheme, "selection", &color))
    {
      color.alpha = 1;
      set_css_color_mixed (str, "accent-color", &color, alt, .1);
    }

  if (is_dark)
    g_string_append (str, DARK_CSS_SUFFIX);
  else
    g_string_append (str, LIGHT_CSS_SUFFIX);

  g_string_append (str, "}");

  return g_string_free (str, FALSE);
}
