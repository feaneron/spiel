/*
 * spiel-slide.h
 *
 * Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>

#include "spiel-types.h"

G_BEGIN_DECLS

#define SPIEL_TYPE_SLIDE (spiel_slide_get_type())
G_DECLARE_FINAL_TYPE (SpielSlide, spiel_slide, SPIEL, SLIDE, GObject)

SpielSlide * spiel_slide_new (const char *text);

const char * spiel_slide_get_text (SpielSlide *self);
void         spiel_slide_set_text (SpielSlide *self,
                                   const char *text);

const SpielSlideFeature ** spiel_slide_get_features (SpielSlide *self);

void spiel_slide_get_marks (SpielSlide   *self,
                            GtkTextMark **out_start,
                            GtkTextMark **out_end);

void spiel_slide_set_marks (SpielSlide   *self,
                            GtkTextMark  *start,
                            GtkTextMark  *end);

G_END_DECLS
