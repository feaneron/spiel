/*
 * spiel-presentation-window.c
 *
 * Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "spiel-presentation-window.h"

#include "spiel-presentation-controller.h"
#include "spiel-project.h"
#include "spiel-slide.h"
#include "spiel-slide-rendition.h"

struct _SpielPresentationWindow
{
  AdwWindow parent_instance;

  GtkRevealer *bottom_controls_revealer;
  SpielSlideRendition *rendition;
  GtkLabel *slide_counter_label;
  GtkRevealer *top_controls_revealer;

  SpielPresentationController *controller;

  GSimpleActionGroup *action_group;

  graphene_point_t cursor_position;
  guint hide_controls_timeout_id;
  gboolean hovering_controls;
};

static gboolean hide_controls_after_timeout_cb (gpointer data);

G_DEFINE_FINAL_TYPE (SpielPresentationWindow, spiel_presentation_window, ADW_TYPE_WINDOW)

enum {
  PROP_0,
  PROP_PRESENTATION_CONTROLLER,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

/*
 * Auxiliary functions
 */

static void
update_actions (SpielPresentationWindow *self)
{
  SpielProject *project;
  GListModel *slides;
  GAction *action;
  uint32_t current_slide;
  size_t n_slides;

  project = spiel_presentation_controller_get_project (self->controller);
  current_slide = spiel_presentation_controller_get_current_slide (self->controller);

  slides = spiel_project_get_slides (project);
  n_slides = g_list_model_get_n_items (slides);

  action = g_action_map_lookup_action (G_ACTION_MAP (self->action_group), "previous");
  g_simple_action_set_enabled (G_SIMPLE_ACTION (action), current_slide > 0);

  action = g_action_map_lookup_action (G_ACTION_MAP (self->action_group), "next");
  g_simple_action_set_enabled (G_SIMPLE_ACTION (action), current_slide < n_slides - 1);
}

static void
update_slide_counter_label (SpielPresentationWindow *self)
{
  g_autofree char *text = NULL;
  SpielProject *project;
  GListModel *slides;
  uint32_t current_slide;

  current_slide = spiel_presentation_controller_get_current_slide (self->controller);
  project = spiel_presentation_controller_get_project (self->controller);
  slides = spiel_project_get_slides (project);

  text = g_strdup_printf ("%u / %u", current_slide + 1, g_list_model_get_n_items (slides));

  gtk_label_set_text (self->slide_counter_label, text);
}

static void
go_to_slide (SpielPresentationWindow *self,
             uint32_t                 position)
{
  g_autoptr (SpielSlide) slide = NULL;
  SpielProject *project;
  GListModel *slides;

  project = spiel_presentation_controller_get_project (self->controller);
  slides = spiel_project_get_slides (project);
  slide = g_list_model_get_item (slides, position);

  spiel_slide_rendition_set_slide (self->rendition, slide);

  update_slide_counter_label (self);
  update_actions (self);
}

static inline void
hide_controls (SpielPresentationWindow *self)
{
  g_clear_handle_id (&self->hide_controls_timeout_id, g_source_remove);

  gtk_revealer_set_reveal_child (self->bottom_controls_revealer, FALSE);
  gtk_revealer_set_reveal_child (self->top_controls_revealer, FALSE);
}

static inline void
show_controls (SpielPresentationWindow *self)
{
  g_clear_handle_id (&self->hide_controls_timeout_id, g_source_remove);

  gtk_revealer_set_reveal_child (self->bottom_controls_revealer, TRUE);
  gtk_revealer_set_reveal_child (self->top_controls_revealer, TRUE);

  self->hide_controls_timeout_id = g_timeout_add_seconds (3, hide_controls_after_timeout_cb, self);
}


/*
 * Callbacks
 */

static gboolean
hide_controls_after_timeout_cb (gpointer data)
{
  SpielPresentationWindow *self = (SpielPresentationWindow *) data;

  g_assert (SPIEL_IS_PRESENTATION_WINDOW (self));

  if (self->hovering_controls)
    return G_SOURCE_CONTINUE;

  self->hide_controls_timeout_id = 0;
  hide_controls (self);

  return G_SOURCE_REMOVE;
}

static void
on_motion_controller_leave_cb (GtkEventControllerMotion *motion_controller,
                               SpielPresentationWindow  *self)
{
  self->hovering_controls = FALSE;

  hide_controls (self);
}

static void
on_motion_controller_motion_cb (GtkEventControllerMotion *motion_controller,
                                double                    x,
                                double                    y,
                                SpielPresentationWindow  *self)
{
  GtkWidget *hover;

  if (!gtk_drag_check_threshold (GTK_WIDGET (self),
                                 self->cursor_position.x,
                                 self->cursor_position.y,
                                 x, y))
    {
      return;
    }

  graphene_point_init (&self->cursor_position, x, y);

  hover = gtk_widget_pick (GTK_WIDGET (self), x, y, GTK_PICK_DEFAULT);

  if (hover)
    {
      GtkWidget *controls[] = {
        GTK_WIDGET (self->bottom_controls_revealer),
        GTK_WIDGET (self->top_controls_revealer),
      };

      self->hovering_controls = FALSE;

      for (size_t i = 0; i < G_N_ELEMENTS (controls); i++)
        {
          self->hovering_controls |= hover == controls[i] ||
                                     gtk_widget_is_ancestor (hover, controls[i]);

          if (self->hovering_controls)
            break;
        }
    }

  show_controls (self);
}



/*
 * Actions
 */

static void
on_next_action_activated_cb (GSimpleAction *action,
                             GVariant      *parameter,
                             gpointer       user_data)
{
  SpielPresentationWindow *self = user_data;
  uint32_t current_slide;

  g_assert (SPIEL_IS_PRESENTATION_WINDOW (self));

  current_slide = spiel_presentation_controller_get_current_slide (self->controller);

  spiel_presentation_controller_set_current_slide (self->controller, current_slide + 1);
}

static void
on_presentation_controller_current_slide_changed_cb (SpielPresentationController *controller,
                                                     GParamSpec                  *pspec,
                                                     SpielPresentationWindow     *self)
{
  g_assert (SPIEL_IS_PRESENTATION_WINDOW (self));

  go_to_slide (self, spiel_presentation_controller_get_current_slide (self->controller));
}

static void
on_previous_action_activated_cb (GSimpleAction *action,
                                 GVariant      *parameter,
                                 gpointer       user_data)
{
  SpielPresentationWindow *self = user_data;
  uint32_t current_slide;

  g_assert (SPIEL_IS_PRESENTATION_WINDOW (self));

  current_slide = spiel_presentation_controller_get_current_slide (self->controller);
  g_assert (current_slide > 0);

  spiel_presentation_controller_set_current_slide (self->controller, current_slide - 1);
}

static void
on_toggle_fullscreen_action_activated_cb (GSimpleAction *action,
                                          GVariant      *parameter,
                                          gpointer       user_data)
{
  SpielPresentationWindow *self = user_data;

  g_assert (SPIEL_IS_PRESENTATION_WINDOW (self));

  if (gtk_window_is_fullscreen (GTK_WINDOW (self)))
    gtk_window_unfullscreen (GTK_WINDOW (self));
  else
    gtk_window_fullscreen (GTK_WINDOW (self));
}


/*
 * GObject overrides
 */

static void
spiel_presentation_window_constructed (GObject *object)
{
  SpielPresentationWindow *self = (SpielPresentationWindow *)object;

  G_OBJECT_CLASS (spiel_presentation_window_parent_class)->constructed (object);

  g_assert (self->controller != NULL);

  g_signal_connect_object (self->controller,
                           "notify::current-slide",
                           G_CALLBACK (on_presentation_controller_current_slide_changed_cb),
                           self,
                           0);

  go_to_slide (self, spiel_presentation_controller_get_current_slide (self->controller));
}

static void
spiel_presentation_window_dispose (GObject *object)
{
  SpielPresentationWindow *self = (SpielPresentationWindow *)object;

  gtk_widget_dispose_template (GTK_WIDGET (self), SPIEL_TYPE_PRESENTATION_WINDOW);

  g_clear_handle_id (&self->hide_controls_timeout_id, g_source_remove);
  g_clear_object (&self->controller);

  G_OBJECT_CLASS (spiel_presentation_window_parent_class)->dispose (object);
}

static void
spiel_presentation_window_get_property (GObject    *object,
                                        guint       prop_id,
                                        GValue     *value,
                                        GParamSpec *pspec)
{
  SpielPresentationWindow *self = SPIEL_PRESENTATION_WINDOW (object);

  switch (prop_id)
    {
    case PROP_PRESENTATION_CONTROLLER:
      g_value_set_object (value, self->controller);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
spiel_presentation_window_set_property (GObject      *object,
                                        guint         prop_id,
                                        const GValue *value,
                                        GParamSpec   *pspec)
{
  SpielPresentationWindow *self = SPIEL_PRESENTATION_WINDOW (object);

  switch (prop_id)
    {
    case PROP_PRESENTATION_CONTROLLER:
      g_assert (self->controller == NULL);
      self->controller = g_value_dup_object (value);
      g_assert (self->controller != NULL);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
spiel_presentation_window_class_init (SpielPresentationWindowClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  g_type_ensure (SPIEL_TYPE_SLIDE_RENDITION);

  object_class->constructed = spiel_presentation_window_constructed;
  object_class->dispose = spiel_presentation_window_dispose;
  object_class->get_property = spiel_presentation_window_get_property;
  object_class->set_property = spiel_presentation_window_set_property;

  properties[PROP_PRESENTATION_CONTROLLER] =
    g_param_spec_object ("presentation-controller", NULL, NULL,
                         SPIEL_TYPE_PRESENTATION_CONTROLLER,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);

  gtk_widget_class_set_template_from_resource (widget_class, "/com/feaneron/Spiel/spiel-presentation-window.ui");

  gtk_widget_class_bind_template_child (widget_class, SpielPresentationWindow, bottom_controls_revealer);
  gtk_widget_class_bind_template_child (widget_class, SpielPresentationWindow, rendition);
  gtk_widget_class_bind_template_child (widget_class, SpielPresentationWindow, slide_counter_label);
  gtk_widget_class_bind_template_child (widget_class, SpielPresentationWindow, top_controls_revealer);

  gtk_widget_class_bind_template_callback (widget_class, on_motion_controller_leave_cb);
  gtk_widget_class_bind_template_callback (widget_class, on_motion_controller_motion_cb);
}

static void
spiel_presentation_window_init (SpielPresentationWindow *self)
{
  const GActionEntry actions[] = {
    { "next", on_next_action_activated_cb },
    { "previous", on_previous_action_activated_cb },
    { "toggle-fullscreen", on_toggle_fullscreen_action_activated_cb },
  };

  self->action_group = g_simple_action_group_new ();
  gtk_widget_insert_action_group (GTK_WIDGET (self), "presentation", G_ACTION_GROUP (self->action_group));
  g_action_map_add_action_entries (G_ACTION_MAP (self->action_group), actions, G_N_ELEMENTS (actions), self);

  gtk_widget_init_template (GTK_WIDGET (self));
}

SpielPresentationWindow *
spiel_presentation_window_new (SpielPresentationController *controller)
{
  g_return_val_if_fail (SPIEL_IS_PRESENTATION_CONTROLLER (controller), NULL);

  return g_object_new (SPIEL_TYPE_PRESENTATION_WINDOW,
                       "presentation-controller", controller,
                       NULL);
}
