/*
 * spiel-presentation-controller.c
 *
 * Copyright 2024 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "spiel-presentation-controller.h"

#include "spiel-project.h"

struct _SpielPresentationController
{
  GObject parent_instance;

  SpielProject *project;
  uint32_t current_slide;
};

G_DEFINE_FINAL_TYPE (SpielPresentationController, spiel_presentation_controller, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_CURRENT_SLIDE,
  PROP_PROJECT,
  N_PROPS,
};

static GParamSpec *properties [N_PROPS];

/*
 * GObject overrides
 */

static void
spiel_presentation_controller_finalize (GObject *object)
{
  SpielPresentationController *self = (SpielPresentationController *)object;

  g_clear_object (&self->project);

  G_OBJECT_CLASS (spiel_presentation_controller_parent_class)->finalize (object);
}

static void
spiel_presentation_controller_get_property (GObject    *object,
                                            guint       prop_id,
                                            GValue     *value,
                                            GParamSpec *pspec)
{
  SpielPresentationController *self = SPIEL_PRESENTATION_CONTROLLER (object);

  switch (prop_id)
    {
    case PROP_CURRENT_SLIDE:
      g_value_set_uint (value, self->current_slide);
      break;

    case PROP_PROJECT:
      g_value_set_object (value, self->project);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
spiel_presentation_controller_set_property (GObject      *object,
                                            guint         prop_id,
                                            const GValue *value,
                                            GParamSpec   *pspec)
{
  SpielPresentationController *self = SPIEL_PRESENTATION_CONTROLLER (object);

  switch (prop_id)
    {
    case PROP_CURRENT_SLIDE:
      spiel_presentation_controller_set_current_slide (self, g_value_get_uint (value));
      break;

    case PROP_PROJECT:
      g_assert (self->project == NULL);
      self->project = g_value_dup_object (value);
      g_assert (self->project != NULL);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
spiel_presentation_controller_class_init (SpielPresentationControllerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = spiel_presentation_controller_finalize;
  object_class->get_property = spiel_presentation_controller_get_property;
  object_class->set_property = spiel_presentation_controller_set_property;

  properties[PROP_CURRENT_SLIDE] = g_param_spec_uint ("current-slide", NULL, NULL,
                                                      0, G_MAXUINT, 0,
                                                      G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  properties[PROP_PROJECT] = g_param_spec_object ("project", NULL, NULL,
                                                  SPIEL_TYPE_PROJECT,
                                                  G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
spiel_presentation_controller_init (SpielPresentationController *self)
{

}

SpielPresentationController *
spiel_presentation_controller_new (SpielProject *project,
                                   uint32_t      current_slide)
{
  return g_object_new (SPIEL_TYPE_PRESENTATION_CONTROLLER,
                       "project", project,
                       "current-slide", current_slide,
                       NULL);
}

SpielProject *
spiel_presentation_controller_get_project (SpielPresentationController *self)
{
  g_return_val_if_fail (SPIEL_IS_PRESENTATION_CONTROLLER (self), NULL);

  return self->project;
}

uint32_t
spiel_presentation_controller_get_current_slide (SpielPresentationController *self)
{
  g_return_val_if_fail (SPIEL_IS_PRESENTATION_CONTROLLER (self), 0);

  return self->current_slide;
}

void
spiel_presentation_controller_set_current_slide (SpielPresentationController *self,
                                                 uint32_t                     current_slide)
{
  g_return_if_fail (SPIEL_IS_PRESENTATION_CONTROLLER (self));

  if (self->current_slide == current_slide)
    return;

  // TODO: check bounds
  self->current_slide = current_slide;
  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_CURRENT_SLIDE]);
}

