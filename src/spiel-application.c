/* spiel-application.c
 *
 * Copyright 2023 Georges Stavracas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"

#include "spiel-application.h"
#include "spiel-greeter.h"
#include "spiel-project.h"
#include "spiel-project-list.h"
#include "spiel-recoloring.h"
#include "spiel-slide-layout-registry-private.h"
#include "spiel-types-private.h"
#include "spiel-window.h"

#include <gtksourceview/gtksource.h>
#include <libpeas.h>

struct _SpielApplication
{
	AdwApplication parent_instance;

  GtkCssProvider *recoloring_css;

  GSettings *settings;
  SpielProjectList *project_list;
  SpielSlideLayoutRegistry *layout_registry;
};

G_DEFINE_TYPE (SpielApplication, spiel_application, ADW_TYPE_APPLICATION)


/*
 * Auxiliary functions
 */

static void
load_plugin (PeasEngine     *engine,
             PeasPluginInfo *plugin_info)
{
  g_autoptr (GtkCssProvider) css_provider = NULL;
  g_autofree char *css_style_path = NULL;
  g_autofree char *icons_dir = NULL;
  GtkIconTheme *icon_theme;
  const char *plugin_datadir;

  g_message ("Loading plugin: %s", peas_plugin_info_get_name (plugin_info));

  peas_engine_load_plugin (engine, plugin_info);

  /* Add icons */
  plugin_datadir = peas_plugin_info_get_data_dir (plugin_info);

  if (g_str_has_prefix (plugin_datadir, "resource://"))
    plugin_datadir += strlen ("resource://");
  icons_dir = g_strdup_printf ("%s/icons", plugin_datadir);

  icon_theme = gtk_icon_theme_get_for_display (gdk_display_get_default ());
  gtk_icon_theme_add_resource_path (icon_theme, icons_dir);

  css_style_path = g_strdup_printf ("%s/style.css", plugin_datadir);

  if (g_resources_get_info (css_style_path, 0, NULL, NULL, NULL))
    {
      css_provider = gtk_css_provider_new ();
      gtk_css_provider_load_from_resource (css_provider, css_style_path);

      gtk_style_context_add_provider_for_display (gdk_display_get_default (),
                                                  GTK_STYLE_PROVIDER (css_provider),
                                                  GTK_STYLE_PROVIDER_PRIORITY_USER);
    }
}

static void
regenerate_css (SpielApplication *self)
{
  GtkSourceStyleSchemeManager *scheme_manager;
  GtkSourceStyleScheme *scheme;
  AdwStyleManager *style_manager;
  g_autofree char *css = NULL;
  const char *current_scheme;

  current_scheme = g_settings_get_string (self->settings, "style-scheme");
  style_manager = adw_style_manager_get_default ();
  scheme_manager = gtk_source_style_scheme_manager_get_default ();

  if (adw_style_manager_get_dark (style_manager))
    {
      GtkSourceStyleScheme *light_scheme;
      const char *dark_variant;

      light_scheme = gtk_source_style_scheme_manager_get_scheme (scheme_manager, current_scheme);
      dark_variant = gtk_source_style_scheme_get_metadata (light_scheme, "dark-variant");
      scheme = gtk_source_style_scheme_manager_get_scheme (scheme_manager, dark_variant);
    }
  else
    {
      scheme = gtk_source_style_scheme_manager_get_scheme (scheme_manager, current_scheme);
    }

  css = spiel_recoloring_generate_css (scheme);
  gtk_css_provider_load_from_string (self->recoloring_css, css ?: "");
}


/*
 * Callbacks
 */

static void
on_open_greeter_action_activated_cb (GSimpleAction *action,
                                     GVariant      *parameter,
                                     gpointer       user_data)
{
	SpielApplication *self = user_data;
  GtkWindow *window = NULL;
  GList *windows;

  windows = gtk_application_get_windows (GTK_APPLICATION (self));
  for (GList *l = windows; l != NULL; l = l->next)
    {
      if (SPIEL_IS_GREETER (l->data))
        {
          window = l->data;
          break;
        }
    }

  if (!window)
    window = spiel_greeter_new (self);

  gtk_window_present (window);
}

static void
on_quit_action_activated_cb (GSimpleAction *action,
                             GVariant      *parameter,
                             gpointer       user_data)
{
	SpielApplication *self = user_data;

	g_assert (SPIEL_IS_APPLICATION (self));

	g_application_quit (G_APPLICATION (self));
}

static void
on_settings_style_scheme_changed_cb (GSettings        *settings,
                                     const char       *key,
                                     SpielApplication *self)
{
  regenerate_css (self);
}

static void
on_style_manager_dark_changed_cb (AdwStyleManager  *style_manager,
                                  GParamSpec       *pspec,
                                  SpielApplication *self)
{
  regenerate_css (self);
}

static gboolean
style_variant_to_color_scheme (GValue   *value,
                               GVariant *variant,
                               gpointer  user_data)
{
  const char *style_variant = g_variant_get_string (variant, NULL);

  if (g_str_equal (style_variant, "dark"))
    g_value_set_enum (value, ADW_COLOR_SCHEME_FORCE_DARK);
  else if (g_str_equal (style_variant, "light"))
    g_value_set_enum (value, ADW_COLOR_SCHEME_FORCE_LIGHT);
  else if (g_str_equal (style_variant, "default"))
    g_value_set_enum (value, ADW_COLOR_SCHEME_DEFAULT);
  else
    return FALSE;

  return TRUE;
}


/*
 * GApplication overrides
 */

static void
spiel_application_startup (GApplication *application)
{
  GtkSourceStyleSchemeManager *scheme_manager;
	SpielApplication *self;
  AdwStyleManager *style_manager;
  PeasEngine *engine;

  self = (SpielApplication *)application;

  G_APPLICATION_CLASS (spiel_application_parent_class)->startup (application);

  gtk_source_init ();
  spiel_init_public_types ();

  /* All layouts must be loaded before loading projects */
  engine = peas_engine_get_default ();
  peas_engine_enable_loader (engine, "gjs");
  peas_engine_add_search_path (engine,
                               "resource:///com/feaneron/Spiel/layouts",
                               "resource:///com/feaneron/Spiel/layouts");

  for (guint i = 0; i < g_list_model_get_n_items (G_LIST_MODEL (engine)); i++)
    {
      g_autoptr(PeasPluginInfo) info = g_list_model_get_item (G_LIST_MODEL (engine), i);
      load_plugin (engine, info);
    }

  spiel_slide_layout_registry_load (self->layout_registry);

  scheme_manager = gtk_source_style_scheme_manager_get_default ();
  gtk_source_style_scheme_manager_append_search_path (scheme_manager,
                                                      PKGDATADIR "/styles");

  style_manager = adw_style_manager_get_default ();
  g_settings_bind_with_mapping (self->settings,
                                "style-variant",
                                style_manager,
                                "color-scheme",
                                G_SETTINGS_BIND_GET,
                                style_variant_to_color_scheme,
                                NULL,
                                self,
                                NULL);

  self->recoloring_css = gtk_css_provider_new ();
  gtk_style_context_add_provider_for_display (gdk_display_get_default (),
                                              GTK_STYLE_PROVIDER (self->recoloring_css),
                                              GTK_STYLE_PROVIDER_PRIORITY_USER);

  g_signal_connect (style_manager,
                    "notify::dark",
                    G_CALLBACK (on_style_manager_dark_changed_cb),
                    self);

  g_signal_connect (self->settings,
                    "changed::style-scheme",
                    G_CALLBACK (on_settings_style_scheme_changed_cb),
                    self);

  regenerate_css (self);
}

static void
spiel_application_activate (GApplication *app)
{
	GtkWindow *window;

	g_assert (SPIEL_IS_APPLICATION (app));

	window = gtk_application_get_active_window (GTK_APPLICATION (app));

	if (window == NULL)
		window = spiel_greeter_new (SPIEL_APPLICATION (app));

	gtk_window_present (window);
}

static void
spiel_application_shutdown (GApplication *application)
{
	SpielApplication *self = (SpielApplication *)application;

  G_APPLICATION_CLASS (spiel_application_parent_class)->shutdown (application);

  g_clear_object (&self->project_list);
}

static void
spiel_application_finalize (GObject *object)
{
  SpielApplication *self = (SpielApplication *)object;

  g_assert (self->project_list == NULL);

  g_clear_object (&self->settings);

  G_OBJECT_CLASS (spiel_application_parent_class)->finalize (object);
}

static void
spiel_application_class_init (SpielApplicationClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
	GApplicationClass *app_class = G_APPLICATION_CLASS (klass);

  object_class->finalize = spiel_application_finalize;

  app_class->startup = spiel_application_startup;
	app_class->activate = spiel_application_activate;
	app_class->shutdown = spiel_application_shutdown;
}

static void
spiel_application_init (SpielApplication *self)
{
  const GActionEntry app_actions[] = {
    { "open-greeter", on_open_greeter_action_activated_cb, },
    { "quit", on_quit_action_activated_cb },
  };

  self->settings = g_settings_new ("com.feaneron.Spiel");
  self->layout_registry = spiel_slide_layout_registry_new ();
  self->project_list = spiel_project_list_new ();

	g_action_map_add_action_entries (G_ACTION_MAP (self),
	                                 app_actions,
	                                 G_N_ELEMENTS (app_actions),
	                                 self);

	gtk_application_set_accels_for_action (GTK_APPLICATION (self),
	                                       "app.quit",
	                                       (const char *[]) { "<primary>q", NULL });
	gtk_application_set_accels_for_action (GTK_APPLICATION (self),
	                                       "app.open-greeter",
	                                       (const char *[]) { "<primary><alt>o", NULL });
}

SpielApplication *
spiel_application_new (const char        *application_id,
                       GApplicationFlags  flags)
{
	g_return_val_if_fail (application_id != NULL, NULL);

	return g_object_new (SPIEL_TYPE_APPLICATION,
	                     "application-id", application_id,
	                     "flags", flags,
	                     NULL);
}

GSettings *
spiel_application_get_settings (SpielApplication *self)
{
  g_assert (SPIEL_IS_APPLICATION (self));

  return g_object_ref (self->settings);
}

SpielProjectList *
spiel_application_get_project_list (SpielApplication *self)
{
  g_assert (SPIEL_IS_APPLICATION (self));

  return self->project_list;
}

GtkWindow *
spiel_application_find_project_window (SpielApplication *self,
                                       SpielProject     *project)
{
  GList *windows;

	g_return_val_if_fail (SPIEL_IS_APPLICATION (self), NULL);
  g_return_val_if_fail (SPIEL_IS_PROJECT (project), NULL);

  windows = gtk_application_get_windows (GTK_APPLICATION (self));
  for (GList *l = windows; l != NULL; l = l->next)
    {
      SpielProject *window_project;

      if (!SPIEL_IS_WINDOW (l->data))
        continue;

      window_project = spiel_window_get_project (l->data);
      if (window_project == project)
        return GTK_WINDOW (l->data);
    }

  return NULL;
}

SpielSlideLayoutRegistry *
spiel_application_get_layout_registry (SpielApplication *self)
{
  g_assert (SPIEL_IS_APPLICATION (self));

  return self->layout_registry;
}
