/*
 * spiel-slide.c
 *
 * Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "spiel-slide.h"
#include "spiel-slide-feature-private.h"

#include <md4c.h>

typedef enum
{
  NODE_TYPE_BLOCK,
  NODE_TYPE_SPAN,
  NODE_TYPE_TEXT,
} NodeType;

typedef struct
{
  NodeType node_type;
  GPtrArray *children; /* owned */
} Node;

typedef struct
{
  Node node;
  MD_BLOCKTYPE block_type;

  union {

    struct {
      uint32_t level;
    } h;

  } data;
} BlockNode;

typedef struct
{
  Node node;
  MD_SPANTYPE type;

  union {

    struct {
      char *src;
    } img;

  } data;
} SpanNode;

typedef struct
{
  Node node;
  char *text;
} TextNode;

typedef struct
{
  GPtrArray *stack; /* transfer none */
  Node *root; /* transfer full */
  GPtrArray *features;
  int level;
} MarkdownParse;

struct _SpielSlide
{
  GObject parent_instance;

  char *text;
  GtkTextMark *start;
  GtkTextMark *end;

  GPtrArray *features;
};

G_DEFINE_FINAL_TYPE (SpielSlide, spiel_slide, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_TEXT,
  N_PROPS
};

static GParamSpec *properties[N_PROPS] = { NULL, };


/*
 * Nodes
 */

static void
node_free (gpointer data)
{
  Node *node = (Node *) data;

  switch (node->node_type)
    {
    case NODE_TYPE_BLOCK:
      {
        BlockNode *block = (BlockNode *)node;
        g_clear_pointer (&node->children, g_ptr_array_unref);
        g_slice_free (BlockNode, block);
      }
      break;

    case NODE_TYPE_SPAN:
      {
        SpanNode *span = (SpanNode *)node;

        switch (span->type)
          {
          case MD_SPAN_IMG:
            g_clear_pointer (&span->data.img.src, g_free);
            break;

          case MD_SPAN_STRONG:
          case MD_SPAN_EM:
          case MD_SPAN_A:
          case MD_SPAN_CODE:
          case MD_SPAN_DEL:
          case MD_SPAN_LATEXMATH:
          case MD_SPAN_LATEXMATH_DISPLAY:
          case MD_SPAN_WIKILINK:
          case MD_SPAN_U:
            break;

          default:
            g_assert_not_reached ();
          }

        g_clear_pointer (&node->children, g_ptr_array_unref);
        g_slice_free (SpanNode, span);
      }
      break;

    case NODE_TYPE_TEXT:
      {
        TextNode *text = (TextNode *)node;

        g_assert (&node->children == NULL);
        g_clear_pointer (&text->text, g_free);
        g_slice_free (TextNode, text);
      }
      break;

    default:
      g_assert_not_reached ();
    }
}

G_DEFINE_AUTOPTR_CLEANUP_FUNC (Node, node_free)
G_DEFINE_AUTOPTR_CLEANUP_FUNC (BlockNode, node_free)
G_DEFINE_AUTOPTR_CLEANUP_FUNC (SpanNode, node_free)
G_DEFINE_AUTOPTR_CLEANUP_FUNC (TextNode, node_free)

static void
node_init (Node     *node,
           NodeType  type)
{
  node->node_type = type;

  switch (type)
    {
    case NODE_TYPE_BLOCK:
    case NODE_TYPE_SPAN:
      node->children = g_ptr_array_new_with_free_func (node_free);
      break;

    case NODE_TYPE_TEXT:
      break;

    default:
      g_assert_not_reached ();
    }
}

static BlockNode *
block_node_new (MD_BLOCKTYPE block_type)
{
  g_autoptr (BlockNode) block = NULL;

  block = g_slice_new0 (BlockNode);
  node_init ((Node *) block, NODE_TYPE_BLOCK);
  block->block_type = block_type;

  return g_steal_pointer (&block);
}

static SpanNode *
span_node_new (MD_SPANTYPE span_type)
{
  g_autoptr (SpanNode) span = NULL;

  span = g_slice_new0 (SpanNode);
  node_init ((Node *) span, NODE_TYPE_SPAN);
  span->type = span_type;

  return g_steal_pointer (&span);
}

static TextNode *
text_new (char *text)
{
  g_autoptr (TextNode) text_node = NULL;

  text_node = g_slice_new0 (TextNode);
  node_init ((Node *) text_node, NODE_TYPE_TEXT);
  text_node->text = g_steal_pointer (&text);

  return g_steal_pointer (&text_node);
}


/*
 * MD_PARSER implementation
 */

static void
add_and_push_node (MarkdownParse *parse,
                   Node          *node)
{
  g_assert (parse->stack != NULL);

  if (parse->stack->len > 0)
    {
      Node *topmost = g_ptr_array_index (parse->stack, parse->stack->len - 1);
      g_ptr_array_add (topmost->children, node);
    }

  g_ptr_array_add (parse->stack, node);
}

static void
pop_node (MarkdownParse *parse)
{
  g_assert (parse->stack != NULL);
  g_ptr_array_remove_index (parse->stack, parse->stack->len - 1);
}

static void apply_node_to_string (GString    *string,
                                  const Node *node);

static inline void
apply_span_to_string (GString        *string,
                         const SpanNode *span)
{
  const Node *node = (const Node *) span;

  g_assert (node->node_type == NODE_TYPE_SPAN);

  switch (span->type)
    {
    case MD_SPAN_STRONG:
      g_string_append_len (string, "<b>", strlen ("<b>"));
      break;

    case MD_SPAN_EM:
      g_string_append_len (string, "<i>", strlen ("<i>"));
      break;

    case MD_SPAN_IMG:
    case MD_SPAN_A:
    case MD_SPAN_CODE:
    case MD_SPAN_DEL:
    case MD_SPAN_LATEXMATH:
    case MD_SPAN_LATEXMATH_DISPLAY:
    case MD_SPAN_WIKILINK:
    case MD_SPAN_U:
      break;

    default:
      g_assert_not_reached ();
    }

  for (size_t i = 0; i < node->children->len; i++)
    {
      const Node *child = g_ptr_array_index (node->children, i);
      apply_node_to_string (string, child);
    }

  switch (span->type)
    {
    case MD_SPAN_STRONG:
      g_string_append_len (string, "</b>", strlen ("</b>"));
      break;

    case MD_SPAN_EM:
      g_string_append_len (string, "</i>", strlen ("</i>"));
      break;

    case MD_SPAN_IMG:
    case MD_SPAN_A:
    case MD_SPAN_CODE:
    case MD_SPAN_DEL:
    case MD_SPAN_LATEXMATH:
    case MD_SPAN_LATEXMATH_DISPLAY:
    case MD_SPAN_WIKILINK:
    case MD_SPAN_U:
      break;

    default:
      g_assert_not_reached ();
    }
}

static inline void
apply_block_to_string (GString         *string,
                       const BlockNode *block)
{
  const Node *node = (const Node *) block;

  g_assert (node->node_type == NODE_TYPE_BLOCK);

  for (size_t i = 0; i < node->children->len; i++)
    {
      const Node *child = g_ptr_array_index (node->children, i);
      apply_node_to_string (string, child);
    }
}

static void
apply_node_to_string (GString    *string,
                      const Node *node)
{
  switch (node->node_type)
    {
    case NODE_TYPE_BLOCK:
      apply_block_to_string (string, (const BlockNode *) node);
      break;

    case NODE_TYPE_SPAN:
      apply_span_to_string (string, (const SpanNode *) node);
      break;

    case NODE_TYPE_TEXT:
      g_string_append (string, ((const TextNode *) node)->text);
      break;

    default:
      g_assert_not_reached ();
    }
}

static inline char *
stringify_node (const Node *node)
{
  GString *string = g_string_new (NULL);

  apply_node_to_string (string, node);

  return g_string_free_and_steal (string);
}

static inline gboolean
is_p_block_media (const BlockNode *block)
{
  const Node *child;

  g_assert (block->node.node_type == NODE_TYPE_BLOCK);
  g_assert (block->block_type == MD_BLOCK_P);

  if (block->node.children->len != 1)
    return FALSE;

  child = g_ptr_array_index (block->node.children, 0);

  return child->node_type == NODE_TYPE_SPAN &&
         ((SpanNode *) child)->type == MD_SPAN_IMG;
}

static inline void
consume_p_block (MarkdownParse   *parse,
                 const BlockNode *block)
{
  g_autoptr (SpielSlideFeature) feature = NULL;

  g_assert (block->node.node_type == NODE_TYPE_BLOCK);
  g_assert (block->block_type == MD_BLOCK_P);

  if (is_p_block_media (block))
    {
      g_debug ("%*sMEDIA", parse->level * 2, "");
      feature = spiel_slide_feature_new_media ();
    }
  else
    {
      g_autofree char *text = stringify_node ((const Node *) block);

      g_debug ("%*sNOTE: '%s'", parse->level * 2, "", text);

      feature = spiel_slide_feature_new_note (g_steal_pointer (&text));
    }

  g_assert (feature != NULL);
  g_ptr_array_add (parse->features, g_steal_pointer (&feature));
}

static inline void
consume_h_block (MarkdownParse   *parse,
                 const BlockNode *block)
{
  g_autoptr (SpielSlideFeature) feature = NULL;
  g_autofree char *text = NULL;

  g_assert (block->block_type == MD_BLOCK_H);

  text = stringify_node ((const Node *) block);

  g_debug ("%*sTITLE (%u): '%s'", parse->level * 2, "", block->data.h.level, text);

  feature = spiel_slide_feature_new_title (g_steal_pointer (&text),
                                           block->data.h.level);
  g_ptr_array_add (parse->features, g_steal_pointer (&feature));
}

static int
markdown_parser_enter_block (MD_BLOCKTYPE block_type,
                             gpointer     detail,
                             gpointer     user_data)
{
  MarkdownParse *parse = (MarkdownParse *) user_data;
  g_autoptr (BlockNode) block = NULL;

  g_debug ("%*sEntering block %d", parse->level * 2, "", block_type);

  block = block_node_new (block_type);

  switch (block_type)
    {
    case MD_BLOCK_DOC:
      g_assert (parse->stack == NULL);
      g_assert (parse->root == NULL);

      parse->stack = g_ptr_array_new ();
      parse->root = (Node *) block;
      break;

    case MD_BLOCK_H:
      block->data.h.level = ((MD_BLOCK_H_DETAIL*) detail)->level;
      break;

    case MD_BLOCK_P:
    case MD_BLOCK_QUOTE:
    case MD_BLOCK_UL:
    case MD_BLOCK_OL:
    case MD_BLOCK_LI:
    case MD_BLOCK_HR:
    case MD_BLOCK_CODE:
    case MD_BLOCK_HTML:
    case MD_BLOCK_TABLE:
    case MD_BLOCK_THEAD:
    case MD_BLOCK_TBODY:
    case MD_BLOCK_TR:
    case MD_BLOCK_TH:
    case MD_BLOCK_TD:
      break;

    default:
      g_assert_not_reached ();
    }

  add_and_push_node (parse, (Node *) g_steal_pointer (&block));

  parse->level++;

  return 0;
}

static int
markdown_parser_leave_block (MD_BLOCKTYPE block_type,
                             gpointer     detail,
                             gpointer     user_data)
{
  MarkdownParse *parse = (MarkdownParse *) user_data;
  const Node *node;

  parse->level--;

  g_debug ("%*sLeaving block %d", parse->level * 2, "", block_type);

  node = g_ptr_array_index (parse->stack, parse->stack->len - 1);
  g_assert (node->node_type == NODE_TYPE_BLOCK);

  switch (block_type)
    {
    case MD_BLOCK_DOC:
      g_assert (parse->stack != NULL);
      g_assert (parse->stack->len == 1);
      g_assert (g_ptr_array_index (parse->stack, 0) == parse->root);
      break;

    case MD_BLOCK_H:
      consume_h_block (parse, (const BlockNode *) node);
      break;

    case MD_BLOCK_P:
      consume_p_block (parse, (const BlockNode *) node);
      break;

    case MD_BLOCK_QUOTE:
    case MD_BLOCK_UL:
    case MD_BLOCK_OL:
    case MD_BLOCK_LI:
    case MD_BLOCK_HR:
    case MD_BLOCK_CODE:
    case MD_BLOCK_HTML:
    case MD_BLOCK_TABLE:
    case MD_BLOCK_THEAD:
    case MD_BLOCK_TBODY:
    case MD_BLOCK_TR:
    case MD_BLOCK_TH:
    case MD_BLOCK_TD:
      break;

    default:
      g_assert_not_reached ();
    }

  pop_node (parse);

  return 0;
}

static int
markdown_parser_enter_span (MD_SPANTYPE span_type,
                            gpointer    detail,
                            gpointer    user_data)
{
  MarkdownParse *parse = (MarkdownParse *) user_data;
  g_autoptr (SpanNode) span = NULL;

  g_debug ("%*sEntering span %d", parse->level * 2, "", span_type);

  span = span_node_new (span_type);

  switch (span_type)
    {
    case MD_SPAN_IMG:
      {
        const MD_SPAN_IMG_DETAIL *img_detail = (MD_SPAN_IMG_DETAIL *) detail;
        GString *string;

        string = g_string_new_len (img_detail->src.text, img_detail->src.size);

        span->data.img.src = g_string_free_and_steal (string);
      }
      break;

    case MD_SPAN_STRONG:
    case MD_SPAN_EM:
    case MD_SPAN_A:
    case MD_SPAN_CODE:
    case MD_SPAN_DEL:
    case MD_SPAN_LATEXMATH:
    case MD_SPAN_LATEXMATH_DISPLAY:
    case MD_SPAN_WIKILINK:
    case MD_SPAN_U:
      break;

    default:
      g_assert_not_reached ();
    }

  add_and_push_node (parse, (Node *) g_steal_pointer (&span));

  parse->level++;

  return 0;
}

static int
markdown_parser_leave_span (MD_SPANTYPE span_type,
                            gpointer    detail,
                            gpointer    user_data)
{
  MarkdownParse *parse = (MarkdownParse *) user_data;
  const Node *node;

  parse->level--;

  g_debug ("%*sLeaving span %d", parse->level * 2, "", span_type);

  node = g_ptr_array_index (parse->stack, parse->stack->len - 1);
  g_assert (node->node_type == NODE_TYPE_SPAN);

  pop_node (parse);

  return 0;
}

static int
markdown_parser_text (MD_TEXTTYPE    text_type,
                      const MD_CHAR *text,
                      MD_SIZE        size,
                      gpointer       user_data)
{
  MarkdownParse *parse = (MarkdownParse *) user_data;
  g_autoptr (TextNode) text_node = NULL;
  GString *string;

  string = g_string_new_len (text, size);

  g_debug ("%*sText (%d): %s", parse->level * 2, "", text_type, string->str);

  text_node = text_new (g_string_free_and_steal (string));
  add_and_push_node (parse, (Node *) g_steal_pointer (&text_node));
  pop_node (parse);

  return 0;
}

static const MD_PARSER markdown_parser = {
  .flags = MD_FLAG_NOHTML,
  .enter_block = markdown_parser_enter_block,
  .leave_block = markdown_parser_leave_block,
  .enter_span = markdown_parser_enter_span,
  .leave_span = markdown_parser_leave_span,
  .text = markdown_parser_text,
};

static void
parse_markdown (SpielSlide *self)
{
  MarkdownParse parse = {
    .features = g_ptr_array_new_null_terminated (20,
                                                 (GDestroyNotify) spiel_slide_feature_free,
                                                 TRUE),
  };
  unsigned long len;

  len = strlen (self->text);

  g_debug ("Extracting features from text (len=%lu):\n%s\n", len, self->text);

  md_parse (self->text, len, &markdown_parser, &parse);

  /* TODO: might be better to diff features? */
  g_clear_pointer (&self->features, g_ptr_array_unref);
  self->features = g_steal_pointer (&parse.features);

  g_debug ("Feature extraction finished");
}


/*
 * GObject overrides
 */

static void
spiel_slide_finalize (GObject *object)
{
  SpielSlide *self = (SpielSlide *)object;

  g_clear_pointer (&self->features, g_ptr_array_unref);
  g_clear_pointer (&self->text, g_free);

  G_OBJECT_CLASS (spiel_slide_parent_class)->finalize (object);
}

static void
spiel_slide_get_property (GObject    *object,
                          guint       prop_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
  SpielSlide *self = SPIEL_SLIDE (object);

  switch (prop_id)
    {
    case PROP_TEXT:
      g_value_set_string (value, self->text);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
spiel_slide_set_property (GObject      *object,
                          guint         prop_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
  SpielSlide *self = SPIEL_SLIDE (object);

  switch (prop_id)
    {
    case PROP_TEXT:
      spiel_slide_set_text (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
spiel_slide_class_init (SpielSlideClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = spiel_slide_finalize;
  object_class->get_property = spiel_slide_get_property;
  object_class->set_property = spiel_slide_set_property;

  properties[PROP_TEXT] =
    g_param_spec_string ("text", "", "",
                         NULL,
                         G_PARAM_READWRITE |
                         G_PARAM_CONSTRUCT |
                         G_PARAM_STATIC_STRINGS |
                         G_PARAM_EXPLICIT_NOTIFY);

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
spiel_slide_init (SpielSlide *self)
{
  self->features = g_ptr_array_new_null_terminated (20,
                                                    (GDestroyNotify) spiel_slide_feature_free,
                                                    TRUE);
}

SpielSlide *
spiel_slide_new (const char *text)
{
  return g_object_new (SPIEL_TYPE_SLIDE,
                       "text", text,
                       NULL);
}


const char *
spiel_slide_get_text (SpielSlide *self)
{
  g_return_val_if_fail (SPIEL_IS_SLIDE (self), NULL);

  return self->text;
}

void
spiel_slide_set_text (SpielSlide *self,
                      const char *text)
{
  g_return_if_fail (SPIEL_IS_SLIDE (self));
  g_return_if_fail (text != NULL);

  if (!g_set_str (&self->text, text))
    return;

  parse_markdown (self);

  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_TEXT]);
}

const SpielSlideFeature **
spiel_slide_get_features (SpielSlide *self)
{
  g_return_val_if_fail (SPIEL_IS_SLIDE (self), NULL);

  return (const SpielSlideFeature **) self->features->pdata;
}

void
spiel_slide_get_marks (SpielSlide   *self,
                       GtkTextMark **out_start,
                       GtkTextMark **out_end)
{
  g_return_if_fail (SPIEL_IS_SLIDE (self));

  if (out_start)
    *out_start = self->start;

  if (out_end)
    *out_end = self->end;
}

void
spiel_slide_set_marks (SpielSlide  *self,
                       GtkTextMark *start,
                       GtkTextMark *end)
{
  g_return_if_fail (SPIEL_IS_SLIDE (self));
  g_return_if_fail (start != NULL);
  g_return_if_fail (end != NULL);

  g_set_object (&self->start, start);
  g_set_object (&self->end, end);
}
