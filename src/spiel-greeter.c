/*
 * spiel-greeter.c
 *
 * Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "spiel-greeter.h"

#include "spiel-application.h"
#include "spiel-greeter-row.h"
#include "spiel-project.h"
#include "spiel-project-list.h"
#include "spiel-window.h"

struct _SpielGreeter
{
  AdwApplicationWindow parent_instance;

  AdwNavigationView *navigation_view;
  GtkListBox *projects_listbox;

  GCancellable *cancellable;

  SpielProjectList *project_list;
  gboolean selection_mode;
  guint n_selected_projects;
};

G_DEFINE_FINAL_TYPE (SpielGreeter, spiel_greeter, ADW_TYPE_APPLICATION_WINDOW)

enum {
  PROP_0,
  PROP_PROJECT_LIST,
  PROP_SELECTION_MODE,
  N_PROPS,
};

static GParamSpec *properties [N_PROPS];


/*
 * Auxiliary methods
 */

static void
update_delete_projects_action (SpielGreeter *self)
{
  GAction *action;

  action = g_action_map_lookup_action (G_ACTION_MAP (self), "delete-projects");
  g_assert (action != NULL);
  g_assert (G_IS_SIMPLE_ACTION (action));

  g_simple_action_set_enabled (G_SIMPLE_ACTION (action),
                               self->n_selected_projects > 0);
}

static void
unselect_all_rows (SpielGreeter *self)
{
  GtkListBoxRow *row;
  unsigned int i = 0;

  while ((row = gtk_list_box_get_row_at_index (self->projects_listbox, i++)) != NULL)
    {
      g_assert (SPIEL_IS_GREETER_ROW (row));
      spiel_greeter_row_set_selected (SPIEL_GREETER_ROW (row), FALSE);
    }

  self->n_selected_projects = 0;
  update_delete_projects_action (self);
}

static void
update_visible_page (SpielGreeter *self)
{
  const char *visible_page_tag;

  if (spiel_project_list_get_error (self->project_list))
    {
      visible_page_tag = "error";
    }
  else if (g_list_model_get_n_items (G_LIST_MODEL (self->project_list)) == 0)
    {
      if (spiel_project_list_is_loading (self->project_list))
        visible_page_tag = "loading";
      else
        visible_page_tag = "empty";
    }
  else
    {
      visible_page_tag = "list";
    }

  adw_navigation_view_replace_with_tags (self->navigation_view,
                                         (const char *const []) {
                                           visible_page_tag,
                                         }, 1);
}


/*
 * Callbacks
 */

static void
projects_deleted_cb (GObject      *source,
                     GAsyncResult *result,
                     gpointer      user_data)
{
  g_autoptr (GError) error = NULL;
  SpielProjectList *project_list;
  SpielGreeter *self;

  self = (SpielGreeter *) user_data; /* invalid if cancelled */
  project_list = SPIEL_PROJECT_LIST (source);

  if (!spiel_project_list_delete_projects_finish (project_list, result, &error))
    {
      if (!g_error_matches (error, G_IO_ERROR, G_IO_ERROR_CANCELLED))
        {
          g_warning ("Error deleting projects: %s", error->message);
          gtk_widget_set_sensitive (GTK_WIDGET (self), TRUE);
          g_clear_object (&self->cancellable);
        }

      return;
    }

  g_debug ("Finished deleting projects");

  gtk_widget_set_sensitive (GTK_WIDGET (self), TRUE);
}

static void
project_loaded_cb (GObject      *source,
                   GAsyncResult *result,
                   gpointer      user_data)
{
  g_autoptr (GError) error = NULL;
  SpielApplication *application;
  SpielProject *project;
  SpielGreeter *self;
  GtkWindow *window;

  self = (SpielGreeter *) user_data; /* invalid if cancelled */
  project = SPIEL_PROJECT (source);

  if (!spiel_project_load_finish (project, result, &error))
    {
      if (!g_error_matches (error, G_IO_ERROR, G_IO_ERROR_CANCELLED))
        {
          g_warning ("Error loading project: %s", error->message);
          gtk_widget_set_sensitive (GTK_WIDGET (self->navigation_view), TRUE);
          g_clear_object (&self->cancellable);
        }
      return;
    }

  application = (SpielApplication *) gtk_window_get_application (GTK_WINDOW (self));

  g_assert (SPIEL_IS_APPLICATION (application));
  g_assert (spiel_application_find_project_window (application, project) == NULL);

  window = spiel_window_new (application, project);
  gtk_window_present (window);
  gtk_window_destroy (GTK_WINDOW (self));
}

static void
on_project_list_project_created_cb (GObject      *source,
                                    GAsyncResult *result,
                                    gpointer      user_data)
{
  g_autoptr (SpielProject) project = NULL;
  g_autoptr (GError) error = NULL;
  SpielGreeter *self;

  self = SPIEL_GREETER (user_data);
  gtk_widget_set_sensitive (GTK_WIDGET (self), TRUE);

  project = spiel_project_list_create_project_finish (SPIEL_PROJECT_LIST (source),
                                                      result,
                                                      &error);
  if (error)
    {
      g_warning ("Error creating project: %s", error->message);
      return;
    }

  spiel_project_load (project,
                      self->cancellable,
                      project_loaded_cb,
                      self);
}

static void
on_delete_projects_action_activated_cb (GSimpleAction *action,
                                        GVariant      *parameter,
                                        gpointer       user_data)
{
  g_autoptr (GList) selected_projects = NULL;
  SpielGreeter *self = user_data;
  size_t n_projects;

  g_assert (SPIEL_IS_GREETER (self));
  g_assert (self->selection_mode);

  n_projects = g_list_model_get_n_items (G_LIST_MODEL (self->project_list));

  for (size_t i = 0; i < n_projects; i++)
    {
      GtkListBoxRow *row = gtk_list_box_get_row_at_index (self->projects_listbox, i);

      g_assert (SPIEL_IS_GREETER_ROW (row));

      if (spiel_greeter_row_get_selected (SPIEL_GREETER_ROW (row)))
        selected_projects = g_list_prepend (selected_projects, spiel_greeter_row_get_project (SPIEL_GREETER_ROW (row)));
    }

  g_debug ("Deleting %u project(s)", g_list_length (selected_projects));

  self->cancellable = g_cancellable_new ();
  spiel_project_list_delete_projects (self->project_list,
                                      selected_projects,
                                      self->cancellable,
                                      projects_deleted_cb,
                                      self);

  gtk_widget_set_sensitive (GTK_WIDGET (self), FALSE);
}

static void
on_new_project_action_activated_cb (GSimpleAction *action,
                                    GVariant      *parameter,
                                    gpointer       user_data)
{
  SpielGreeter *self = user_data;

  g_assert (SPIEL_IS_GREETER (self));

  gtk_widget_set_sensitive (GTK_WIDGET (self->navigation_view), FALSE);

  self->cancellable = g_cancellable_new ();
  spiel_project_list_create_project (self->project_list,
                                     NULL,
                                     on_project_list_project_created_cb,
                                     self);
}

static void
on_project_list_items_changed_cb (SpielProjectList *project_list,
                                  unsigned int      position,
                                  unsigned int      removed,
                                  unsigned int      added,
                                  SpielGreeter     *self)
{
  update_visible_page (self);
}

static void
on_project_list_changed_cb (SpielProjectList *project_list,
                            GParamSpec       *pspec,
                            SpielGreeter     *self)
{
  update_visible_page (self);
}

static void
on_project_row_activated_cb (SpielGreeterRow *row,
                             SpielGreeter    *self)
{
  if (self->selection_mode)
    {
      gboolean selected = spiel_greeter_row_get_selected (row);
      spiel_greeter_row_set_selected (row, !selected);

      if (spiel_greeter_row_get_selected (row))
        self->n_selected_projects++;
      else
        self->n_selected_projects--;
      update_delete_projects_action (self);
    }
  else
    {
      SpielApplication *application;
      SpielProject *project;
      GtkWindow *window;

      project = spiel_greeter_row_get_project (row);

      g_assert (project != NULL);

      gtk_widget_set_sensitive (GTK_WIDGET (self->navigation_view), FALSE);

      application = (SpielApplication *) gtk_window_get_application (GTK_WINDOW (self));
      g_assert (SPIEL_IS_APPLICATION (application));

      window = spiel_application_find_project_window (application, project);
      if (window)
        {
          gtk_window_present (window);
          gtk_window_destroy (GTK_WINDOW (self));
          return;
        }

      self->cancellable = g_cancellable_new ();
      spiel_project_load (project,
                          self->cancellable,
                          project_loaded_cb,
                          self);
    }
}

static GtkWidget *
create_widget_func (gpointer item,
                    gpointer user_data)
{
  SpielGreeter *self;
  GtkWidget *row;

  self = SPIEL_GREETER (user_data);

  row = spiel_greeter_row_new (item);
  g_signal_connect (row, "activated", G_CALLBACK (on_project_row_activated_cb), self);

  g_object_bind_property (self, "selection-mode",
                          row, "selectable",
                          G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);

  return row;
}


/*
 * GObject overrides
 */

static void
spiel_greeter_dispose (GObject *object)
{
  SpielGreeter *self = (SpielGreeter *)object;

  g_cancellable_cancel (self->cancellable);

  g_clear_object (&self->cancellable);
  g_clear_object (&self->project_list);

  G_OBJECT_CLASS (spiel_greeter_parent_class)->dispose (object);
}

static void
spiel_greeter_get_property (GObject    *object,
                            guint       prop_id,
                            GValue     *value,
                            GParamSpec *pspec)
{
  SpielGreeter *self = SPIEL_GREETER (object);

  switch (prop_id)
    {
    case PROP_PROJECT_LIST:
      g_value_set_object (value, self->project_list);
      break;

    case PROP_SELECTION_MODE:
      g_value_set_boolean (value, self->selection_mode);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
spiel_greeter_set_property (GObject      *object,
                            guint         prop_id,
                            const GValue *value,
                            GParamSpec   *pspec)
{
  SpielGreeter *self = SPIEL_GREETER (object);

  switch (prop_id)
    {
    case PROP_PROJECT_LIST:
      g_assert (self->project_list == NULL);
      self->project_list = g_value_dup_object (value);
      g_signal_connect_object (self->project_list,
                               "items-changed",
                               G_CALLBACK (on_project_list_items_changed_cb),
                               self,
                               0);

      g_signal_connect_object (self->project_list,
                               "notify",
                               G_CALLBACK (on_project_list_changed_cb),
                               self,
                               0);
      update_visible_page (self);

      gtk_list_box_bind_model (self->projects_listbox,
                               G_LIST_MODEL (self->project_list),
                               create_widget_func,
                               self,
                               NULL);
      break;

    case PROP_SELECTION_MODE:
      self->selection_mode = g_value_get_boolean (value);
      if (!self->selection_mode)
        unselect_all_rows (self);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
spiel_greeter_class_init (SpielGreeterClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = spiel_greeter_dispose;
  object_class->get_property = spiel_greeter_get_property;
  object_class->set_property = spiel_greeter_set_property;

  properties[PROP_PROJECT_LIST] =
    g_param_spec_object ("project-list", "", "",
                         SPIEL_TYPE_PROJECT_LIST,
                         G_PARAM_READWRITE |
                         G_PARAM_CONSTRUCT_ONLY |
                         G_PARAM_STATIC_STRINGS);

  properties[PROP_SELECTION_MODE] =
    g_param_spec_boolean ("selection-mode", "", "",
                          FALSE,
                          G_PARAM_READWRITE |
                          G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);

  gtk_widget_class_set_template_from_resource (widget_class, "/com/feaneron/Spiel/spiel-greeter.ui");

  gtk_widget_class_bind_template_child (widget_class, SpielGreeter, navigation_view);
  gtk_widget_class_bind_template_child (widget_class, SpielGreeter, projects_listbox);
}

static void
spiel_greeter_init (SpielGreeter *self)
{
  const GActionEntry actions[] = {
    { "delete-projects", on_delete_projects_action_activated_cb },
    { "new-project", on_new_project_action_activated_cb },
  };

  gtk_widget_init_template (GTK_WIDGET (self));

  g_action_map_add_action_entries (G_ACTION_MAP (self),
                                   actions,
                                   G_N_ELEMENTS (actions),
                                   self);

  update_delete_projects_action (self);
}

GtkWindow *
spiel_greeter_new (SpielApplication *application)
{
  return g_object_new (SPIEL_TYPE_GREETER,
                       "application", application,
                       "project-list", spiel_application_get_project_list (application),
                       NULL);
}
