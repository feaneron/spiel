/*
 * spiel-project-private.h
 *
 * Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "spiel-project.h"

G_BEGIN_DECLS

SpielMediaGallery * spiel_project_get_media_gallery (SpielProject *self);

GtkSourceFile * spiel_project_get_markdown_file (SpielProject *self);

void spiel_project_create (GCancellable        *cancellable,
                           GAsyncReadyCallback  callback,
                           gpointer             user_data);

SpielProject * spiel_project_create_finish (GAsyncResult  *result,
                                            GError       **error);

void spiel_project_open (GFile               *folder,
                         GCancellable        *cancellable,
                         GAsyncReadyCallback  callback,
                         gpointer             user_data);

SpielProject * spiel_project_open_finish (GAsyncResult  *result,
                                          GError       **error);

void spiel_project_delete (SpielProject        *self,
                           GCancellable        *cancellable,
                           GAsyncReadyCallback  callback,
                           gpointer             user_data);

gboolean spiel_project_delete_finish (SpielProject  *self,
                                      GAsyncResult  *result,
                                      GError       **error);

G_END_DECLS
