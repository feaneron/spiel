/*
 * spiel-slide-feature-descriptor.c
 *
 * Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "spiel-slide-feature.h"
#include "spiel-slide-feature-descriptor.h"

#include <errno.h>
#include <gio/gio.h>
#include <limits.h>
#include <stdlib.h>

typedef enum
{
  MATCH_EXACT,
  MATCH_DIFFERENT,
  MATCH_LESSER,
  MATCH_LESSER_EQUAL,
  MATCH_GREATER,
  MATCH_GREATER_EQUAL,
} MatchType;

struct _SpielSlideFeatureDescriptor
{
  GObject parent_instance;

  char *descriptor;

  SpielFeatureType feature;

  union {

    struct {
      uint32_t level;
      MatchType match_type;
    } title;

  } data;
};

static void g_initable_iface_init (GInitableIface *iface);

G_DEFINE_FINAL_TYPE_WITH_CODE (SpielSlideFeatureDescriptor,
                               spiel_slide_feature_descriptor,
                               G_TYPE_OBJECT,
                               G_IMPLEMENT_INTERFACE (G_TYPE_INITABLE,
                                                      g_initable_iface_init))

G_DEFINE_QUARK (SpielSlideFeatureDescriptor,
                spiel_slide_feature_descriptor_error);

enum {
  PROP_0,
  PROP_DESCRIPTOR,
  N_PROPS,
};

static GParamSpec *properties [N_PROPS];

static gboolean
compare_level_with_match_type (uint32_t  a,
                               uint32_t  b,
                               MatchType match_type)
{
  switch (match_type)
    {
    case MATCH_EXACT:
      return a == b;
    case MATCH_DIFFERENT:
      return a != b;
    case MATCH_GREATER_EQUAL:
      return a >= b;
    case MATCH_LESSER_EQUAL:
      return a <= b;
    case MATCH_GREATER:
      return a > b;
    case MATCH_LESSER:
      return a < b;
    default:
      g_assert_not_reached ();
    }
}

/*
 * Parser vtable
 */

static const struct {
  const char *operand;
  MatchType match_type;
} operands_vtable[] = {
  { "!=", MATCH_DIFFERENT },
  { ">=", MATCH_GREATER_EQUAL },
  { "<=", MATCH_LESSER_EQUAL },
  { ">", MATCH_GREATER },
  { "<", MATCH_LESSER },
};

static gboolean
parse_match (const char  *start,
             const char  *end,
             int64_t     *out_value,
             MatchType   *out_match_type,
             GError     **error)
{
  g_autofree char *str = NULL;
  MatchType match_type;
  const char *aux;
  const char *aux2;
  int64_t value;
  size_t len;

  g_assert (start < end);

  len = end - start;
  str = g_malloc0 (len + 1);
  memcpy (str, start, len);

  str = g_strstrip (str);

  match_type = MATCH_EXACT;
  aux = str;

  for (size_t i = 0; i < G_N_ELEMENTS (operands_vtable); i++)
    {
      if (g_str_has_prefix (str, operands_vtable[i].operand))
        {
          match_type = operands_vtable[i].match_type;
          aux += strlen (operands_vtable[i].operand);
          break;
        }
    }

  /* Catch cases like 'title(>=)' */
  while (*aux == ' ')
    aux++;

  if (*aux == '\0')
    {
      g_set_error (error,
                   SPIEL_SLIDE_FEATURE_DESCRIPTOR_ERROR,
                   SPIEL_SLIDE_FEATURE_DESCRIPTOR_ERROR_INVALID_SYNTAX,
                   "No value");
      return FALSE;
    }

  /* Check if all remaining characters are numbers */
  aux2 = aux;
  while (*aux2 != '\0')
    {
      if (*aux2 < '0' || *aux2 > '9')
        {
          g_set_error (error,
                       SPIEL_SLIDE_FEATURE_DESCRIPTOR_ERROR,
                       SPIEL_SLIDE_FEATURE_DESCRIPTOR_ERROR_INVALID_SYNTAX,
                       "Non-digit characters");
          return FALSE;
        }

      aux2++;
    }

  value = strtoll (aux, NULL, 10);

  if (value == 0 && errno == EINVAL)
    {
      g_set_error (error,
                   SPIEL_SLIDE_FEATURE_DESCRIPTOR_ERROR,
                   SPIEL_SLIDE_FEATURE_DESCRIPTOR_ERROR_INVALID_SYNTAX,
                   "Error parsing value");
      return FALSE;
    }

  if ((value == LLONG_MIN || value == LLONG_MAX) && errno == ERANGE)
    {
      g_set_error (error,
                   SPIEL_SLIDE_FEATURE_DESCRIPTOR_ERROR,
                   SPIEL_SLIDE_FEATURE_DESCRIPTOR_ERROR_INVALID_SYNTAX,
                   "Value out of range");
      return FALSE;
    }

  if (out_match_type)
    *out_match_type = match_type;

  if (out_value)
    *out_value = value;

  return TRUE;
}

static gboolean
parse_title (SpielSlideFeatureDescriptor  *self,
             GError                      **error)
{
  const char *parenthesis_start;
  const char *parenthesis_end;
  MatchType match_type;
  int64_t value;

  g_assert (g_str_has_prefix (self->descriptor, "title"));

  parenthesis_start = self->descriptor;
  parenthesis_start += strlen ("title");

  if (*parenthesis_start != '(')
    {
      g_set_error (error,
                   SPIEL_SLIDE_FEATURE_DESCRIPTOR_ERROR,
                   SPIEL_SLIDE_FEATURE_DESCRIPTOR_ERROR_INVALID_SYNTAX,
                   "title must contain a level (e.g. title(>= 1)");
      return FALSE;
    }

  parenthesis_start += strlen ("(");

  parenthesis_end = strstr (self->descriptor, ")");

  if (!parenthesis_end)
    {
      g_set_error (error,
                   SPIEL_SLIDE_FEATURE_DESCRIPTOR_ERROR,
                   SPIEL_SLIDE_FEATURE_DESCRIPTOR_ERROR_INVALID_SYNTAX,
                   "Parenthesis not closed");
      return FALSE;
    }

  if (!parse_match (parenthesis_start, parenthesis_end, &value, &match_type, error))
    return FALSE;

  if (value < 1)
    {
      g_set_error (error,
                   SPIEL_SLIDE_FEATURE_DESCRIPTOR_ERROR,
                   SPIEL_SLIDE_FEATURE_DESCRIPTOR_ERROR_INVALID_SYNTAX,
                   "Title level must be equal or greater than 1");
      return FALSE;
    }

  self->feature = SPIEL_FEATURE_TITLE;
  self->data.title.level = (uint32_t) value;
  self->data.title.match_type = match_type;

  return TRUE;
}

static gboolean
parse_note (SpielSlideFeatureDescriptor  *self,
            GError                      **error)
{
  if (g_strcmp0 (self->descriptor, "note") != 0)
    {
      g_set_error (error,
                   SPIEL_SLIDE_FEATURE_DESCRIPTOR_ERROR,
                   SPIEL_SLIDE_FEATURE_DESCRIPTOR_ERROR_INVALID_SYNTAX,
                   "Notes don't receive any arguments");
      return FALSE;
    }

  self->feature = SPIEL_FEATURE_NOTE;

  return TRUE;
}

static gboolean
parse_media (SpielSlideFeatureDescriptor  *self,
             GError                      **error)
{
  if (g_strcmp0 (self->descriptor, "media") != 0)
    {
      g_set_error (error,
                   SPIEL_SLIDE_FEATURE_DESCRIPTOR_ERROR,
                   SPIEL_SLIDE_FEATURE_DESCRIPTOR_ERROR_INVALID_SYNTAX,
                   "Pictures don't receive any arguments");
      return FALSE;
    }

  self->feature = SPIEL_FEATURE_MEDIA;

  return TRUE;
}

static const struct {
  const char *prefix;
  gboolean (*parse) (SpielSlideFeatureDescriptor  *self,
                     GError                      **error);
} parser_vtable[] = {
  { "media", parse_media },
  { "note", parse_note },
  { "title", parse_title },
};

/*
 * GInitable interface
 */

static gboolean
spiel_slide_feature_descriptor_initable_init (GInitable     *initable,
                                              GCancellable  *cancellable,
                                              GError       **error)
{
  SpielSlideFeatureDescriptor *self = SPIEL_SLIDE_FEATURE_DESCRIPTOR (initable);

  for (size_t i = 0; i < G_N_ELEMENTS (parser_vtable); i++)
    {
      if (g_str_has_prefix (self->descriptor, parser_vtable[i].prefix))
        return parser_vtable[i].parse (self, error);
    }

  g_set_error (error,
               SPIEL_SLIDE_FEATURE_DESCRIPTOR_ERROR,
               SPIEL_SLIDE_FEATURE_DESCRIPTOR_ERROR_UNKNOWN_FEATURE,
               "Unknown feature");

  return FALSE;
}

static void
g_initable_iface_init (GInitableIface *iface)
{
  iface->init = spiel_slide_feature_descriptor_initable_init;
}


/*
 * GObject overrides
 */

static void
spiel_slide_feature_descriptor_finalize (GObject *object)
{
  SpielSlideFeatureDescriptor *self = (SpielSlideFeatureDescriptor *)object;

  g_clear_pointer (&self->descriptor, g_free);

  G_OBJECT_CLASS (spiel_slide_feature_descriptor_parent_class)->finalize (object);
}

static void
spiel_slide_feature_descriptor_get_property (GObject    *object,
                                             guint       prop_id,
                                             GValue     *value,
                                             GParamSpec *pspec)
{
  SpielSlideFeatureDescriptor *self = SPIEL_SLIDE_FEATURE_DESCRIPTOR (object);

  switch (prop_id)
    {
    case PROP_DESCRIPTOR:
      g_value_set_string (value, self->descriptor);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
spiel_slide_feature_descriptor_set_property (GObject      *object,
                                             guint         prop_id,
                                             const GValue *value,
                                             GParamSpec   *pspec)
{
  SpielSlideFeatureDescriptor *self = SPIEL_SLIDE_FEATURE_DESCRIPTOR (object);

  switch (prop_id)
    {
    case PROP_DESCRIPTOR:
      g_assert (self->descriptor == NULL);
      self->descriptor = g_value_dup_string (value);
      g_assert (self->descriptor != NULL);
      self->descriptor = g_strstrip (self->descriptor);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
spiel_slide_feature_descriptor_class_init (SpielSlideFeatureDescriptorClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = spiel_slide_feature_descriptor_finalize;
  object_class->get_property = spiel_slide_feature_descriptor_get_property;
  object_class->set_property = spiel_slide_feature_descriptor_set_property;

  properties[PROP_DESCRIPTOR] =
    g_param_spec_string ("descriptor", NULL, NULL,
                         NULL,
                         G_PARAM_READWRITE |
                         G_PARAM_CONSTRUCT_ONLY |
                         G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
spiel_slide_feature_descriptor_init (SpielSlideFeatureDescriptor *self)
{
}

/**
 * spiel_slide_feature_descriptor_from_string: (constructor)
 * @descriptor: a string
 * @error: (nullable): return location for an error
 *
 * Returns: (transfer full)(nullable): a #SpielSlideFeatureDescriptor
 */
SpielSlideFeatureDescriptor *
spiel_slide_feature_descriptor_from_string (const char  *descriptor,
                                            GError     **error)
{
  g_return_val_if_fail (descriptor != NULL, NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  return g_initable_new (SPIEL_TYPE_SLIDE_FEATURE_DESCRIPTOR,
                         NULL,
                         error,
                         "descriptor", descriptor,
                         NULL);
}

gboolean
spiel_slide_feature_descriptor_match (SpielSlideFeatureDescriptor *self,
                                      const SpielSlideFeature     *feature)
{
  g_return_val_if_fail (SPIEL_IS_SLIDE_FEATURE_DESCRIPTOR (self), FALSE);
  g_return_val_if_fail (feature != NULL, FALSE);

  if (self->feature != spiel_slide_feature_get_feature_type (feature))
    return FALSE;

  switch (self->feature)
    {
    case SPIEL_FEATURE_TITLE:
      return compare_level_with_match_type (spiel_slide_feature_get_title_level (feature),
                                            self->data.title.level,
                                            self->data.title.match_type);

    case SPIEL_FEATURE_MEDIA:
    case SPIEL_FEATURE_NOTE:
      return TRUE;

    default:
      g_assert_not_reached ();
    }
}

gboolean
spiel_slide_feature_descriptor_equal (gconstpointer a,
                                      gconstpointer b)
{
  const SpielSlideFeatureDescriptor *descriptor_a = a;
  const SpielSlideFeatureDescriptor *descriptor_b = b;

  if (descriptor_a->feature != descriptor_b->feature)
    return FALSE;

  switch (descriptor_a->feature)
    {
    case SPIEL_FEATURE_TITLE:
      return descriptor_a->data.title.level == descriptor_b->data.title.level &&
             descriptor_a->data.title.match_type == descriptor_b->data.title.match_type;

    case SPIEL_FEATURE_MEDIA:
    case SPIEL_FEATURE_NOTE:
      return TRUE;

    default:
      g_assert_not_reached ();
    }
}
