/*
 * spiel-project.c
 *
 * Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "spiel-project-private.h"

#include "spiel-buffer.h"
#include "spiel-media-gallery.h"
#include "spiel-slide-extractor.h"

#include <gtksourceview/gtksource.h>
#include <glib/gi18n.h>

#define G_SETTINGS_ENABLE_BACKEND
#include <gio/gsettingsbackend.h>

struct _SpielProject
{
  GObject parent_instance;

  char *id;
  char *name;
  GFile *folder;

  SpielMediaGallery *media_gallery;

  SpielProjectState state;

  struct {
    GFile *file;
    GSettings *settings;
  } settings;

  struct {
    GFile *file;
    GtkSourceFile *source_file;
    SpielBuffer *buffer;
    SpielSlideExtractor *extractor;
  } markdown;
};

G_DEFINE_FINAL_TYPE (SpielProject, spiel_project, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_ID,
  PROP_NAME,
  PROP_SLIDES,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];


/*
 * Auxiliary methods
 */

static inline char *
project_path_from_uuid (const char *uuid)
{
  return g_build_filename (g_get_user_data_dir (), uuid, NULL);
}

static inline void
load_gsettings_from_file (SpielProject *self)
{
  g_autoptr (GSettingsBackend) backend = NULL;
  g_autoptr (GSettings) settings = NULL;
  g_autoptr (GFile) settings_file = NULL;
  g_autofree char *settings_path = NULL;
  g_autofree char *schema_path = NULL;

  settings_file = g_file_get_child (self->folder, "settings.ini");
  settings_path = g_file_get_path (settings_file);
  schema_path = g_strdup_printf ("/com/feaneron/Spiel/Project/%s/", self->id);

  backend = g_keyfile_settings_backend_new (settings_path, "/", NULL);
  settings = g_settings_new_with_backend_and_path ("com.feaneron.Spiel.Project",
                                                   backend,
                                                   schema_path);

  g_settings_bind (settings, "name",
                   self, "name",
                   G_SETTINGS_BIND_DEFAULT);

  self->settings.settings = g_steal_pointer (&settings);
}

static inline void
load_media_gallery (SpielProject *self)
{
  g_assert (self->settings.settings != NULL);
  g_assert (self->media_gallery == NULL);

  self->media_gallery = spiel_media_gallery_new ();
}

static inline void
load_project (SpielProject *self)
{
  load_gsettings_from_file (self);
  load_media_gallery (self);
}


/*
 * Callbacks
 */

static void
buffer_loaded_cb (GObject      *source,
                  GAsyncResult *result,
                  gpointer      user_data)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GTask) task = user_data;
  SpielProject *self;

  self = g_task_get_source_object (task);

  if (!spiel_buffer_load_finish (SPIEL_BUFFER (source), result, &error))
    {
      if (!g_error_matches (error, G_IO_ERROR, G_IO_ERROR_CANCELLED))
        {
          self->state = SPIEL_PROJECT_STATE_UNLOADED;
          g_task_return_error (task, g_steal_pointer (&error));
        }
      return;
    }

  self->markdown.extractor = spiel_slide_extractor_new (self->markdown.buffer);

  self->state = SPIEL_PROJECT_STATE_LOADED;
  g_task_return_boolean (task, TRUE);

  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_SLIDES]);
}

static void
markdown_file_written_cb (GObject      *source,
                          GAsyncResult *result,
                          gpointer      user_data)
{
  g_autoptr (GFileOutputStream) output_stream = NULL;
  g_autoptr (GError) error = NULL;
  g_autoptr (GTask) task = NULL;
  SpielProject *project;

  task = user_data;

  if (!g_output_stream_write_all_finish (G_OUTPUT_STREAM (source), result, NULL, &error))
    {
      g_assert (output_stream == NULL);
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  g_debug ("Loading project settings");

  project = g_task_get_task_data (task);
  load_project (project);

  /* Defaults */
  g_settings_set_string (project->settings.settings, "name", _("Unnamed Project"));

  g_task_return_pointer (task, g_object_ref (project), g_object_unref);
}

static void
markdown_file_created_cb (GObject      *source,
                          GAsyncResult *result,
                          gpointer      user_data)
{
  g_autoptr (GFileOutputStream) output_stream = NULL;
  g_autoptr (GError) error = NULL;
  g_autoptr (GTask) task = NULL;
  GCancellable *cancellable;

  task = user_data;
  output_stream = g_file_create_finish (G_FILE (source), result, &error);

  if (error != NULL)
    {
      g_assert (output_stream == NULL);
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  cancellable = g_task_get_cancellable (task);

  g_output_stream_write_all_async (G_OUTPUT_STREAM (output_stream),
                                   "Hi!", /* TODO: better default */
                                   strlen ("Hi!"),
                                   G_PRIORITY_DEFAULT,
                                   cancellable,
                                   markdown_file_written_cb,
                                   g_steal_pointer (&task));
}

static void
delete_project_in_thread_cb (GTask        *task,
                             gpointer      source_object,
                             gpointer      task_data,
                             GCancellable *cancellable)
{
  g_autoptr (GFile) settings_file = NULL;
  g_autoptr (GError) error = NULL;
  SpielProject *self;

  self = SPIEL_PROJECT (source_object);

  /* TODO: check if files aren't busy */

  if (!g_file_delete (self->markdown.file, cancellable, &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  settings_file = g_file_get_child (self->folder, "settings.ini");
  if (!g_file_delete (settings_file, cancellable, &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  /*
   * Delete the folder last, as if the folder is not empty, it'll
   * error. We actually want it to error in that case.
   */
  if (!g_file_delete (self->folder, cancellable, &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  g_task_return_boolean (task, TRUE);
}


/*
 * GObject overrides
 */

static void
spiel_project_finalize (GObject *object)
{
  SpielProject *self = (SpielProject *)object;

  g_clear_object (&self->media_gallery);

  g_clear_object (&self->folder);
  g_clear_object (&self->markdown.extractor);
  g_clear_object (&self->markdown.buffer);
  g_clear_object (&self->markdown.file);
  g_clear_object (&self->markdown.source_file);

  g_clear_pointer (&self->id, g_free);
  g_clear_pointer (&self->name, g_free);

  G_OBJECT_CLASS (spiel_project_parent_class)->finalize (object);
}

static void
spiel_project_get_property (GObject    *object,
                            guint       prop_id,
                            GValue     *value,
                            GParamSpec *pspec)
{
  SpielProject *self = SPIEL_PROJECT (object);

  switch (prop_id)
    {
    case PROP_ID:
      g_value_set_string (value, self->id);
      break;

    case PROP_NAME:
      g_value_set_string (value, self->name);
      break;

    case PROP_SLIDES:
      g_value_set_object (value, self->markdown.extractor ? spiel_slide_extractor_get_slides (self->markdown.extractor) : NULL);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
spiel_project_set_property (GObject      *object,
                            guint         prop_id,
                            const GValue *value,
                            GParamSpec   *pspec)
{
  SpielProject *self = SPIEL_PROJECT (object);

  switch (prop_id)
    {
    case PROP_ID:
      g_assert (self->id == NULL);
      self->id = g_value_dup_string (value);
      break;

    case PROP_NAME:
      spiel_project_set_name (self, g_value_get_string (value));
      break;

    case PROP_SLIDES:
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
spiel_project_class_init (SpielProjectClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = spiel_project_finalize;
  object_class->get_property = spiel_project_get_property;
  object_class->set_property = spiel_project_set_property;

  properties[PROP_ID] =
    g_param_spec_string ("id", "", "", NULL,
                         G_PARAM_READWRITE |
                         G_PARAM_CONSTRUCT_ONLY |
                         G_PARAM_STATIC_STRINGS);

  properties[PROP_NAME] =
    g_param_spec_string ("name", "", "", NULL,
                         G_PARAM_READWRITE |
                         G_PARAM_STATIC_STRINGS);

  properties[PROP_SLIDES] =
    g_param_spec_object ("slides", NULL, NULL,
                         G_TYPE_LIST_MODEL,
                         G_PARAM_READABLE |
                         G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
spiel_project_init (SpielProject *self)
{
  self->state = SPIEL_PROJECT_STATE_UNLOADED;
}

const char *
spiel_project_get_id (SpielProject *self)
{
  g_return_val_if_fail (SPIEL_IS_PROJECT (self), NULL);

  return self->id;
}

SpielProjectState
spiel_project_get_state (SpielProject *self)
{
  g_return_val_if_fail (SPIEL_IS_PROJECT (self), -1);

  return self->state;
}

const char *
spiel_project_get_name (SpielProject *self)
{
  g_return_val_if_fail (SPIEL_IS_PROJECT (self), NULL);

  return self->name;
}

void
spiel_project_set_name (SpielProject *self,
                        const char   *name)
{
  g_return_if_fail (SPIEL_IS_PROJECT (self));

  if (g_set_str (&self->name, name))
    g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_NAME]);
}

SpielMediaGallery *
spiel_project_get_media_gallery (SpielProject *self)
{
  g_return_val_if_fail (SPIEL_IS_PROJECT (self), NULL);

  return self->media_gallery;
}

GtkSourceFile *
spiel_project_get_markdown_file (SpielProject *self)
{
  g_return_val_if_fail (SPIEL_IS_PROJECT (self), NULL);

  return self->markdown.source_file;
}

void
spiel_project_create (GCancellable        *cancellable,
                      GAsyncReadyCallback  callback,
                      gpointer             user_data)
{
  g_autoptr (SpielProject) project = NULL;
  g_autoptr (GError) error = NULL;
  g_autoptr (GTask) task = NULL;
  g_autofree char *uuid = NULL;
  g_autofree char *path = NULL;

  do {
    g_clear_pointer (&uuid, g_free);
    g_clear_pointer (&path, g_free);

    uuid = g_uuid_string_random ();
    path = project_path_from_uuid (uuid);
  } while (g_file_test (path, G_FILE_TEST_IS_DIR));

  project = g_object_new (SPIEL_TYPE_PROJECT,
                          "id", uuid,
                          NULL);
  project->folder = g_file_new_for_path (path);
  project->markdown.file = g_file_get_child (project->folder, "document.md");
  project->markdown.source_file = gtk_source_file_new ();
  gtk_source_file_set_location (project->markdown.source_file,
                                project->markdown.file);

  task = g_task_new (NULL, cancellable, callback, user_data);
  g_task_set_source_tag (task, spiel_project_create);
  g_task_set_task_data (task, g_object_ref (project), g_object_unref);

  g_debug ("Creating folder for project %s", uuid);

  if (!g_file_make_directory_with_parents (project->folder, cancellable, &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  g_debug ("Creating markdown file");

  g_file_create_async (project->markdown.file,
                       G_FILE_CREATE_NONE,
                       G_PRIORITY_DEFAULT,
                       cancellable,
                       markdown_file_created_cb,
                       g_steal_pointer (&task));
}

SpielProject *
spiel_project_create_finish (GAsyncResult  *result,
                             GError       **error)
{
  g_return_val_if_fail (!error || !*error, NULL);
  g_return_val_if_fail (g_task_is_valid (result, NULL), NULL);
  g_return_val_if_fail (g_task_get_source_tag (G_TASK (result)) == spiel_project_create, NULL);

  return g_task_propagate_pointer (G_TASK (result), error);
}

void
spiel_project_open (GFile               *folder,
                    GCancellable        *cancellable,
                    GAsyncReadyCallback  callback,
                    gpointer             user_data)
{
  g_autoptr (SpielProject) project = NULL;
  g_autoptr (GTask) task = NULL;
  g_autofree char *uuid = NULL;

  g_return_if_fail (G_IS_FILE (folder));

  uuid = g_file_get_basename (folder);
  g_return_if_fail (g_uuid_string_is_valid (uuid));

  project = g_object_new (SPIEL_TYPE_PROJECT,
                          "id", uuid,
                          NULL);
  project->folder = g_object_ref (folder);

  g_debug ("Loading project document");

  project->markdown.file = g_file_get_child (project->folder, "document.md");
  project->markdown.source_file = gtk_source_file_new ();
  gtk_source_file_set_location (project->markdown.source_file,
                                project->markdown.file);

  task = g_task_new (NULL, cancellable, callback, user_data);
  g_task_set_source_tag (task, spiel_project_open);
  g_task_set_task_data (task, g_object_ref (folder), g_object_unref);

  /* Settings */
  g_debug ("Loading project settings");

  load_project (project);

  g_task_return_pointer (task, g_steal_pointer (&project), NULL);
}

SpielProject *
spiel_project_open_finish (GAsyncResult  *result,
                           GError       **error)
{
  g_return_val_if_fail (!error || !*error, NULL);
  g_return_val_if_fail (g_task_is_valid (result, NULL), NULL);
  g_return_val_if_fail (g_task_get_source_tag (G_TASK (result)) == spiel_project_open, NULL);

  return g_task_propagate_pointer (G_TASK (result), error);
}

void
spiel_project_delete (SpielProject        *self,
                      GCancellable        *cancellable,
                      GAsyncReadyCallback  callback,
                      gpointer             user_data)
{
  g_autoptr (GTask) task = NULL;

  g_return_if_fail (SPIEL_IS_PROJECT (self));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, spiel_project_delete);
  g_task_run_in_thread (task, delete_project_in_thread_cb);
}

gboolean
spiel_project_delete_finish (SpielProject  *self,
                             GAsyncResult  *result,
                             GError       **error)
{
  g_return_val_if_fail (!error || !*error, FALSE);
  g_return_val_if_fail (g_task_is_valid (result, self), FALSE);
  g_return_val_if_fail (g_task_get_source_tag (G_TASK (result)) == spiel_project_delete, FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

void
spiel_project_load (SpielProject        *self,
                    GCancellable        *cancellable,
                    GAsyncReadyCallback  callback,
                    gpointer             user_data)
{
  g_autoptr (GTask) task = NULL;

  g_return_if_fail (SPIEL_IS_PROJECT (self));
  g_return_if_fail (self->state == SPIEL_PROJECT_STATE_UNLOADED);

  g_assert (self->markdown.buffer == NULL);
  g_assert (self->markdown.extractor == NULL);

  self->state = SPIEL_PROJECT_STATE_LOADING;

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, spiel_project_load);

  self->markdown.buffer = SPIEL_BUFFER (spiel_buffer_new (self));

  spiel_buffer_load (SPIEL_BUFFER (self->markdown.buffer),
                     cancellable,
                     buffer_loaded_cb,
                     g_steal_pointer (&task));
}

gboolean
spiel_project_load_finish (SpielProject  *self,
                           GAsyncResult  *result,
                           GError       **error)
{
  g_return_val_if_fail (!error || !*error, FALSE);
  g_return_val_if_fail (g_task_is_valid (result, self), FALSE);
  g_return_val_if_fail (g_task_get_source_tag (G_TASK (result)) == spiel_project_load, FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

SpielBuffer *
spiel_project_get_buffer (SpielProject *self)
{
  g_return_val_if_fail (SPIEL_IS_PROJECT (self), NULL);

  return self->markdown.buffer;
}

void
spiel_project_unload_markdown (SpielProject *self)
{
  g_return_if_fail (SPIEL_IS_PROJECT (self));

  g_clear_object (&self->markdown.extractor);
  g_clear_object (&self->markdown.buffer);

  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_SLIDES]);
}

GListModel *
spiel_project_get_slides (SpielProject *self)
{
  g_return_val_if_fail (SPIEL_IS_PROJECT (self), NULL);

  if (!self->markdown.extractor)
    {
      g_warning ("Trying to get slides before loading the project");
      return NULL;
    }

  return spiel_slide_extractor_get_slides (self->markdown.extractor);
}
