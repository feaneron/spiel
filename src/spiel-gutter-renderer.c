/*
 * spiel-gutter-renderer.c
 *
 * Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "spiel-gutter-renderer.h"

#include "spiel-buffer.h"
#include "spiel-project.h"
#include "spiel-slide.h"

enum {
  FOREGROUND,
  BACKGROUND,
  LINE_BACKGROUND,
};

struct _SpielGutterRenderer
{
  GtkSourceGutterRenderer parent_instance;

  GSignalGroup *buffer_signals;
  GHashTable *slide_at_line;
  PangoLayout *cached_layout;

  uint32_t last_cursor_line;
  double draw_width;
  double draw_width_with_margin;

  struct {
    GdkRGBA fg;
    GdkRGBA bg;
    gboolean bold;
  } selection;

  SpielProject *project;
};

G_DEFINE_FINAL_TYPE (SpielGutterRenderer, spiel_gutter_renderer, GTK_SOURCE_TYPE_GUTTER_RENDERER)

static GQuark selection_quark;
static GQuark slide_quark;

enum {
  PROP_0,
  PROP_PROJECT,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static gboolean
get_style_rgba (GtkSourceStyleScheme *scheme,
                const gchar          *style_name,
                int                   type,
                GdkRGBA              *rgba)
{
  GtkSourceLanguageManager *langs;
  GtkSourceLanguage *def;
  GtkSourceStyle *style = NULL;
  const char *fallback = style_name;

  g_assert (!scheme || GTK_SOURCE_IS_STYLE_SCHEME (scheme));
  g_assert (style_name != NULL);
  g_assert (type == FOREGROUND || type == BACKGROUND || type == LINE_BACKGROUND);
  g_assert (rgba != NULL);

  memset (rgba, 0, sizeof *rgba);

  if (scheme == NULL)
    return FALSE;

  langs = gtk_source_language_manager_get_default ();
  def = gtk_source_language_manager_get_language (langs, "def");

  g_assert (def != NULL);

  while (style == NULL && fallback != NULL)
    {
      if (!(style = gtk_source_style_scheme_get_style (scheme, fallback)))
        fallback = gtk_source_language_get_style_fallback (def, fallback);
    }

  if (style != NULL)
    {
      const char *name;
      const char *name_set;
      g_autofree gchar *str = NULL;
      gboolean set = FALSE;

      switch (type)
        {
        default:
        case FOREGROUND:
          name = "foreground";
          name_set = "foreground-set";
          break;

        case BACKGROUND:
          name = "background";
          name_set = "background-set";
          break;

        case LINE_BACKGROUND:
          name = "line-background";
          name_set = "line-background-set";
          break;
        }

      g_object_get (style,
                    name, &str,
                    name_set, &set,
                    NULL);

      if (str != NULL)
        gdk_rgba_parse (rgba, str);

      return set && rgba->alpha > .0;
    }

  return FALSE;
}

static inline gboolean
lookup_color (GtkStyleContext *context,
              const char      *name,
              GdkRGBA         *color)
{
G_GNUC_BEGIN_IGNORE_DEPRECATIONS
  return gtk_style_context_lookup_color (context, name, color);
G_GNUC_END_IGNORE_DEPRECATIONS
}

static void
reload_style (SpielGutterRenderer *self)
{
  GtkSourceStyleScheme *scheme;
  GtkStyleContext *context;
  GtkSourceBuffer *buffer;
  GtkSourceView *view;
  GdkRGBA fg;
  gboolean had_sel_fg = FALSE;

  view = gtk_source_gutter_renderer_get_view (GTK_SOURCE_GUTTER_RENDERER (self));
  if (view == NULL)
    return;

  buffer = gtk_source_gutter_renderer_get_buffer (GTK_SOURCE_GUTTER_RENDERER (self));
  scheme = gtk_source_buffer_get_style_scheme (buffer);

  G_GNUC_BEGIN_IGNORE_DEPRECATIONS
  context = gtk_widget_get_style_context (GTK_WIDGET (self));
  gtk_style_context_get_color (context, &fg);
  G_GNUC_END_IGNORE_DEPRECATIONS

  if (!get_style_rgba (scheme, "selection", FOREGROUND, &self->selection.fg))
    {
      if (!lookup_color (context, "theme_selected_fg_color", &self->selection.fg))
        self->selection.fg = fg;
    }
  else
    {
      had_sel_fg = TRUE;
    }

  if (!get_style_rgba (scheme, "selection", BACKGROUND, &self->selection.bg))
    {
      if (!lookup_color (context, "theme_selected_bg_color", &self->selection.bg))
        lookup_color (context, "accent_bg_color", &self->selection.bg);
      /* Make selection like libadwaita would */
      self->selection.bg.alpha = .3;
    }
  else if (!had_sel_fg)
    {
      /* gtksourceview will fixup bad selections */
      self->selection.bg.alpha = .3;
    }
}

static void
draw_selection_bg (SpielGutterRenderer  *self,
                   GtkSnapshot          *snapshot,
                   GtkSourceGutterLines *lines,
                   unsigned int          line,
                   double                line_y,
                   double                width,
                   double                height)
{
  GskRoundedRect rounded_rect;
  gboolean is_first_line;
  gboolean is_last_line;

  if (self->selection.bg.alpha == .0)
    return;

  rounded_rect = GSK_ROUNDED_RECT_INIT (2, line_y, width - 2, height);

  is_first_line = line == 0 ||
                  line == gtk_source_gutter_lines_get_first (lines) ||
                  !gtk_source_gutter_lines_has_qclass (lines, line - 1, selection_quark);

  is_last_line = line == gtk_source_gutter_lines_get_last (lines) ||
                 !gtk_source_gutter_lines_has_qclass (lines, line + 1, selection_quark);

  if (is_first_line)
    rounded_rect.corner[0] = GRAPHENE_SIZE_INIT (9, 9);

  if (is_last_line)
    rounded_rect.corner[3] = GRAPHENE_SIZE_INIT (9, 9);

  gtk_snapshot_push_rounded_clip (snapshot, &rounded_rect);
  gtk_snapshot_append_color (snapshot,
                             &self->selection.bg,
                             &GRAPHENE_RECT_INIT (2, line_y, width - 2, height));
  gtk_snapshot_pop (snapshot);
}

static void
draw_slide_indicator (SpielGutterRenderer *self,
                      GtkSnapshot         *snapshot,
                      unsigned int         line,
                      double               line_y,
                      double               width,
                      double               height)
{
  GskRoundedRect rounded_rect;
  graphene_point_t text_pos;
  GdkRGBA color;
  uint32_t slide_index;
  double radius;
  double min;
  char text[10];
  int text_height;
  int text_width;

  gdk_rgba_parse (&color, "#ff0000");

  min = MIN (width, height);
  radius = min / 2.0;

  rounded_rect = GSK_ROUNDED_RECT_INIT (0, line_y, min, min);
  gsk_rounded_rect_init (&rounded_rect,
                         &GRAPHENE_RECT_INIT (0, line_y, min, min),
                         &GRAPHENE_SIZE_INIT (radius, radius),
                         &GRAPHENE_SIZE_INIT (radius, radius),
                         &GRAPHENE_SIZE_INIT (radius, radius),
                         &GRAPHENE_SIZE_INIT (radius, radius));
  gsk_rounded_rect_offset (&rounded_rect, floor ((width - min) / 2.0), floor ((height - min) / 2.0));

  gtk_snapshot_push_rounded_clip (snapshot, &rounded_rect);
  gtk_snapshot_append_color (snapshot,
                             &self->selection.bg,
                             &GRAPHENE_RECT_INIT (0, line_y, width, height));
  gtk_snapshot_pop (snapshot);

  /* Slide number */
  slide_index = GPOINTER_TO_UINT (g_hash_table_lookup (self->slide_at_line,
                                                       GUINT_TO_POINTER (line)));

  snprintf (text, sizeof text, "%u", slide_index + 1);
  pango_layout_set_text (self->cached_layout, text, sizeof text);
  pango_layout_get_pixel_size (self->cached_layout, &text_width, &text_height);

  text_pos = GRAPHENE_POINT_INIT ((width - text_width) / 2.0,
                                  line_y + (height - text_height) / 2.0);

  gtk_snapshot_translate (snapshot, &text_pos);
  gtk_snapshot_append_layout (snapshot, self->cached_layout, &self->selection.fg);
  gtk_snapshot_translate (snapshot, &GRAPHENE_POINT_INIT (-text_pos.x, -text_pos.y));
}


/*
 * Callbacks
 */

static void
on_buffer_cursor_moved_cb (GtkTextBuffer       *buffer,
                           SpielGutterRenderer *self)
{
  GtkTextIter iter;
  GtkTextMark *insert;
  guint line;

  g_assert (SPIEL_IS_GUTTER_RENDERER (self));
  g_assert (SPIEL_IS_BUFFER (buffer));

  insert = gtk_text_buffer_get_insert (buffer);
  gtk_text_buffer_get_iter_at_mark (buffer, &iter, insert);
  line = gtk_text_iter_get_line (&iter);

  if (line != self->last_cursor_line || gtk_text_buffer_get_has_selection (buffer))
    gtk_widget_queue_draw (GTK_WIDGET (self));

  self->last_cursor_line = line;
}


/*
 * GtkSourceGutterRenderer overrides
 */

static void
spiel_gutter_renderer_begin (GtkSourceGutterRenderer *renderer,
                             GtkSourceGutterLines    *lines)
{
  SpielGutterRenderer *self = (SpielGutterRenderer *)renderer;
  GtkTextBuffer *buffer;
  GtkTextView *view;
  GtkTextIter selection_begin;
  GtkTextIter selection_end;
  GListModel *slides;

  g_assert (SPIEL_IS_GUTTER_RENDERER (renderer));
  g_assert (lines != NULL);

  buffer = GTK_TEXT_BUFFER (gtk_source_gutter_renderer_get_buffer (renderer));
  view = GTK_TEXT_VIEW (gtk_source_gutter_renderer_get_view (renderer));

  self->draw_width = gtk_widget_get_width (GTK_WIDGET (self));
  self->draw_width_with_margin = self->draw_width + gtk_text_view_get_left_margin (view);

  g_clear_object (&self->cached_layout);
  self->cached_layout = gtk_widget_create_pango_layout (GTK_WIDGET (self), NULL);

  /*
   * Add quark for line selections which will display all the way to the
   * left margin so that we can draw selection borders (rounded corners
   * which extend under the line numbers).
   */
  if (gtk_text_buffer_get_selection_bounds (buffer, &selection_begin, &selection_end))
    {
      int first_selected = -1, last_selected = -1;

      gtk_text_iter_order (&selection_begin, &selection_end);

      if (gtk_text_iter_starts_line (&selection_begin))
        first_selected = gtk_text_iter_get_line (&selection_begin);
      else if (gtk_text_iter_get_line (&selection_begin) != gtk_text_iter_get_line (&selection_end))
        first_selected = gtk_text_iter_get_line (&selection_begin) + 1;

      if (!gtk_text_iter_starts_line (&selection_end))
        last_selected = gtk_text_iter_get_line (&selection_end);
      else if (gtk_text_iter_get_line (&selection_begin) != gtk_text_iter_get_line (&selection_end))
        last_selected = gtk_text_iter_get_line (&selection_end) - 1;

      if (first_selected != -1 && last_selected != -1)
        {
          first_selected = MAX (first_selected, gtk_source_gutter_lines_get_first (lines));
          last_selected = MIN (last_selected, gtk_source_gutter_lines_get_last (lines));

          for (int i = first_selected; i <= last_selected; i++)
            gtk_source_gutter_lines_add_qclass (lines, i, selection_quark);
        }
    }

  /* Slide indexes */
  g_hash_table_remove_all (self->slide_at_line);

  slides = spiel_project_get_slides (self->project);
  for (uint32_t i = 0; i < g_list_model_get_n_items (slides); i++)
    {
      g_autoptr (SpielSlide) slide = NULL;
      GtkTextMark *slide_start;
      GtkTextMark *slide_end;
      GtkTextIter slide_start_iter;
      GtkTextIter slide_end_iter;
      int slide_line;

      slide = g_list_model_get_item (slides, i);
      spiel_slide_get_marks (slide, &slide_start, &slide_end);
      gtk_text_buffer_get_iter_at_mark (buffer, &slide_end_iter, slide_end);

      if (gtk_text_iter_get_line (&slide_end_iter) < gtk_source_gutter_lines_get_first (lines))
        continue;

      gtk_text_buffer_get_iter_at_mark (buffer, &slide_start_iter, slide_start);
      if (gtk_text_iter_get_line (&slide_start_iter) > gtk_source_gutter_lines_get_last (lines))
        break;

      slide_line = MAX (gtk_text_iter_get_line (&slide_start_iter),
                        gtk_source_gutter_lines_get_first (lines));

      gtk_source_gutter_lines_add_qclass (lines, slide_line, slide_quark);

      g_hash_table_insert (self->slide_at_line,
                           GUINT_TO_POINTER (slide_line),
                           GUINT_TO_POINTER (i));
    }
}

static void
spiel_gutter_renderer_snapshot_line (GtkSourceGutterRenderer *renderer,
                                     GtkSnapshot             *snapshot,
                                     GtkSourceGutterLines    *lines,
                                     guint                    line)
{
  SpielGutterRenderer *self = (SpielGutterRenderer *)renderer;
  int line_height;
  int line_y;

  g_assert (SPIEL_IS_GUTTER_RENDERER (self));
  g_assert (GTK_IS_SNAPSHOT (snapshot));
  g_assert (lines != NULL);

  /*
   * This is our primary draw routine. It is called for every line that
   * is visible. We are incredibly sensitive to performance churn here
   * so it is important that we be as minimal as possible while
   * retaining the features we need.
   */

  gtk_source_gutter_lines_get_line_yrange (lines, line, GTK_SOURCE_GUTTER_RENDERER_ALIGNMENT_MODE_CELL, &line_y, &line_height);

  if (gtk_source_gutter_lines_has_qclass (lines, line, selection_quark))
    draw_selection_bg (self, snapshot, lines, line, line_y, self->draw_width_with_margin, line_height);

  if (gtk_source_gutter_lines_has_qclass (lines, line, slide_quark))
    draw_slide_indicator (self, snapshot, line, line_y, self->draw_width_with_margin, line_height);
}

static void
spiel_gutter_renderer_end (GtkSourceGutterRenderer *renderer)
{
  SpielGutterRenderer *self = (SpielGutterRenderer *)renderer;

  g_assert (SPIEL_IS_GUTTER_RENDERER (self));

  GTK_SOURCE_GUTTER_RENDERER_CLASS (spiel_gutter_renderer_parent_class)->end (renderer);

  g_clear_object (&self->cached_layout);
}

static void
spiel_gutter_renderer_change_buffer (GtkSourceGutterRenderer *renderer,
                                     GtkSourceBuffer         *old_buffer)
{
  SpielGutterRenderer *self = (SpielGutterRenderer *)renderer;
  GtkSourceBuffer *buffer;

  g_assert (SPIEL_IS_GUTTER_RENDERER (self));
  g_assert (!old_buffer || GTK_SOURCE_IS_BUFFER (old_buffer));

  buffer = gtk_source_gutter_renderer_get_buffer (GTK_SOURCE_GUTTER_RENDERER (self));
  g_signal_group_set_target (self->buffer_signals, buffer);

  reload_style (self);
}


/*
 * GtkWidget overrides
 */

static void
spiel_gutter_renderer_css_changed (GtkWidget         *widget,
                                   GtkCssStyleChange *change)
{
  SpielGutterRenderer *self = (SpielGutterRenderer *)widget;

  g_assert (SPIEL_IS_GUTTER_RENDERER (self));

  GTK_WIDGET_CLASS (spiel_gutter_renderer_parent_class)->css_changed (widget, change);

  reload_style (self);
}


/*
 * GObject overrides
 */

static void
spiel_gutter_renderer_dispose (GObject *object)
{
  SpielGutterRenderer *self = (SpielGutterRenderer *)object;

  g_clear_pointer (&self->slide_at_line, g_hash_table_destroy);
  g_clear_object (&self->buffer_signals);
  g_clear_object (&self->project);

  G_OBJECT_CLASS (spiel_gutter_renderer_parent_class)->dispose (object);
}

static void
spiel_gutter_renderer_get_property (GObject    *object,
                                    guint       prop_id,
                                    GValue     *value,
                                    GParamSpec *pspec)
{
  SpielGutterRenderer *self = SPIEL_GUTTER_RENDERER (object);

  switch (prop_id)
    {
    case PROP_PROJECT:
      g_value_set_object (value, self->project);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
spiel_gutter_renderer_set_property (GObject      *object,
                                    guint         prop_id,
                                    const GValue *value,
                                    GParamSpec   *pspec)
{
  SpielGutterRenderer *self = SPIEL_GUTTER_RENDERER (object);

  switch (prop_id)
    {
    case PROP_PROJECT:
      g_assert (self->project == NULL);
      self->project = g_value_dup_object (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
spiel_gutter_renderer_class_init (SpielGutterRendererClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  GtkSourceGutterRendererClass *renderer_class = GTK_SOURCE_GUTTER_RENDERER_CLASS (klass);

  object_class->dispose = spiel_gutter_renderer_dispose;
  object_class->get_property = spiel_gutter_renderer_get_property;
  object_class->set_property = spiel_gutter_renderer_set_property;

  widget_class->css_changed = spiel_gutter_renderer_css_changed;

  renderer_class->begin = spiel_gutter_renderer_begin;
  renderer_class->snapshot_line = spiel_gutter_renderer_snapshot_line;
  renderer_class->end = spiel_gutter_renderer_end;
  renderer_class->change_buffer = spiel_gutter_renderer_change_buffer;

  properties[PROP_PROJECT] =
    g_param_spec_object ("project", "", "",
                         SPIEL_TYPE_PROJECT,
                         G_PARAM_READWRITE |
                         G_PARAM_CONSTRUCT_ONLY |
                         G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);

  selection_quark = g_quark_from_static_string ("spiel-selection");
  slide_quark = g_quark_from_static_string ("spiel-slide");
}

static void
spiel_gutter_renderer_init (SpielGutterRenderer *self)
{
  self->slide_at_line = g_hash_table_new (g_direct_hash, g_direct_equal);

  /* FIXME: do something better */
  gtk_widget_set_size_request (GTK_WIDGET (self), 36, 10);

  self->buffer_signals = g_signal_group_new (SPIEL_TYPE_BUFFER);
  g_signal_group_connect_object (self->buffer_signals,
                                 "notify::has-selection",
                                 G_CALLBACK (gtk_widget_queue_draw),
                                 self,
                                 G_CONNECT_SWAPPED);
  g_signal_group_connect_object (self->buffer_signals,
                                 "mark-set",
                                 G_CALLBACK (gtk_widget_queue_draw),
                                 self,
                                 G_CONNECT_SWAPPED);
  g_signal_group_connect_object (self->buffer_signals,
                                 "mark-deleted",
                                 G_CALLBACK (gtk_widget_queue_draw),
                                 self,
                                 G_CONNECT_SWAPPED);
  g_signal_group_connect (self->buffer_signals,
                          "cursor-moved",
                          G_CALLBACK (on_buffer_cursor_moved_cb),
                          self);
}

SpielGutterRenderer *
spiel_gutter_renderer_new (SpielProject *project)
{
  return g_object_new (SPIEL_TYPE_GUTTER_RENDERER,
                       "project", project,
                       NULL);
}
