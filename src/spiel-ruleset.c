/*
 * spiel-ruleset.c
 *
 * Copyright 2024 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "spiel-ruleset-private.h"
#include "spiel-slide-feature-descriptor.h"

#include <gio/gio.h>

struct _SpielRuleset
{
  GObject parent_instance;

  GStrv denied_features;
  GStrv optional_features;
  GStrv required_features;

  GPtrArray *required_feature_descriptors;
  GPtrArray *optional_feature_descriptors;
  GPtrArray *denied_feature_descriptors;
};

static void g_initable_iface_init (GInitableIface *iface);

G_DEFINE_FINAL_TYPE_WITH_CODE (SpielRuleset, spiel_ruleset, G_TYPE_OBJECT,
                               G_IMPLEMENT_INTERFACE (G_TYPE_INITABLE, g_initable_iface_init))

G_DEFINE_QUARK (SpielRuleset, spiel_ruleset_error);

enum {
  PROP_0,
  PROP_DENIED_FEATURES,
  PROP_OPTIONAL_FEATURES,
  PROP_REQUIRED_FEATURES,
  N_PROPS,
};

static GParamSpec *properties [N_PROPS];


/*
 * Auxiliary methods
 */

static gboolean
match_and_consume_descriptors (GPtrArray               *descriptors,
                               const SpielSlideFeature *feature)
{
  g_assert (descriptors != NULL);
  g_assert (feature != NULL);

  for (size_t i = 0; i < descriptors->len; i++)
    {
      SpielSlideFeatureDescriptor *descriptor = g_ptr_array_index (descriptors, i);

      if (spiel_slide_feature_descriptor_match (descriptor, feature))
        {
          g_ptr_array_remove_index_fast (descriptors, i);
          return TRUE;
        }
    }

  return FALSE;
}


/*
 * Callbacks
 */

static gpointer
copy_feature_descriptor_cb (gconstpointer src,
                            gpointer      data)
{
  SpielSlideFeatureDescriptor *descriptor = (SpielSlideFeatureDescriptor *) src;
  return g_object_ref (descriptor);
}


/*
 * GInitable interface
 */

static gboolean
spiel_ruleset_initable_init (GInitable     *initable,
                             GCancellable  *cancellable,
                             GError       **error)
{
  SpielRuleset *self = (SpielRuleset *) initable;

  g_assert (SPIEL_IS_RULESET (self));
  g_assert (self->denied_features != NULL);
  g_assert (self->optional_features != NULL);
  g_assert (self->required_features != NULL);

  if (!self->denied_features[0] &&
      !self->optional_features[0] &&
      !self->required_features[0])
    {
      g_set_error (error, SPIEL_RULESET_ERROR, SPIEL_RULESET_ERROR_NO_RULES, "No rules given for the ruleset");
      return FALSE;
    }

  self->required_feature_descriptors = g_ptr_array_new_with_free_func (g_object_unref);
  self->optional_feature_descriptors = g_ptr_array_new_with_free_func (g_object_unref);
  self->denied_feature_descriptors = g_ptr_array_new_with_free_func (g_object_unref);

  for (size_t i = 0; self->required_features[i]; i++)
    {
      g_autoptr (SpielSlideFeatureDescriptor) descriptor = NULL;

      descriptor = spiel_slide_feature_descriptor_from_string (self->required_features[i], error);
      if (!descriptor)
        return FALSE;

      g_ptr_array_add (self->required_feature_descriptors, g_steal_pointer (&descriptor));
    }

  for (size_t i = 0; self->optional_features[i]; i++)
    {
      g_autoptr (SpielSlideFeatureDescriptor) descriptor = NULL;

      descriptor = spiel_slide_feature_descriptor_from_string (self->optional_features[i], error);
      if (!descriptor)
        return FALSE;

      g_ptr_array_add (self->optional_feature_descriptors, g_steal_pointer (&descriptor));
    }

  for (size_t i = 0; self->denied_features[i]; i++)
    {
      g_autoptr (SpielSlideFeatureDescriptor) descriptor = NULL;

      descriptor = spiel_slide_feature_descriptor_from_string (self->denied_features[i], error);
      if (!descriptor)
        return FALSE;

      g_ptr_array_add (self->denied_feature_descriptors, g_steal_pointer (&descriptor));
    }

  return TRUE;
}

static void
g_initable_iface_init (GInitableIface *iface)
{
  iface->init = spiel_ruleset_initable_init;
}


/*
 * GObject overrides
 */

static void
spiel_ruleset_finalize (GObject *object)
{
  SpielRuleset *self = (SpielRuleset *)object;

  g_clear_pointer (&self->denied_features, g_strfreev);
  g_clear_pointer (&self->optional_features, g_strfreev);
  g_clear_pointer (&self->required_features, g_strfreev);

  g_clear_pointer (&self->denied_feature_descriptors, g_ptr_array_unref);
  g_clear_pointer (&self->optional_feature_descriptors, g_ptr_array_unref);
  g_clear_pointer (&self->required_feature_descriptors, g_ptr_array_unref);

  G_OBJECT_CLASS (spiel_ruleset_parent_class)->finalize (object);
}

static void
spiel_ruleset_get_property (GObject    *object,
                            guint       prop_id,
                            GValue     *value,
                            GParamSpec *pspec)
{
  SpielRuleset *self = SPIEL_RULESET (object);

  switch (prop_id)
    {
    case PROP_DENIED_FEATURES:
      g_value_set_boxed (value, self->denied_features);
      break;

    case PROP_OPTIONAL_FEATURES:
      g_value_set_boxed (value, self->optional_features);
      break;

    case PROP_REQUIRED_FEATURES:
      g_value_set_boxed (value, self->required_features);
      break;


    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
spiel_ruleset_set_property (GObject      *object,
                            guint         prop_id,
                            const GValue *value,
                            GParamSpec   *pspec)
{
  SpielRuleset *self = SPIEL_RULESET (object);

  switch (prop_id)
    {
    case PROP_DENIED_FEATURES:
      g_clear_pointer (&self->denied_features, g_strfreev);
      self->denied_features = g_value_dup_boxed (value);
      if (!self->denied_features)
        self->denied_features = g_strdupv ((char* []) { NULL, });
      g_assert (self->denied_features != NULL);
      break;

    case PROP_OPTIONAL_FEATURES:
      g_clear_pointer (&self->optional_features, g_strfreev);
      self->optional_features = g_value_dup_boxed (value);
      if (!self->optional_features)
        self->optional_features = g_strdupv ((char* []) { NULL, });
      g_assert (self->optional_features != NULL);
      break;

    case PROP_REQUIRED_FEATURES:
      g_clear_pointer (&self->required_features, g_strfreev);
      self->required_features = g_value_dup_boxed (value);
      if (!self->required_features)
        self->required_features = g_strdupv ((char* []) { NULL, });
      g_assert (self->required_features != NULL);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
spiel_ruleset_class_init (SpielRulesetClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = spiel_ruleset_finalize;
  object_class->get_property = spiel_ruleset_get_property;
  object_class->set_property = spiel_ruleset_set_property;

  properties[PROP_DENIED_FEATURES] =
    g_param_spec_boxed ("denied-features", NULL, NULL,
                        G_TYPE_STRV,
                        G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  properties[PROP_OPTIONAL_FEATURES] =
    g_param_spec_boxed ("optional-features", NULL, NULL,
                        G_TYPE_STRV,
                        G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  properties[PROP_REQUIRED_FEATURES] =
    g_param_spec_boxed ("required-features", NULL, NULL,
                        G_TYPE_STRV,
                        G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
spiel_ruleset_init (SpielRuleset *self)
{
  self->denied_features = g_strdupv ((char* []) { NULL, });
  self->optional_features = g_strdupv ((char* []) { NULL, });
  self->required_features = g_strdupv ((char* []) { NULL, });
}

/**
 * spiel_ruleset_new:
 * @required_features: (array zero-terminated=1): required features
 * @optional_features: (array zero-terminated=1): optional features
 * @denied_features: (array zero-terminated=1): denied features
 * @error: (nullable): return location for an error
 *
 * Returns: a new ruleset, if it succeeds creation
 */
SpielRuleset *
spiel_ruleset_new (const char * const  *required_features,
                   const char * const  *optional_features,
                   const char * const  *denied_features,
                   GError             **error)
{
  return g_initable_new (SPIEL_TYPE_RULESET,
                         NULL,
                         error,
                         "required-features", required_features,
                         "optional-features", optional_features,
                         "denied-features", denied_features,
                         NULL);
}

/**
 * spiel_ruleset_get_required_features:
 *
 * Returns: required features of the ruleset
 */
const char * const *
spiel_ruleset_get_required_features (SpielRuleset *self)
{
  g_return_val_if_fail (SPIEL_IS_RULESET (self), NULL);

  return (const char * const *) self->required_features;
}

/**
 * spiel_ruleset_get_optional_features:
 *
 * Returns: optional features of the ruleset
 */
const char * const *
spiel_ruleset_get_optional_features (SpielRuleset *self)
{
  g_return_val_if_fail (SPIEL_IS_RULESET (self), NULL);

  return (const char * const *) self->optional_features;
}

/**
 * spiel_ruleset_get_denied_features:
 *
 * Returns: denied features of the ruleset
 */
const char * const *
spiel_ruleset_get_denied_features (SpielRuleset *self)
{
  g_return_val_if_fail (SPIEL_IS_RULESET (self), NULL);

  return (const char * const *) self->denied_features;
}

SpielRulesetMatchResult
spiel_ruleset_calculate_score_for_features (SpielRuleset             *self,
                                            const SpielSlideFeature  *features[],
                                            uint32_t                 *out_score)
{
  g_autoptr (GPtrArray) required_feature_descriptors = NULL;
  g_autoptr (GPtrArray) optional_feature_descriptors = NULL;
  g_autoptr (GPtrArray) denied_feature_descriptors = NULL;
  size_t n_features = 0;

  required_feature_descriptors = g_ptr_array_copy (self->required_feature_descriptors, copy_feature_descriptor_cb, NULL);
  optional_feature_descriptors = g_ptr_array_copy (self->optional_feature_descriptors, copy_feature_descriptor_cb, NULL);
  denied_feature_descriptors = g_ptr_array_copy (self->denied_feature_descriptors, copy_feature_descriptor_cb, NULL);

  for (size_t j = 0; features[j]; j++)
    {
      const SpielSlideFeature *feature = features[j];

      if (match_and_consume_descriptors (denied_feature_descriptors, feature))
        return SPIEL_RULESET_MATCH_RESULT_DENIED;

      if (match_and_consume_descriptors (required_feature_descriptors, feature))
        n_features++;

      if (match_and_consume_descriptors (optional_feature_descriptors, feature))
        n_features++;
    }

  if (self->required_feature_descriptors->len > 0 && required_feature_descriptors->len > 0)
    return SPIEL_RULESET_MATCH_RESULT_UNMATCHED;

  if (out_score)
    *out_score = n_features;

  return SPIEL_RULESET_MATCH_RESULT_SCORED;
}
