/*
 * ttl-title-layout.c
 *
 * Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "ttl-title-layout.h"

#include "spiel-slide-feature.h"
#include "spiel-slide-layout.h"

struct _TtlTitleLayout
{
  AdwBin parent_instance;

  GtkLabel *title_label;
};

static void spiel_slide_layout_interface_init (SpielSlideLayoutInterface *iface);

G_DEFINE_FINAL_TYPE_WITH_CODE (TtlTitleLayout, ttl_title_layout, ADW_TYPE_BIN,
                               G_IMPLEMENT_INTERFACE (SPIEL_TYPE_SLIDE_LAYOUT,
                                                      spiel_slide_layout_interface_init))

static gboolean
ttl_title_layout_apply (SpielSlideLayout        *layout,
                        const SpielSlideFeature *features[])
{
  TtlTitleLayout *self = (TtlTitleLayout *) layout;

  g_assert (TTL_TITLE_LAYOUT (self));

  for (size_t i = 0; features[i]; i++)
    {
      if (spiel_slide_feature_get_feature_type (features[i]) == SPIEL_FEATURE_TITLE)
        {
          const char *title = spiel_slide_feature_get_title_text (features[i]);
          gtk_label_set_markup (self->title_label, title);
          break;
        }
    }

  return TRUE;
}

static void
spiel_slide_layout_interface_init (SpielSlideLayoutInterface *iface)
{
  iface->apply = ttl_title_layout_apply;
}

static void
ttl_title_layout_finalize (GObject *object)
{
  TtlTitleLayout *self = (TtlTitleLayout *)object;

  G_OBJECT_CLASS (ttl_title_layout_parent_class)->finalize (object);
}

static void
ttl_title_layout_class_init (TtlTitleLayoutClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->finalize = ttl_title_layout_finalize;

  gtk_widget_class_set_template_from_resource (widget_class, "/com/feaneron/Spiel/layouts/titles/ttl-title-layout.ui");

  gtk_widget_class_bind_template_child (widget_class, TtlTitleLayout, title_label);

  gtk_widget_class_set_css_name (widget_class, "title-layout");
}

static void
ttl_title_layout_init (TtlTitleLayout *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}
