/*
 * spiel-slide-feature.h
 *
 * Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>
#include <stdint.h>

G_BEGIN_DECLS

typedef enum {
  SPIEL_FEATURE_TITLE,
  SPIEL_FEATURE_MEDIA,
  SPIEL_FEATURE_NOTE,
} SpielFeatureType;

#define SPIEL_TYPE_SLIDE_FEATURE (spiel_slide_feature_get_type ())
typedef struct _SpielSlideFeature SpielSlideFeature;

GType spiel_slide_feature_get_type (void) G_GNUC_CONST;

uint32_t     spiel_slide_feature_get_title_level (const SpielSlideFeature *self);
const char * spiel_slide_feature_get_title_text  (const SpielSlideFeature *self);
const char * spiel_slide_feature_get_note        (const SpielSlideFeature *self);

SpielFeatureType spiel_slide_feature_get_feature_type (const SpielSlideFeature *self);

unsigned int        spiel_slide_feature_hash_shallow (gconstpointer      data);
unsigned int        spiel_slide_feature_hash_deep    (gconstpointer      data);

gboolean            spiel_slide_feature_equal_shallow (gconstpointer a,
                                                       gconstpointer b);
gboolean            spiel_slide_feature_equal_deep    (gconstpointer a,
                                                       gconstpointer b);

SpielSlideFeature * spiel_slide_feature_copy (const SpielSlideFeature *self);
void                spiel_slide_feature_free (SpielSlideFeature *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (SpielSlideFeature, spiel_slide_feature_free)

G_END_DECLS
