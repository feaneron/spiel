/*
 * spiel-source-view.c
 *
 * Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "spiel-source-view.h"

#include "spiel-application.h"
#include "spiel-buffer.h"
#include "spiel-gutter-renderer.h"
#include "spiel-project.h"

struct _SpielSourceView
{
  GtkSourceView parent_instance;

  SpielProject *project;
};

G_DEFINE_FINAL_TYPE (SpielSourceView, spiel_source_view, GTK_SOURCE_TYPE_VIEW)

enum {
  PROP_0,
  PROP_PROJECT,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];


/*
 * GtkTextView overrides
 */

static GtkTextBuffer *
spiel_source_view_create_buffer (GtkTextView *text_view)
{
  return spiel_buffer_new_empty ();
}


/*
 * GObject overrides
 */

static void
spiel_source_view_dispose (GObject *object)
{
  SpielSourceView *self = (SpielSourceView *)object;

  if (self->project)
    spiel_project_unload_markdown (self->project);

  g_clear_object (&self->project);

  G_OBJECT_CLASS (spiel_source_view_parent_class)->dispose (object);
}

static void
spiel_source_view_constructed (GObject *object)
{
  SpielSourceView *self = (SpielSourceView *)object;

  G_OBJECT_CLASS (spiel_source_view_parent_class)->constructed (object);

  if (self->project)
    {
      g_autoptr (SpielGutterRenderer) gutter_renderer = NULL;
      g_autoptr (SpielBuffer) buffer = NULL;
      GtkSourceGutter *gutter;

      buffer = spiel_project_get_buffer (self->project);
      gtk_text_view_set_buffer (GTK_TEXT_VIEW (self), GTK_TEXT_BUFFER (buffer));

      gutter = gtk_source_view_get_gutter (GTK_SOURCE_VIEW (self), GTK_TEXT_WINDOW_LEFT);
      gutter_renderer = spiel_gutter_renderer_new (self->project);
      gtk_source_gutter_insert (gutter, GTK_SOURCE_GUTTER_RENDERER (gutter_renderer), 0);
    }
}

static void
spiel_source_view_get_property (GObject    *object,
                                guint       prop_id,
                                GValue     *value,
                                GParamSpec *pspec)
{
  SpielSourceView *self = SPIEL_SOURCE_VIEW (object);

  switch (prop_id)
    {
    case PROP_PROJECT:
      g_value_set_object (value, self->project);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
spiel_source_view_set_property (GObject      *object,
                                guint         prop_id,
                                const GValue *value,
                                GParamSpec   *pspec)
{
  SpielSourceView *self = SPIEL_SOURCE_VIEW (object);

  switch (prop_id)
    {
    case PROP_PROJECT:
      g_assert (self->project == NULL);
      self->project = g_value_dup_object (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
spiel_source_view_class_init (SpielSourceViewClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  GtkTextViewClass *text_view_class = GTK_TEXT_VIEW_CLASS (klass);

  object_class->dispose = spiel_source_view_dispose;
  object_class->constructed = spiel_source_view_constructed;
  object_class->get_property = spiel_source_view_get_property;
  object_class->set_property = spiel_source_view_set_property;

  properties[PROP_PROJECT] =
    g_param_spec_object ("project", "", "",
                         SPIEL_TYPE_PROJECT,
                         G_PARAM_READWRITE |
                         G_PARAM_CONSTRUCT_ONLY |
                         G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);

  text_view_class->create_buffer = spiel_source_view_create_buffer;

  gtk_widget_class_set_template_from_resource (widget_class, "/com/feaneron/Spiel/spiel-source-view.ui");
}

static void
spiel_source_view_init (SpielSourceView *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}

GtkWidget *
spiel_source_view_new (SpielProject *project)
{
  return g_object_new (SPIEL_TYPE_SOURCE_VIEW,
                       "project", project,
                       NULL);
}
