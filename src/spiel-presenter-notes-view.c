/*
 * spiel-presenter-notes-view.c
 *
 * Copyright 2024 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "spiel-presenter-notes-view.h"

#include "spiel-presentation-controller.h"
#include "spiel-presenter-note-row.h"
#include "spiel-project.h"
#include "spiel-slide.h"
#include "spiel-slide-rendition.h"
#include "spiel-slide-feature.h"

#include <glib/gi18n.h>

struct _SpielPresenterNotesView
{
  AdwBin parent_instance;

  SpielSlideRendition *current_slide_rendition;
  SpielSlideRendition *next_slide_rendition;
  AdwBin *notes_bin;

  SpielPresentationController *controller;
};

G_DEFINE_FINAL_TYPE (SpielPresenterNotesView, spiel_presenter_notes_view, ADW_TYPE_BIN)

enum
{
  PROP_0,
  PROP_PRESENTATION_CONTROLLER,
  N_PROPS,
};

static GParamSpec *properties [N_PROPS];


/*
 * Auxiliary methods
 */


static GtkWidget *
create_widget_for_slide (SpielSlide *slide,
                         size_t      position)
{

  const SpielSlideFeature **features;
  g_autoptr (GtkWidget) group = NULL;
  g_autofree char *title = NULL;
  gboolean has_notes = FALSE;

  title = g_strdup_printf (_("Slide %lu"), position + 1);

  group = adw_preferences_group_new ();
  adw_preferences_group_set_title (ADW_PREFERENCES_GROUP (group), title);
  g_object_ref_sink (group);

  features = spiel_slide_get_features (slide);

  for (size_t feature_index = 0; features && features[feature_index]; feature_index++)
    {
      const SpielSlideFeature *feature = features[feature_index];

      switch (spiel_slide_feature_get_feature_type (feature))
        {
        case SPIEL_FEATURE_NOTE:
          {
            GtkWidget *row = spiel_presenter_note_row_new (feature);
            adw_preferences_group_add (ADW_PREFERENCES_GROUP (group), row);
            has_notes = TRUE;
          }
          break;

        case SPIEL_FEATURE_MEDIA:
        case SPIEL_FEATURE_TITLE:
          continue;

        default:
          g_assert_not_reached ();
        }
    }

  if (!has_notes)
    {
      GtkWidget *empty_row = adw_action_row_new ();
      adw_preferences_row_set_title (ADW_PREFERENCES_ROW (empty_row), _("No Notes"));
      gtk_list_box_row_set_activatable (GTK_LIST_BOX_ROW (empty_row), FALSE);
      adw_preferences_group_add (ADW_PREFERENCES_GROUP (group), empty_row);
    }

  return g_steal_pointer (&group);
}

/*
 * Callback
 */

static void
on_presentation_controller_current_slide_changed_cb (SpielPresentationController *controller,
                                                     GParamSpec                  *pspec,
                                                     SpielPresenterNotesView     *self)
{
  g_autoptr (SpielSlide) current_slide = NULL;
  g_autoptr (SpielSlide) next_slide = NULL;
  SpielProject *project;
  GListModel *slides;
  GtkWidget *notes;
  uint32_t current_slide_index;

  g_assert (SPIEL_IS_PRESENTER_NOTES_VIEW (self));

  project = spiel_presentation_controller_get_project (controller);
  slides = spiel_project_get_slides (project);
  current_slide_index = spiel_presentation_controller_get_current_slide (controller);

  current_slide = g_list_model_get_item (slides, current_slide_index);
  spiel_slide_rendition_set_slide (self->current_slide_rendition, current_slide);

  next_slide = g_list_model_get_item (slides, current_slide_index + 1);
  spiel_slide_rendition_set_slide (self->next_slide_rendition, next_slide);

  notes = create_widget_for_slide (current_slide, current_slide_index);
  adw_bin_set_child (self->notes_bin, notes);
}


/*
 * GObject overrides
 */

static void
spiel_presenter_notes_view_dispose (GObject *object)
{
  SpielPresenterNotesView *self = (SpielPresenterNotesView *)object;

  g_clear_object (&self->controller);

  G_OBJECT_CLASS (spiel_presenter_notes_view_parent_class)->dispose (object);
}

static void
spiel_presenter_notes_view_constructed (GObject *object)
{
  SpielPresenterNotesView *self = (SpielPresenterNotesView *)object;

  G_OBJECT_CLASS (spiel_presenter_notes_view_parent_class)->constructed (object);

  g_signal_connect_object (self->controller,
                           "notify::current-slide",
                           G_CALLBACK (on_presentation_controller_current_slide_changed_cb),
                           self, 0);
}

static void
spiel_presenter_notes_view_get_property (GObject    *object,
                                         guint       prop_id,
                                         GValue     *value,
                                         GParamSpec *pspec)
{
  SpielPresenterNotesView *self = SPIEL_PRESENTER_NOTES_VIEW (object);

  switch (prop_id)
    {
    case PROP_PRESENTATION_CONTROLLER:
      g_value_set_object (value, self->controller);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
spiel_presenter_notes_view_set_property (GObject      *object,
                                         guint         prop_id,
                                         const GValue *value,
                                         GParamSpec   *pspec)
{
  SpielPresenterNotesView *self = SPIEL_PRESENTER_NOTES_VIEW (object);

  switch (prop_id)
    {
    case PROP_PRESENTATION_CONTROLLER:
      g_assert (self->controller == NULL);
      self->controller = g_value_dup_object (value);
      g_assert (self->controller != NULL);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
spiel_presenter_notes_view_class_init (SpielPresenterNotesViewClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  g_type_ensure (SPIEL_TYPE_SLIDE_RENDITION);

  object_class->dispose = spiel_presenter_notes_view_dispose;
  object_class->constructed = spiel_presenter_notes_view_constructed;
  object_class->get_property = spiel_presenter_notes_view_get_property;
  object_class->set_property = spiel_presenter_notes_view_set_property;

  properties[PROP_PRESENTATION_CONTROLLER] =
    g_param_spec_object ("presentation-controller", NULL, NULL,
                         SPIEL_TYPE_PRESENTATION_CONTROLLER,
                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);

  gtk_widget_class_set_template_from_resource (widget_class, "/com/feaneron/Spiel/spiel-presenter-notes-view.ui");

  gtk_widget_class_bind_template_child (widget_class, SpielPresenterNotesView, current_slide_rendition);
  gtk_widget_class_bind_template_child (widget_class, SpielPresenterNotesView, next_slide_rendition);
  gtk_widget_class_bind_template_child (widget_class, SpielPresenterNotesView, notes_bin);
}

static void
spiel_presenter_notes_view_init (SpielPresenterNotesView *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}

GtkWidget *
spiel_presenter_notes_view_new (SpielPresentationController *controller)
{
  return g_object_new (SPIEL_TYPE_PRESENTER_NOTES_VIEW,
                       "presentation-controller", controller,
                       NULL);
}
