/*
 * spiel-project-list.c
 *
 * Copyright 2023 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "spiel-project-list.h"

#include "spiel-project-private.h"

#include <gio/gio.h>

/* random number that everyone else seems to use, too */
#define FILES_PER_QUERY 100

struct _SpielProjectList
{
  GObject parent_instance;

  GListStore *projects;

  GCancellable *cancellable;
  GError *error;
};

static void g_list_model_interface_init (GListModelInterface *iface);

G_DEFINE_FINAL_TYPE_WITH_CODE (SpielProjectList, spiel_project_list, G_TYPE_OBJECT,
                               G_IMPLEMENT_INTERFACE (G_TYPE_LIST_MODEL, g_list_model_interface_init))

enum {
  PROP_0,
  PROP_ERROR,
  PROP_LOADING,
  N_PROPS
};

static GParamSpec *properties[N_PROPS] = { NULL, };


/*
 * Auxiliary functions
 */

static gboolean
stop_loading (SpielProjectList *self)
{
  if (!self->cancellable)
    return FALSE;

  g_cancellable_cancel (self->cancellable);
  g_clear_object (&self->cancellable);
  return TRUE;
}


/*
 * Callbacks
 */

static void
on_items_changed_cb (GListModel       *model,
                     unsigned int      position,
                     unsigned int      removed,
                     unsigned int      added,
                     SpielProjectList *self)
{
  g_list_model_items_changed (G_LIST_MODEL (self), position, removed, added);
}

static void
project_deleted_cb (GObject      *source_object,
                    GAsyncResult *result,
                    gpointer      user_data)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GTask) task = NULL;
  SpielProjectList *self;
  SpielProject *project;
  GPtrArray *projects;

  task = G_TASK (user_data);
  self = g_task_get_source_object (task);
  project = SPIEL_PROJECT (source_object);
  projects = g_task_get_task_data (task);

  if (spiel_project_delete_finish (project, result, &error))
    {
      unsigned int position = 0;

      g_list_store_find (self->projects, project, &position);
      g_list_store_remove (self->projects, position);
    }
  else
    {
      g_warning ("Error deleting project %s: %s",
                 spiel_project_get_id (project),
                 error->message);
    }

  /* FIXME: we need to report errors somehow */

  g_ptr_array_remove (projects, project);

  g_debug ("Delete project %s, %u remaining",
           spiel_project_get_id (project),
           projects->len);

  if (projects->len == 0)
    g_task_return_boolean (task, TRUE);
}

static void
project_opened_cb (GObject      *source,
                   GAsyncResult *result,
                   gpointer      user_data)
{
  g_autoptr (SpielProject) project = NULL;
  g_autoptr (GError) error = NULL;
  SpielProjectList *self;

  self = user_data; /* invalid if cancelled */

  project = spiel_project_open_finish (result, &error);
  if (!project)
    {
      if (!g_error_matches (error, G_IO_ERROR, G_IO_ERROR_CANCELLED))
        g_warning ("Error loading project: %s", error->message);
      return;
    }

  g_list_store_append (self->projects, project);
}

static void
enumerator_closed_cb (GObject      *source,
                      GAsyncResult *res,
                      gpointer      user_data)
{
  g_file_enumerator_close_finish (G_FILE_ENUMERATOR (source), res, NULL);
}

static void
got_files_cb (GObject      *source,
              GAsyncResult *result,
              gpointer      user_data)
{
  g_autolist (GFileInfo) file_infos = NULL;
  g_autoptr (GError) error = NULL;
  SpielProjectList *self;
  GFileEnumerator *enumerator;

  self = user_data; /* invalid if cancelled */
  enumerator = G_FILE_ENUMERATOR (source);

  file_infos = g_file_enumerator_next_files_finish (enumerator, result, &error);

  if (file_infos == NULL)
    {
      if (g_error_matches (error, G_IO_ERROR, G_IO_ERROR_CANCELLED))
        {
          g_clear_error (&error);
          return;
        }

      g_file_enumerator_close_async (enumerator,
                                     G_PRIORITY_DEFAULT,
                                     NULL,
                                     enumerator_closed_cb,
                                     NULL);

      g_object_freeze_notify (G_OBJECT (self));

      g_clear_object (&self->cancellable);
      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_LOADING]);

      if (error)
        {
          self->error = error;
          g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_ERROR]);
        }

      g_object_thaw_notify (G_OBJECT (self));
      return;
    }

  for (GList *l = file_infos; l != NULL; l = l->next)
    {
      g_autoptr (GFile) file = NULL;
      GFileInfo *info;

      info = l->data;
      file = g_file_enumerator_get_child (enumerator, info);

      if (g_file_info_get_file_type (info) != G_FILE_TYPE_DIRECTORY)
        {
          g_message ("Ignoring %s", g_file_peek_path (file));
          continue;
        }

      g_message ("Loading directory %s", g_file_peek_path (file));

      spiel_project_open (file, self->cancellable, project_opened_cb, self);
    }

  g_file_enumerator_next_files_async (enumerator,
                                      50 * FILES_PER_QUERY,
                                      G_PRIORITY_DEFAULT,
                                      self->cancellable,
                                      got_files_cb,
                                      self);
}

static void
got_enumerator_cb (GObject      *source,
                   GAsyncResult *result,
                   gpointer      user_data)
{
  g_autoptr (GFileEnumerator) enumerator = NULL;
  g_autoptr (GError) error = NULL;
  SpielProjectList *self;
  GFile *file;

  self = user_data;
  file = G_FILE (source);
  enumerator = g_file_enumerate_children_finish (file, result, &error);
  if (error)
    {
      if (g_error_matches (error, G_IO_ERROR, G_IO_ERROR_CANCELLED))
        {
          g_clear_error (&error);
          return;
        }

      g_object_freeze_notify (G_OBJECT (self));
      self->error = g_steal_pointer (&error);
      g_clear_object (&self->cancellable);
      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_LOADING]);
      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_ERROR]);
      g_object_thaw_notify (G_OBJECT (self));
      return;
    }

  g_file_enumerator_next_files_async (enumerator,
                                      50 * FILES_PER_QUERY,
                                      G_PRIORITY_DEFAULT,
                                      self->cancellable,
                                      got_files_cb,
                                      self);
}

static void
project_created_cb (GObject      *source,
                    GAsyncResult *result,
                    gpointer      user_data)
{
  g_autoptr (SpielProject) project = NULL;
  g_autoptr (GError) error = NULL;
  g_autoptr (GTask) task = NULL;
  SpielProjectList *self;

  task = G_TASK (user_data);
  project = spiel_project_create_finish (result, &error);
  if (!project)
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  self = g_task_get_source_object (task);
  g_list_store_append (self->projects, project);

  g_task_return_pointer (task, g_object_ref (project), g_object_unref);
}


/*
 * GListModel interface
 */

static gpointer
spiel_project_list_get_item (GListModel   *model,
                             unsigned int  i)
{
  SpielProjectList *self;

  g_assert (SPIEL_IS_PROJECT_LIST (model));

  self = (SpielProjectList *)model;
  return g_list_model_get_item (G_LIST_MODEL (self->projects), i);
}

static GType
spiel_project_list_get_item_type (GListModel *model)
{
  return SPIEL_TYPE_PROJECT;
}

static unsigned int
spiel_project_list_get_n_items (GListModel *model)
{
  SpielProjectList *self;

  g_assert (SPIEL_IS_PROJECT_LIST (model));

  self = (SpielProjectList *)model;
  return g_list_model_get_n_items (G_LIST_MODEL (self->projects));
}


static void
g_list_model_interface_init (GListModelInterface *iface)
{
  iface->get_n_items = spiel_project_list_get_n_items;
  iface->get_item_type = spiel_project_list_get_item_type;
  iface->get_item = spiel_project_list_get_item;
}


/*
 * GObject overrides
 */

static void
spiel_project_list_dispose (GObject *object)
{
  SpielProjectList *self = (SpielProjectList *)object;

  stop_loading (self);

  G_OBJECT_CLASS (spiel_project_list_parent_class)->dispose (object);
}

static void
spiel_project_list_finalize (GObject *object)
{
  SpielProjectList *self = (SpielProjectList *)object;

  g_clear_object (&self->projects);

  G_OBJECT_CLASS (spiel_project_list_parent_class)->finalize (object);
}

static void
spiel_project_list_get_property (GObject    *object,
                                 guint       prop_id,
                                 GValue     *value,
                                 GParamSpec *pspec)
{
  SpielProjectList *self = SPIEL_PROJECT_LIST (object);

  switch (prop_id)
    {
    case PROP_ERROR:
      g_value_set_boxed (value, self->error);
      break;

    case PROP_LOADING:
      g_value_set_boolean (value, spiel_project_list_is_loading (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
spiel_project_list_set_property (GObject      *object,
                                 guint         prop_id,
                                 const GValue *value,
                                 GParamSpec   *pspec)
{
  //SpielProject *self = SPIEL_PROJECT (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
spiel_project_list_class_init (SpielProjectListClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = spiel_project_list_dispose;
  object_class->finalize = spiel_project_list_finalize;
  object_class->get_property = spiel_project_list_get_property;
  object_class->set_property = spiel_project_list_set_property;

  properties[PROP_ERROR] = g_param_spec_boxed ("error", NULL, NULL,
                                               G_TYPE_ERROR,
                                               G_PARAM_READABLE |
                                               G_PARAM_EXPLICIT_NOTIFY |
                                               G_PARAM_STATIC_STRINGS);

  properties[PROP_LOADING] = g_param_spec_boolean ("loading", NULL, NULL,
                                                   FALSE,
                                                   G_PARAM_READABLE |
                                                   G_PARAM_EXPLICIT_NOTIFY |
                                                   G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
spiel_project_list_init (SpielProjectList *self)
{
  g_autoptr (GFile) folder = NULL;

  self->projects = g_list_store_new (SPIEL_TYPE_PROJECT);
  g_signal_connect (self->projects, "items-changed", G_CALLBACK (on_items_changed_cb), self);

  folder = g_file_new_for_path (g_get_user_data_dir ());

  self->cancellable = g_cancellable_new ();
  g_file_enumerate_children_async (folder,
                                   G_FILE_ATTRIBUTE_STANDARD_NAME,
                                   G_FILE_QUERY_INFO_NONE,
                                   G_PRIORITY_DEFAULT,
                                   self->cancellable,
                                   got_enumerator_cb,
                                   self);
}

SpielProjectList *
spiel_project_list_new (void)
{
  return g_object_new (SPIEL_TYPE_PROJECT_LIST, NULL);
}

const GError *
spiel_project_list_get_error (SpielProjectList *self)
{
  g_return_val_if_fail (SPIEL_IS_PROJECT_LIST (self), NULL);

  return self->error;
}

gboolean
spiel_project_list_is_loading (SpielProjectList *self)
{
  g_return_val_if_fail (SPIEL_IS_PROJECT_LIST (self), FALSE);

  return self->cancellable != NULL;
}

SpielProject *
spiel_project_list_get_project_by_id (SpielProjectList *self,
                                      const char       *project_id)
{
  g_return_val_if_fail (SPIEL_IS_PROJECT_LIST (self), NULL);
  g_return_val_if_fail (g_uuid_string_is_valid (project_id), NULL);

  /* TODO: store in a hash table? */

  for (unsigned int i = 0; i < g_list_model_get_n_items (G_LIST_MODEL (self)); i++)
    {
      g_autoptr (SpielProject) project = g_list_model_get_item (G_LIST_MODEL (self), i);

      if (g_strcmp0 (spiel_project_get_id (project), project_id) == 0)
        return g_steal_pointer (&project);
    }

  return NULL;
}

void
spiel_project_list_create_project (SpielProjectList    *self,
                                   GCancellable        *cancellable,
                                   GAsyncReadyCallback  callback,
                                   gpointer             user_data)
{
  g_autoptr (GTask) task = NULL;

  g_return_if_fail (SPIEL_IS_PROJECT_LIST (self));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, spiel_project_list_create_project);

  spiel_project_create (cancellable, project_created_cb, g_steal_pointer (&task));
}

SpielProject *
spiel_project_list_create_project_finish (SpielProjectList  *self,
                                          GAsyncResult      *result,
                                          GError           **error)
{
  g_return_val_if_fail (!error || !*error, NULL);
  g_return_val_if_fail (SPIEL_IS_PROJECT_LIST (self), NULL);
  g_return_val_if_fail (g_task_is_valid (result, self), NULL);
  g_return_val_if_fail (g_task_get_source_tag (G_TASK (result)) == spiel_project_list_create_project, NULL);

  return g_task_propagate_pointer (G_TASK (result), error);
}

void
spiel_project_list_delete_projects (SpielProjectList    *self,
                                    GList               *projects,
                                    GCancellable        *cancellable,
                                    GAsyncReadyCallback  callback,
                                    gpointer             user_data)
{
  g_autoptr (GPtrArray) projects_array = NULL;
  g_autoptr (GTask) task = NULL;

  g_return_if_fail (SPIEL_IS_PROJECT_LIST (self));
  g_return_if_fail (projects != NULL);

  projects_array = g_ptr_array_new_with_free_func (g_object_unref);
  for (GList *l = projects; l; l = l->next)
    g_ptr_array_add (projects_array, g_object_ref (l->data));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, spiel_project_list_delete_projects);
  g_task_set_task_data (task, g_ptr_array_ref (projects_array), (GDestroyNotify) g_ptr_array_unref);

  for (size_t i = 0; i < projects_array->len; i++)
    {
      SpielProject *project = g_ptr_array_index (projects_array, i);

      g_debug ("Deleting project %s", spiel_project_get_id (project));
      spiel_project_delete (project, cancellable, project_deleted_cb, g_object_ref (task));
    }
}

gboolean
spiel_project_list_delete_projects_finish (SpielProjectList  *self,
                                           GAsyncResult      *result,
                                           GError           **error)
{
  g_return_val_if_fail (!error || !*error, FALSE);
  g_return_val_if_fail (SPIEL_IS_PROJECT_LIST (self), FALSE);
  g_return_val_if_fail (g_task_is_valid (result, self), FALSE);
  g_return_val_if_fail (g_task_get_source_tag (G_TASK (result)) == spiel_project_list_delete_projects, FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}
