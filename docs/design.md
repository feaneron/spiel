Slides are automatically generated from the Markdown text, without manual
intervention.



# Project

Projects are the toplevel data structure in Spiel. A Spiel project contains in
disk:

 * Project-specific settings
   * Project version
   * Theme
 * Markdown document



# Slide Deck

A slide deck is merely a GListModel of slides. It is generated at execution
time from the information contained in the project.



# Slide

Each slide is generated from a slide template, with the specific Markdown
data applied to it.



# Slide Template

A slide template is a static and immutable description of what a slide
has. It holds the following data:

 * Markdown blocks of interest
 * ???



# Themes

A theme dictates the style and typography of the slide deck. The theme applies
to the project.
