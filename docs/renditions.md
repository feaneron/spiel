Slide renditions are renders of specific slides with a particular layout that
best represents their contents. The style classes used in elements are unified
and documented here.

## Rendition types

| Rendition type | CSS Class       |
|----------------|-----------------|
| Thumbnail      | `.thumbnail`    |
| Presentation   | `.presentation` |

## Typography

| Title Level    | CSS Class      |
|----------------|----------------|
| Opening        | `.opening`     |
| Title 1        | `.title-1`     |
| Title 2        | `.title-2`     |
| Title 3        | `.title-3`     |
| Title 4        | `.title-4`     |
