desktop_file = i18n.merge_file(
  input: 'com.feaneron.Spiel.desktop.in',
  output: 'com.feaneron.Spiel.desktop',
  type: 'desktop',
  po_dir: '..' / 'po',
  install: true,
  install_dir: get_option('datadir') / 'applications',
)

desktop_utils = find_program('desktop-file-validate', required: false)
if desktop_utils.found()
  test('Validate desktop file', desktop_utils, args: [desktop_file])
endif

appstream_file = i18n.merge_file(
  input: 'com.feaneron.Spiel.appdata.xml.in',
  output: 'com.feaneron.Spiel.appdata.xml',
  po_dir: '..' / 'po',
  install: true,
  install_dir: get_option('datadir') / 'appdata',
)

appstream_util = find_program('appstream-util', required: false)
if appstream_util.found()
  test('Validate appstream file',
    appstream_util,
    args: ['validate', appstream_file],
    should_fail: true,
  )
endif

install_data('com.feaneron.Spiel.gschema.xml',
  install_dir: get_option('datadir') / 'glib-2.0' / 'schemas',
)

compile_schemas = find_program('glib-compile-schemas')

# for unit tests - gnome.compile_schemas() only looks in srcdir
copied_schema_xml = fs.copyfile(
  'com.feaneron.Spiel.gschema.xml',
  'com.feaneron.Spiel.gschema.xml'
)
compiled_schemas = custom_target(
  'gschemas.compiled',
  input: copied_schema_xml,
  output: 'gschemas.compiled',
  command: [compile_schemas, meson.current_build_dir()],
)

tests_gschema_dir = meson.current_build_dir()

test(
  'Validate schema file',
  compile_schemas,
  args: ['--strict', '--dry-run', meson.current_source_dir()]
)

subdir('icons')
subdir('styles')
